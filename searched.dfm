object SearchEditor: TSearchEditor
  Left = 565
  Top = 89
  ClientHeight = 272
  ClientWidth = 172
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AspLabel: TLabel
    Left = 16
    Top = 160
    Width = 85
    Height = 13
    Caption = 'Aspiration window'
    Transparent = True
  end
  object Fut1Box: TCheckBox
    Left = 16
    Top = 184
    Width = 97
    Height = 17
    Caption = 'Futility 1'
    TabOrder = 0
    OnClick = Fut1BoxClick
  end
  object Fut2Box: TCheckBox
    Left = 16
    Top = 208
    Width = 97
    Height = 17
    Caption = 'Futility 2'
    TabOrder = 1
    OnClick = Fut2BoxClick
  end
  object RazorBox: TCheckBox
    Left = 16
    Top = 232
    Width = 97
    Height = 17
    Caption = 'Razoring'
    TabOrder = 2
    OnClick = RazorBoxClick
  end
  object Fut1Edit: TSpinEdit
    Left = 112
    Top = 184
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 0
    OnChange = Fut1EditChange
  end
  object Fut2Edit: TSpinEdit
    Left = 112
    Top = 208
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
    OnChange = Fut2EditChange
  end
  object RazorEdit: TSpinEdit
    Left = 112
    Top = 232
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
    OnChange = RazorEditChange
  end
  object PVSBox: TCheckBox
    Left = 16
    Top = 16
    Width = 137
    Height = 17
    Caption = 'PVS'
    TabOrder = 6
    OnClick = PVSBoxClick
  end
  object AspEdit: TSpinEdit
    Left = 112
    Top = 160
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 7
    Value = 0
    OnChange = AspEditChange
  end
  object IIDBox: TCheckBox
    Left = 16
    Top = 32
    Width = 137
    Height = 17
    Caption = 'IID'
    TabOrder = 8
    OnClick = IIDBoxClick
  end
  object LMRBox: TCheckBox
    Left = 16
    Top = 48
    Width = 137
    Height = 17
    Caption = 'LMR'
    TabOrder = 9
    OnClick = LMRBoxClick
  end
  object StaticBox: TCheckBox
    Left = 16
    Top = 64
    Width = 137
    Height = 17
    Caption = 'Static Pruning'
    TabOrder = 10
    OnClick = StaticBoxClick
  end
  object HashPVBox: TCheckBox
    Left = 16
    Top = 80
    Width = 137
    Height = 17
    Caption = 'Hash in PV'
    TabOrder = 11
    OnClick = HashPVBoxClick
  end
end
