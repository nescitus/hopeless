
procedure FillSqr( Kind, Sqr, Loc : Integer );
begin
   with Board do begin
     Index[ Sqr ] := Loc;
     List[ Loc ].Sqr := Sqr;
     List[ Loc ].Kind := Kind;
   end; // with Board
end; // FillSqr

procedure MovePiece( SqFr, SqTo : Integer );
begin
  with Board do begin
   Index[ SqTo ] := Index[ SqFr ];
   Index[ SqFr ] := 0;
   List[ Index[SqTo] ].Sqr := SqTo;
  end;
end; // MovePiece

procedure KillPiece( Sqr : Integer );
begin
   // first update material counters...
   Board.MovesTo50 := 0;
   if Board.List[ Board.Index[Sqr] ].Color = WHITE
      then begin
         case Board.List[ Board.Index[Sqr] ].Kind of
           PAWN   : Dec( Board.Index[WPCount] );
           KNIGHT : begin Dec( Board.Index[WNCount] ); Dec(Board.Index[WMat]); end;
           BISH   : begin Dec( Board.Index[WBCount] ); Dec(Board.Index[WMat]); end;
           ROOK   : begin Dec( Board.Index[WRCount] ); Board.Index[WMat] := Board.Index[WMat] - 2; end;
           QUEEN  : begin Dec( Board.Index[WQCount] ); Board.Index[WMat] := Board.Index[WMat] - 3; end;
         end; // of case
      end // white pieces
      else begin // black pieces
         case Board.List[ Board.Index[Sqr] ].Kind of
           PAWN   : Dec( Board.Index[BPCount] );
           KNIGHT : begin Dec( Board.Index[BNCount] ); Dec(Board.Index[BMat]); end;
           BISH   : begin Dec( Board.Index[BBCount] ); Dec(Board.Index[BMat]); end;
           ROOK   : begin Dec( Board.Index[BRCount] ); Board.Index[BMat] := Board.Index[BMat] - 2; end;
           QUEEN  : begin Dec( Board.Index[BQCount] ); Board.Index[BMat] := Board.Index[BMat] - 3; end;
         end; // of case
      end; // black pieces
   // ...then remove a piece
   Board.List[ Board.Index[Sqr] ].Sqr  := OUTSIDE;
   Board.Index [ Sqr ] := 0;
end; // KillPiece

procedure MakeMove( Move : TMove );
begin
   Inc( StackIndex );
   Board.Ep := OUTSIDE;
   if Board.List[ Board.Index[ Move.SqFr ] ].Kind = PAWN
      then Board.MovesTo50 := 0
      else Inc( Board.MovesTo50 );

   if Move.Flag = EPCAPT
      then begin
         if Board.Side = WHITE
            then KillPiece( Move.SqTo + SOUTH )
            else KillPiece( Move.SqTo + NORTH );
      end;

   case Move.SqFr of  // mark the castling rights
     E1 : Board.Index[ WKMoved ] := 1;
     E8 : Board.Index[ BKMoved ] := 1;
     A1 : Board.Index[ WQRMoved ] := 1;
     H1 : Board.Index[ WKRMoved ] := 1;
     A8 : Board.Index[ BQRMoved ] := 1;
     H8 : Board.Index[ BKRMoved ] := 1;
   end; // of case

   if ( Move.Flag = CAPT )
   or ( Move.Flag = CAPTPROM )
      then KillPiece( Move.SqTo )
      else begin
       if Move.Flag = CASTLE
          then begin
            case Move.SqTo of
             G1 : MovePiece( H1, F1 );
             C1 : MovePiece( A1, D1 );
             G8 : MovePiece( H8, F8 );
             C8 : MovePiece( A8, D8 );
           end; // of case
          end  // CASTLING
          else begin
          if Move.Flag = PAWNJUMP
          then begin // set en passant square
            if Board.Side = WHITE
               then Board.Ep := Move.SqTo + SOUTH
               else Board.Ep := Move.SqTo + NORTH;
          end;
          end;
      end;

   MovePiece( Move.SqFr, Move.SqTo );

   if ( Move.Flag = PROM )
   or ( Move.Flag = CAPTPROM )
   then begin
     if Board.List[ Board.Index[Move.SqTo] ].Color = WHITE
        then begin // white piece
          Dec( Board.Index[WPCount] );
          case Move.Prom of
            QUEEN  : begin Inc( Board.Index[WQCount] ); Board.Index[WMat] := Board.Index[WMat] + 3; end;
            ROOK   : begin Inc( Board.Index[WRCount] ); Board.Index[WMat] := Board.Index[WMat] + 2; end;
            BISH   : begin Inc( Board.Index[WBCount] ); Inc( Board.Index[WMat] ); end;
            KNIGHT : begin Inc( Board.Index[WNCount] ); Inc( Board.Index[WMat] ); end;
          end; // of case
        end // white piece
        else begin // black piece
          Dec( Board.Index[BPCount] );
          case Move.Prom of
            QUEEN  : begin Inc( Board.Index[BQCount] ); Board.Index[BMat] := Board.Index[BMat] + 3; end;
            ROOK   : begin Inc( Board.Index[BRCount] ); Board.Index[BMat] := Board.Index[BMat] + 2; end;
            BISH   : begin Inc( Board.Index[BBCount] ); Inc( Board.Index[BMat] ); end;
            KNIGHT : begin Inc( Board.Index[BNCount] ); Inc( Board.Index[BMat] ); end;
          end; // of case
        end; // black piece
     Board.List[ Board.Index[Move.SqTo] ].Kind := Move.Prom;
   end;

   SwitchSide;
   GameStack[ StackIndex ] := Board;
   MoveStack[ StackIndex ] := Move;
end; // MakeMove

procedure UnMakeMove;
begin
   Dec( StackIndex );
   Board := GameStack[ StackIndex ];
end; // UnmakeMove

procedure MakeNullMove;
begin
   Inc( StackIndex );
   SwitchSide;
   Board.Ep := OUTSIDE;
   GameStack[ StackIndex ] := Board;
end; // MakeNullMove

function CountLegalMoves : Integer;  // Auxiliary function
         var A, Lim : Integer;       // counting legal
         List : TMoveSet;            // moves.
begin
  Result := 0;
  GenMoves;
  Lim := MoveNo;
  List := MoveSet;
  for A := 1 to Lim do
  begin
    MakeMove( List[ A ] );
    GenCapt;
    if not KingEnPrise
       then Inc( Result );
    UnMakeMove;
  end;
end; // CountLegalMoves
