
// This file is a part of "Clericus"  -  a chess program  /
// by Pawel Koziol, nescitus@o2.pl.                       /
// -----------------------------------------------------  /

{$DEFINE PAWN_THREATS}
{$DEFINE PIECE_THREATS}
{$DEFINE USE_MOBILITY}
// {$DEFINE MORE_FIANCH}

// Set the distance bonus/penalty, using a formula given  /
// by  Adam Kujawski in his thesis about  chess  program  /
// "Joanna" (1994).                                       /

procedure TEval.SetDist;
          var A, B : Integer;
begin
  for A := 0 to 127 do
  for B := 1 to 127 do
      Dist[ A, B ] := 7 - Abs( ( A mod 16 ) - ( B mod 16 ) )
                        - Abs( ( A div 16 ) - ( B div 16 ) );
end;  // SetDist

// "Neighbours"  are squares near to the king, used  for   /
// attack  calculation.  Please   note   that  not  only   /
// neighbouring  squares in the strict sense of the word   /
// are  treated  as  such, but also three  more  distant   /
// in front of the King (front = facing enemy position).   /

procedure TEval.SetNeighbours;
          var A, B : Integer;
begin
   for A := 0 to 127 do
   for B := 0 to 127 do
   begin
     if ( A = B + NORTH ) or ( A = B + SOUTH )
     or ( A = B + EAST  ) or ( A = B + WEST  )
     or ( A = B + NE  )   or ( A = B + NW  )
     or ( A = B + SE  )   or ( A = B + SW  )
        then begin
          // near neighbours
          Neighbours[ WHITE, A, B ] := TRUE;
          Neighbours[ BLACK, A, B ] := TRUE;
        end
        else begin
          // non-neighbours
          Neighbours[ WHITE, A, B ] := FALSE;
          Neighbours[ BLACK, A, B ] := FALSE;

          if ( A = B + NN )     // distant neighbours
          or ( A = B + NNE )    //  color-dependent )
          or ( A = B + NNW )
             then Neighbours[ BLACK, A, B ] := TRUE;

          if ( A = B + SS )
          or ( A = B + SSE )
          or ( A = B + SSW )
             then Neighbours[ WHITE, A, B ] := TRUE;
        end;
   end;
end; // SetNeighbours

constructor TEval.Create;  // Initialize  all the evaluation
begin                      // parameters to default  values.
  inherited Create;        // Later  they can be  ovewrriden
  SetDist;                 // by the .ini file or by edition
  SetNeighbours;           // of the evaluation function.

  Q_VALUE      := 925;      P_VALUE_N    := 100;
  R_VALUE      := 495;      P_VALUE_DE   := 110;
  B_VALUE      := 335;      P_VALUE_AH   := 90;
  N_VALUE      := 325;

  GRAIN        := 1;
  IMBALANCE    := 4;
  TEMPO        := 7;   // could be a function of game phase

  P_DOUBLE     := -20;
  P_WEAK       := -10;
  P_BLOCKED_DE := -15;  
  P_PROTECTS_K := 8;    // not in Eval Editor
  P_DESERTED_K := -6;   // not in Eval Editor
  P_RUNNING    := 500;  // not in Eval Editor

  N_MOB_UNIT := 1;
  N_TRAPPED  := -50;    // not in Eval Editor
  N_BLOCK_P  := -5;     
  N_UNSAFE   := -8;

  B_PAIR      := 25;
  B_PIN       := 10;
  B_NO_FIANCH := -5;
  B_CENT_MISS := -12;
  B_UNSAFE    := -5;
  B_TRAP_A7   := -150;  // not in Eval editor
  B_TRAP_A6   := -50;   // not in Eval Editor

  R_OPEN     := 10;    R_HALF     := 5;
  R_PRESS_K  := 5;     R_PAIR     := 0;

  Q_DIST_K   := 2;     Q_EARLY    := -5;

  K_BLOCK_R    := -25;  
  K_UNCASTLED  := -15;  // not in Eval Editor
  K_COLOR_WEAK := -15;

  N_ATTACKS_K  := 1;           B_ATTACKS_K  := 1;
  R_ATTACKS_K  := 3;           Q_ATTACKS_K  := 5;

  DoScaleMat    := FALSE;      DoOutposts    := FALSE;
  DoPawnStructs := FALSE;      DoPatterns    := TRUE;
end; // TEval.Create

// Now for the functions concerned with board geometry:    /
// I'm not sure that they really belong to TEval class,    /
// and probably they will be moved as soon as they will    /
// be needed in the interior node recognizers.   FIXME!    /

function TEval.IsKingSide( Sqr : Integer ) : Boolean;
begin
   if GetColumn( Sqr ) > COL_E
      then Result := TRUE
      else Result := FALSE;
end; // IsKingSide

function TEval.IsQueenSide( Sqr : Integer ) : Boolean;
begin
   if GetColumn( Sqr ) < COL_D
      then Result := TRUE
      else Result := FALSE;
end; // IsQueenSide

function TEval.IsCentralFile( Sqr : Integer ) : Boolean;
begin
   if ( Sqr mod 16 = COL_D )
   or ( Sqr mod 16 = COL_E )
      then Result := TRUE
      else Result := FALSE;
end; // IsCentralFile

function TEval.NearFile( Sqr1, Sqr2 : Integer ) : Boolean;
         var F1, F2 : Integer;
begin
   F1 := GetColumn( Sqr1 );
   F2 := GetColumn( Sqr2 );

   if ( F1 = F2 ) or ( F1 = F2 + 1 ) or ( F1 = F2 - 1 )
      then Result := TRUE
      else Result := FALSE;
end; // NearFile

function TEval.NearRank( Sqr1, Sqr2 : Integer ) : Boolean;
         var F1, F2 : Integer;
begin
   F1 := GetRow( Sqr1 );
   F2 := GetRow( Sqr2 );

   if ( F1 = F2 ) or ( F1 = F2 + 1 ) or ( F1 = F2 - 1 )
      then Result := TRUE
      else Result := FALSE;
end; // NearRank

function TEval.HorDist( Sqr1, Sqr2 : Integer ) : Integer;
         var F1, F2 : Integer;
begin
   F1 := GetColumn( Sqr1 );
   F2 := GetColumn( Sqr2 );

   if F1 > F2
      then Result := F1 - F2
      else Result := F2 - F1;
end; // HorDist

// A set of questions concerning pawn situation follows.   /
// Again,  perhaps they will be useful in the  interior    /
// node recognizers, which can result in a necessity of    /
// moving them somewhere else.                FIXME ???    /

function TEval.IsOnWPWay( Sqr : Integer ) : Boolean;
         var Here : Integer;
begin
   Result := FALSE;
   Here := Sqr + SOUTH;
   while ( Here and $88 = 0 ) do
   begin
      if IsPiece ( WHITE, PAWN, Here )
         then begin
           Result := TRUE;
           Exit;
         end;
      Here := Here + SOUTH;
   end; // "while" loop
end;  // IsOnWPWay

function TEval.IsOnBPWay( Sqr : Integer ) : Boolean;
         var Here : Integer;
begin
   Result := FALSE;
   Here := Sqr + NORTH;
   while ( Here and $88 = 0 ) do
   begin
      if IsPiece ( BLACK, PAWN, Here )
         then begin
           Result := TRUE;
           Exit;
         end;
      Here := Here + NORTH;
   end;  // "while" loop
end; // IsOnBPWay

function TEval.AreKingSidePawns : Boolean;
begin
   if ( FileStat[ COL_E, WHITE ] + FileStat[ COL_E, BLACK ]
     +  FileStat[ COL_F, WHITE ] + FileStat[ COL_F, BLACK ]
     +  FileStat[ COL_G, WHITE ] + FileStat[ COL_G, BLACK ]
     +  FileStat[ COL_H, WHITE ] + FileStat[ COL_H, BLACK ] = 0 )
   then Result := FALSE
   else Result := TRUE;
end; // AreKingSidePawns

function TEval.AreQueenSidePawns : Boolean;
begin
   if ( FileStat[ COL_A, WHITE ] + FileStat[ COL_A, BLACK ]
     +  FileStat[ COL_B, WHITE ] + FileStat[ COL_B, BLACK ]
     +  FileStat[ COL_C, WHITE ] + FileStat[ COL_C, BLACK ]
     +  FileStat[ COL_D, WHITE ] + FileStat[ COL_D, BLACK ] = 0 )
   then Result := FALSE
   else Result := TRUE;
end; // AreQueenSidePawns

// Function "AdjustMaterial" deals with possible corrections
// of material evaluation. Currently it deals with:        /
//                                                         /
// 1. A bishop pair                                        /
// 2. Knight devaluation sa pawns disappear   (optional)   /
// 3. Growth of Rook value as pawns disappear (optional)   /
// 4. Small penalty for a rook pair (optional)             /

function TEval.AdjustMaterial : Integer;
begin
    Result := 0;

{1} if Board.Index[ WBCount ] > 1        // Bishop pair    /
       then Result := Result + B_PAIR;   // detection,     /
    if Board.Index[ BBCount ] > 1        // assuming oppo- /
       then Result := Result - B_PAIR;   // site colors    /

    if DoScaleMat
    then begin
{2}   Result := Result
              + Board.Index[ WNCount ] * Board.Index[ WPCount ] * IMBALANCE
              - Board.Index[ BNCount ] * Board.Index[ BPCount ] * IMBALANCE
{3}           - Board.Index[ WRCount ] * Board.Index[ WPCount ] * 2 * IMBALANCE
              + Board.Index[ BRCount ] * Board.Index[ BPCount ] * 2 * IMBALANCE;
    end;

{4} if Board.Index[ WRCount ] > 1
       then Result := Result + R_PAIR;
    if Board.Index[ BRCount ] > 1
       then Result := Result - R_PAIR;

end; // AdjustMaterial

// Mobility fumctions for bishops and rooks; knight mobi-  /
// lity  is counted in "IsKnightAttacker", and instead of  /
// queen mobility program checks distance to enemy  king.  /

function TEval.WBMob( Sqr, Vect : Integer ) : Integer;
         var Here : Integer;
begin
   Result := 0;
   Here := Sqr + Vect;
   while Here and $88 = 0 do
   begin
     if Board.Index[ Here ] = 0
        then Inc( Result )
        else begin
          {$IFDEF PIECE_THREATS}
          if Board.Index[ Here ] < 0
             then begin
               if IsPiece( BLACK, KNIGHT, Here )
                  then WThreats := WThreats + 5;
               if IsPiece( BLACK, ROOK, Here )
                  then WThreats := WThreats + 15;
               if IsPiece( BLACK, QUEEN, Here )
                  then WThreats := WThreats + 25;
             end
             else begin
               if  ( IsWBFwdVect( Vect ) )
               and ( IsPiece( WHITE, PAWN, Here ) )
               then WTrapped := WTrapped + WPBB[ Here ];
             end;
          {$ENDIF}
        Exit;
        end;
     Here := Here + Vect;
   end;
end; // WBMob

function TEval.WRMob( Sqr, Vect : Integer ) : Integer;
         var Here : Integer;
begin
   Result := 0;
   Here := Sqr + Vect;
   while Here and $88 = 0 do
   begin
     if Board.Index[ Here ] = 0
        then Inc( Result )
        else begin
          if ( Board.Index[ Here ] < 0 )
             then begin
               {$IFDEF PIECE_THREATS}
               if IsPiece( BLACK, KNIGHT, Here )
                  then WThreats := WThreats + 5;
               if IsPiece( BLACK, BISH, Here )
                  then WThreats := WThreats + 5;
               if IsPiece( BLACK, QUEEN, Here )
                  then WThreats := WThreats + 25;
               {$ENDIF}
             end
             else begin
               if IsPiece( WHITE, ROOK, Here )
                  then WRookCon := TRUE;
             end;
        Exit;
        end;
     Here := Here + Vect;
   end;
end; // WRMob

function TEval.BBMob( Sqr, Vect : Integer ) : Integer;
         var Here : Integer;
begin
   Result := 0;
   Here := Sqr + Vect;
   while Here and $88 = 0 do
   begin
     if Board.Index[ Here ] = 0
        then Inc( Result )
        else begin
          {$IFDEF PIECE_THREATS}
          if ( Board.Index[ Here ] > 1 )
             then begin
               if IsPiece( WHITE, KNIGHT, Here )
                  then BThreats := BThreats + 5;
               if IsPiece( WHITE, ROOK, Here )
                  then BThreats := BThreats + 15;
               if IsPiece( WHITE, QUEEN, Here )
                  then BThreats := BThreats + 25;
             end
             else begin
               if  ( IsBBFwdVect( Vect ) )
               and ( IsPiece( BLACK, PAWN, Here ) )
               then BTrapped := BTrapped + BPBB[ Here ];
             end;
          {$ENDIF}
        Exit;
        end;
     Here := Here + Vect;
   end;
end; // BBMob

function TEval.BRMob( Sqr, Vect : Integer ) : Integer;
         var Here : Integer;
begin
   Result := 0;
   Here := Sqr + Vect;
   while Here and $88 = 0 do
   begin
     if Board.Index[ Here ] = 0
        then Inc( Result )
        else begin
          if ( Board.Index[ Here ] > 0 )
             then begin
               {$IFDEF PIECE_THREATS}
               if IsPiece( WHITE, KNIGHT, Here )
                  then BThreats := BThreats + 5;
               if IsPiece( WHITE, ROOK, Here )
                  then BThreats := BThreats + 15;
               if IsPiece( WHITE, QUEEN, Here )
                  then BThreats := BThreats + 25;
               {$ENDIF}
             end
             else begin
               if IsPiece( BLACK, ROOK, Here )
                  then BRookCon := TRUE;
             end;
        Exit;
        end;
     Here := Here + Vect;
   end;
end; // BRMob

// "IsNeighbour" returns value defined in "SetNeighbours",
// where  You  should look for the  detailed  explanation.

function TEval.IsNeighbour( Color, Sqr1, Sqr2 : Integer ) : Boolean;
begin
   Result := Neighbours[ Color, Sqr1, Sqr2 ];
end; // IsNeighbour

// Function "IsAttacker" is a basis of King safety  routine.
// It checks  if a piece attacks a square near to  the enemy
// King  (for  details see a commment before  the  procedure
// called "SetNeighbours"). Parameter "Masks" denotes number
// of  pieces  standing  on the attacker's path - the attack
// counts  as such if at most one such piece exists. Boolean
// result  of  the  function indicates that  we  have  found
// an attacker,  whereas the strength of  attack ( basically
// a number  of important squares under attack multiplied by
// a factor  depending  on the attacking piece ) changes the
// value of WPressure/BPressure variable.

function TEval.IsWAttacker( Sqr, Vect, Target, Bonus : Integer ) : Boolean;
         var Masks, Here : Integer;
begin
   Result := FALSE;
   Masks := 0;
   Here := Sqr + Vect;
   while ( Here and $88 = 0 ) do
   begin
      if IsNeighbour( WHITE, Here, Target )
         then begin
           Result := TRUE;
           WPressure := WPressure + Bonus
         end;
      if Board.Index[ Here ] <> 0
         then begin
           Inc( Masks );
           if IsPiece( WHITE, PAWN, Here )
              then Inc( Masks );
           if IsSliderVector( Board.List[ Board.Index[Here] ].Kind, Vect )
              then Dec( Masks );
         end;

      if Masks > 1     // too many pieces block the attack
         then Exit;

      Here := Here + Vect;
   end; // loop
end; // IsWAttacker

function TEval.IsBAttacker( Sqr, Vect, Target, Bonus : Integer ) : Boolean;
         var Masks, Here : Integer;
begin
   Result := FALSE;
   Masks := 0;
   Here := Sqr + Vect;
   while ( Here and $88 = 0 ) do
   begin
      if IsNeighbour( BLACK, Here, Target )
         then begin
           Result := TRUE;
           BPressure := BPressure + Bonus;
         end;
      if Board.Index[ Here ] <> 0
         then begin
           Inc( Masks );
           if IsPiece( BLACK, PAWN, Here ) then Inc( Masks );
              if IsSliderVector( Board.List[ Board.Index[Here] ].Kind, Vect )
                 then Dec( Masks );
         end;

      if Masks > 1     // too many pieces block the attack
         then Exit;

      Here := Here + Vect;
   end; // loop
end; // IsBttacker

// "IsWN(BN)Attacker"  counts attack value for  the  knights
// as well as their mobility (please forgive faulty design).
// Attack algorithm is similar to the one  described  above,
// except that there is no question of  interfering  pieces.
// Knight  mobility,  on  the  other  hand,  is  restricted
// to squares not attacked by enemy pawns.

function TEval.IsWNAttacker( Sqr1, Sqr2 : Integer ) : Boolean;
         var A, MobCount : Integer;
begin
   Result   := FALSE;
   MobCount := 0;

   for A := 1 to 8 do
   if ( ( Sqr1 + KnightArray[A] ) and $88 = 0 )
   then begin // loop
     if ( Board.Index [ Sqr1 + KnightArray[A] ] < -1 )
        then begin
          if IsPiece( BLACK, BISH, Sqr1 + KnightArray[A]  )
             then WThreats := WThreats + 5;
          if IsPiece( BLACK, ROOK, Sqr1 + KnightArray[A]  )
             then WThreats := WThreats + 15;
          if IsPiece( BLACK, QUEEN, Sqr1 + KnightArray[A]  )
             then WThreats := WThreats + 25;
        end;

     {$IFDEF USE_MOBILITY}
     if  ( Board.Index [ Sqr1 + KnightArray[A] ] < 1 )
     and ( not IsPiece( BLACK, PAWN, Sqr1 + KnightArray[A] + NE ) )
     and ( not IsPiece( BLACK, PAWN, Sqr1 + KnightArray[A] + NW ) )
         then begin
           Inc( MobCount );
           WMobility := WMobility + N_MOB_UNIT;
         end;
     {$ENDIF}

     // king attack value
     if IsNeighbour( Black, Sqr1 + KnightArray[A], Sqr2 )
     then begin
        Result := TRUE;
        WPressure := WPressure + N_ATTACKS_K;
     end;
   end; // loop

   {$IFDEF USE_MOBILITY}  // Low mobility penalties
   case MobCount of  
      0 : WMobility := WMobility - 6;
      1 : BMobility := BMobility - 2;
   end; // of case
   {$ENDIF}
end; // IsWNAttacker

function TEval.IsBNAttacker( Sqr1, Sqr2 : Integer ) : Boolean;
         var A, MobCount : Integer;
begin
   Result   := FALSE;
   MobCount := 0;

   for A := 1 to 8 do
   if ( ( Sqr1 + KnightArray[A] ) and $88 = 0 )
   then begin // loop
     if ( Board.Index [ Sqr1 + KnightArray[A] ] > 1 )
        then begin
          {$IFDEF PIECE_THREATS}
          if IsPiece( WHITE, BISH, Sqr1 + KnightArray[A] )
             then BThreats := BThreats + 5;
          if IsPiece( WHITE, ROOK, Sqr1 + KnightArray[A] )
             then BThreats := BThreats + 15;
          if IsPiece( WHITE, QUEEN, Sqr1 + KnightArray[A] )
             then BThreats := BThreats + 25;
          {$ENDIF}
        end;

     {$IFDEF USE_MOBILITY}
     if  ( Board.Index [ Sqr1 + KnightArray[A] ] > -1 )
     and ( not IsPiece( WHITE, PAWN, Sqr1 + KnightArray[A] + SE ) )
     and ( not IsPiece( WHITE, PAWN, Sqr1 + KnightArray[A] + SW ) )
         then begin
           Inc( MobCount );
           BMobility := BMobility + N_MOB_UNIT;
         end;
     {$ENDIF}

     // king attack value
     if IsNeighbour( WHITE, Sqr1 + KnightArray[A], Sqr2 )
     then begin
       Result := TRUE;
       BPressure := BPressure + N_ATTACKS_K
     end;
   end; // loop

   {$IFDEF USE_MOBILITY}    // Low mobility penalties
   case MobCount of                
    0 : BMobility := BMobility - 6;
    1 : BMobility := BMobility - 2;
   end; // of case
   {$ENDIF}
end; // IsBNAttacker

procedure TEval.WPThreatens( Sqr : Integer );
begin
  if Board.Index[ Sqr ] < 0
     then begin
       if IsPieceNC( KNIGHT, Sqr )
          then WThreats := WThreats + 30;
       if IsPieceNC( BISH, Sqr )
          then WThreats := WThreats + 30;
       if IsPieceNC( ROOK, Sqr )
          then WThreats := WThreats + 50;
       if IsPieceNC( QUEEN, Sqr )
          then WThreats := WThreats + 90;
     end;
end; // WPThreatens

procedure TEval.BPThreatens( Sqr : Integer );
begin
  if Board.Index[ Sqr ] > 0
     then begin
       if IsPieceNC( KNIGHT, Sqr )
          then BThreats := BThreats + 30;
       if IsPieceNC( BISH, Sqr )
          then BThreats := BThreats + 30;
       if IsPieceNC( ROOK, Sqr )
          then BThreats := BThreats + 50;
       if IsPieceNC( QUEEN, Sqr )
          then BThreats := BThreats + 90;
     end;
end; // WPThreatens

// Functions for kingside fianchetto evaluation            /
// (used in the middlegame only)                           /

function TEval.WKSFianchetto : Integer;
begin
   Result := 0;

   {$IFDEF MORE_FIANCH}
   if IsKingSide( WhiteKingLoc )
   then begin
     if FileStat[ COL_H, BLACK ] = 0
        then Result := Result - 12
        else begin
          if IsPiece( BLACK, PAWN, H4 )
             then Result := Result - 6;
        end;
   end; // attack patterns for the castle with fianchetto}
   {$ENDIF}

   if  ( not IsPiece( WHITE, BISH, G2 ) )
   and ( not IsPiece( WHITE, BISH, H1 ) )
   and ( not IsPiece( WHITE, BISH, F3 ) )
   and ( not IsPiece( WHITE, BISH, H3 ) )
   then begin
     // standard penalty for the lack of fianchetto
     Result := Result + B_NO_FIANCH;

     // a color weakness near the King
     if ( IsKingSide( WhiteKingLoc )
     or ( WhiteKingLoc = E1 ) )  // assuming short castle
        then begin
          if  ( not IsBishop( 6 ) )
          and ( IsBishop( -6 ) )
              then Result := Result + K_COLOR_WEAK
              else Result := Result + B_NO_FIANCH; // this wing is important - double the penalty
        end;
   end;
end; // WKSFianchetto

function TEval.BKSFianchetto : Integer;
begin
   Result := 0;

   {$IFDEF MORE_FIANCH}
   if IsKingSide( BlackKingLoc )
   then begin
     if FileStat[ COL_H, WHITE ] = 0
        then Result := Result - 12
        else begin
          if IsPiece( WHITE, PAWN, H5 )
             then Result := Result - 6;
        end;
   end; // attack patterns for the castle with fianchetto
   {$ENDIF}

   if  ( not IsPiece( BLACK, BISH, G7 ) )
   and ( not IsPiece( BLACK, BISH, H8 ) )
   and ( not IsPiece( BLACK, BISH, F6 ) )
   and ( not IsPiece( BLACK, BISH, H6 ) )
   then begin
     // standard penalty for the lack of fianchetto
     Result := Result + B_NO_FIANCH;

     // a color weakness near the King
     if ( IsKingSide( BlackKingLoc ) )
     or ( BlackKingLoc = E8 ) // assuming short castle
        then begin
          if  ( not IsBishop( -5 ) )
          and ( IsBishop( 5 ) )
              then Result := Result + K_COLOR_WEAK
              else Result := Result + B_NO_FIANCH;
          end;
        end;
end; // BKSFianchetto

// "WPRelations" and "BPRelations" evaluate dependencies   /
// between pawns and pieces. At present they understand:   /
//                                                         /
// 1) blockage of a c pawn in closed positions (bad!)      /
// 2) blockage of a d/e pawn on initial square (terrible!) /
// 3) a Slav/Stonewall/Colle, where c4-c5 is undesirable   /
// 4) fianchetto and its consequences                      /
// 5) certain pawn structures in the center,               /
//    in which it is vital to preserve a good bishop       /

function TEval.WPRelations( Sqr : Integer ) : Integer;
begin
  Result := 0;

  {$IFDEF PAWN_THREATS}
  WPThreatens( Sqr + NE );
  WPThreatens( Sqr + NW );
  {$ENDIF}

  case Sqr of

{1} C2 : if  ( IsPiece( WHITE, PAWN, D4 ) )
         and ( IsPiece( WHITE, KNIGHT, C3 ) )
         and ( not IsPiece( WHITE, PAWN, E4 ) )
             then Result := Result + N_BLOCK_P;

{2} D2 : if not IsEmpty( D3 )
            then Result := Result + P_BLOCKED_DE;
{2} E2 : if not IsEmpty( E3 )
            then Result := Result + P_BLOCKED_DE;

{3} C5 : if  ( IsPiece ( BLACK, PAWN, C6 ) )
         and ( IsPiece ( BLACK, PAWN, D5 ) )
         and ( IsPiece ( WHITE, PAWN, D4 ) )
         and ( FileStat[ COL_B, BLACK ] >=  FileStat[ COL_B, WHITE ] )
         then Result := Result - 10;

{4} G3 : if ( Board.Index[ BMat ] > -12 )
            then Result := Result + WKSFianchetto;

{4} B3 : if ( Board.Index[ BMat ] > -12 )
         then begin
           if  ( not IsPiece( WHITE, BISH, B2 ) )
           and ( not IsPiece( WHITE, BISH, A3 ) )
              then begin
                Result := Result + B_NO_FIANCH;
                if  ( not IsBishop( 5 ) )
                and ( IsBishop( -5 ) )
                and ( IsQueenSide( WhiteKingLoc ) )
                then Result := Result + K_COLOR_WEAK;
              end;
         end; // B3

{5} F3 : if  ( IsPiece( WHITE, PAWN, E4 ) )
         and ( not IsPiece( WHITE, PAWN, D2 ) )
         and ( not IsBishop( 5 ) )
         and ( IsBishop( -5 ) )
         then Result := Result + B_CENT_MISS;

{5} C3 : if  ( IsPiece( WHITE, PAWN, D4 ) )
         and ( not IsPiece( WHITE, PAWN, E2 ) )
         and ( not IsBishop( 6 ) )
         and ( IsBishop( -6 ) )
         then Result := Result + B_CENT_MISS;
  end; // of case
end; // WPRelations

function TEval.BPRelations( Sqr : Integer ) : Integer;
begin
  Result := 0;

  {$IFDEF PAWN_THREATS}
  BPThreatens( Sqr + SE );
  BPThreatens( Sqr + SW );
  {$ENDIF}

  case Sqr of

{1} C7 : if  ( IsPiece( BLACK, PAWN, D5 ) )
         and ( IsPiece( BLACK, KNIGHT, C6 ) )
         and ( not IsPiece( BLACK, PAWN, E5 ) )
             then Result := Result + N_BLOCK_P;

{2} D7 : if not IsEmpty( D6 )
            then Result := Result + P_BLOCKED_DE;
{2} E7 : if not IsEmpty( E6 )
           then Result := Result + P_BLOCKED_DE;

{3} C4 : if  ( IsPiece ( WHITE, PAWN, C3 ) )
         and ( IsPiece ( WHITE, PAWN, D4 ) )
         and ( IsPiece ( BLACK, PAWN, D5 ) )
         and ( FileStat[ COL_B, WHITE ] >=  FileStat[ COL_B, BLACK ] )
             then Result := Result - 10;

{4} G6 : if ( Board.Index[ WMat ] > -12 )
            then Result := Result + BKSFianchetto;

{4} B6 : if ( Board.Index[ WMat ] > -12 )
         then begin
           if  ( not IsPiece( BLACK, BISH, B7 ) )
           and ( not IsPiece( BLACK, BISH, A6 ) )
              then begin
                Result := Result + B_NO_FIANCH;
                if  ( not IsBishop( -6 ) )
                and ( IsBishop( 6 ) )
                and ( IsQueenSide( BlackKingLoc ) )
                then Result := Result + K_COLOR_WEAK;
              end;
         end; // B6

{5} F6 : if  ( IsPiece( BLACK, PAWN, E5 ) )
         and ( not IsPiece( BLACK, PAWN, D7 ) )
         and ( not IsBishop( -6 ) )
         and ( IsBishop( 6 ) )
         then Result := Result + B_CENT_MISS;

{5} C6 : if  ( IsPiece( BLACK, PAWN, D5 ) )
         and ( not IsPiece( BLACK, PAWN, E7 ) )
         and ( not IsBishop( -5 ) )
         and ( IsBishop( 5 ) )
         then Result := Result + B_CENT_MISS;
  end; // of case
end; // BPRelations

function WPCanExpel( Sqr : Integer ) : Boolean;
begin
  Result := FALSE;

  if  ( IsPiece( WHITE, PAWN, Sqr + SSE ) )
  and ( not IsPiece( BLACK, PAWN, Sqr + SE ) )
  and ( not IsPiece( BLACK, PAWN, Sqr + EAST + EAST ) )
      then begin Result := TRUE; Exit; end;

  if  ( IsPiece( WHITE, PAWN, Sqr + SSW ) )
  and ( not IsPiece( BLACK, PAWN, Sqr + SW ) )
  and ( not IsPiece( BLACK, PAWN, Sqr + WEST + WEST ) )
      then begin Result := TRUE; Exit; end;

  if GetRow( Sqr ) = ROW_5  // watch out for two square
     then begin             // pawn moves
       if  ( IsPiece( WHITE, PAWN, Sqr + SSE + SOUTH ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + SE ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + EAST + EAST ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + SSE ) )
           then begin Result := TRUE; Exit; end;

       if  ( IsPiece( WHITE, PAWN, Sqr + SSW + SOUTH ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + SW ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + WEST + WEST ) )
       and ( not IsPiece( BLACK, PAWN, Sqr + SSW ) )
           then Result := TRUE;
     end;
end; // WPCanExpel

function BPCanExpel( Sqr : Integer ) : Boolean;
begin
  Result := FALSE;

  if  ( IsPiece( BLACK, PAWN, Sqr + NNE ) )
  and ( not IsPiece( WHITE, PAWN, Sqr + NE ) )
  and ( not IsPiece( WHITE, PAWN, Sqr + EAST + EAST ) )
  then begin Result := TRUE; Exit; end;

  if  ( IsPiece( BLACK, PAWN, Sqr + NNW ) )
  and ( not IsPiece( WHITE, PAWN, Sqr + NW ) )
  and ( not IsPiece( WHITE, PAWN, Sqr + WEST + WEST ) )
  then begin Result := TRUE; Exit; end;

  if GetRow( Sqr ) = ROW_4  // watch out for two square
     then begin             // pawn moves
       if  ( IsPiece( BLACK, PAWN, Sqr + NNE + NORTH ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + NE ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + EAST + EAST ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + NNE ) )
       then begin Result := TRUE; Exit; end;

       if ( IsPiece( BLACK, PAWN, Sqr + NNW + NORTH ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + NW ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + WEST + WEST ) )
       and ( not IsPiece( WHITE, PAWN, Sqr + NNW ) )
       then Result := TRUE;
     end;
end; // BPCanExpel

function TEval.IsWNOutpost( Sqr : Integer ) : Integer;
begin
   Result := 0;
   if  ( DoOutposts )
   and ( WNOutpost[ Sqr ] > 0 )
       then begin
         if IsPiece( WHITE, PAWN, Sqr + SE )
            then Result := Result + WNOutpost[ Sqr ];
         if IsPiece( WHITE, PAWN, Sqr + SW )
            then Result := Result + WNOutpost[ Sqr ];
       end;
end; // IsWNOutpost

function TEval.IsBNOutpost( Sqr : Integer ) : Integer;
begin
   Result := 0;
   if  ( DoOutposts )
   and ( BNOutpost[ Sqr ] > 0 )
       then begin
         if IsPiece( BLACK, PAWN, Sqr + NE )
            then Result := Result + BNOutpost[ Sqr ];
         if IsPiece( BLACK, PAWN, Sqr + NW )
            then Result := Result + BNOutpost[ Sqr ];
       end;
end; // IsBNOutpost

// Knight evaluation:                                     /
//                                                        /
// 1. Material and piece/square value.                    /
// 2. Mobility and king attack values (calculated by      /
//    the same function, called IsKnightAttacker).        /
// 3. Outpost bonus                                       /
// 4. Penalty for a knight being trapped on the rim       /
//    in the midst of the enemy camp.                     /
// 5. Penalty for an unstable position (i.e. a knight may /
//    be expelled by a pawn ).                            /

function TEval.WNEv( Sqr : Integer ) : Integer;
begin
{1}  Result := N_VALUE + WN[ Sqr ];

{2}  if IsWNAttacker( Sqr, BlackKingLoc )
        then Inc( WAttackers );

{3}  if DoOutposts
        then Result := Result + IsWNOutpost( Sqr );

{4}  case Sqr of
       A7 : if TwoPawns( BLACK, C6, B7 )
               then Result := Result + N_TRAPPED;

       H7 : if TwoPawns( BLACK, F6, G7 )
               then Result := Result + N_TRAPPED;

       A8 : if ( IsPiece( BLACK, PAWN, A7 ) )
            or ( IsPiece( BLACK, PAWN, C7 ) )
               then Result := Result + N_TRAPPED;

       H8 : if ( IsPiece( BLACK, PAWN, H7 ) )
            or ( IsPiece( BLACK, PAWN, F7 ) )
               then Result := Result + N_TRAPPED;
     end; // of case  - trapped knights

{5}  if BPCanExpel( Sqr )
        then Result := Result + N_UNSAFE;
end; // WNEv

function TEval.BNEv( Sqr : Integer ) : Integer;
begin
{1} Result := N_VALUE + BN[ Sqr ];

{2} if IsBNAttacker( Sqr, WhiteKingLoc )
       then Inc( BAttackers );

{3} if DoOutposts
       then Result := Result + IsBNOutpost( Sqr );

{4} case Sqr of
      A2 : if TwoPawns( WHITE, B2, C3 )
              then Result := Result + N_TRAPPED;

      H2 : if TwoPawns( WHITE, G2, F3 )
              then Result := Result + N_TRAPPED;

      A1 : if ( IsPiece( WHITE, PAWN, A2 ) )
           or ( IsPiece( WHITE, PAWN, C2 ) )
              then Result := Result + N_TRAPPED;

      H1 : if ( IsPiece( WHITE, PAWN, H2 ) )
           or ( IsPiece( WHITE, PAWN, F2 ) )
              then Result := Result + N_TRAPPED;
   end; // of case - trapped knight

{5} if WPCanExpel( Sqr )
       then Result := Result + N_UNSAFE;
end; // BNEv

// Bishop evaluation:                                      /
//                                                         /
// 1. Material value and piece/square bonus (centralization)
// 2. Change of mobility value                             /
// 3. Change of king attack value                          /
// 4. Penalty for an unsafe position
// 5. Trapped bishop penalty (A2/H2/A7/H7 and A3/H3/A6/H6) /
// 6. Detection of knights pinned to K/Q/R                 /
// 7. Detection of bishops returning to their home squares /
//    after castling, so that they are not penalized.      /
//                                                         /
// Please note that fianchetto is evaluated as a part of   /
// advanced pawn structure in WP(BP)Relations and a bishop /
// pair is vieved as a part of material value.             /

function TEval.WBEv( Sqr : Integer ) : Integer;
         var TempMob : Integer;
begin
{1} Result := B_VALUE + WB[ Sqr ];

    {$IFDEF USE_MOBILITY}
{2} TempMob := WBMob(Sqr, NE) + WBMob(Sqr, NW) + WBMob (Sqr, SE) + WBMob(Sqr, SW);
    WMobility := WMobility + B_MOB[ TempMob ];
    {$ENDIF}

    if IsWAttacker( Sqr, NE, BlackKingLoc, B_ATTACKS_K )
    or IsWAttacker( Sqr, NW, BlackKingLoc, B_ATTACKS_K )
    or IsWAttacker( Sqr, SE, BlackKingLoc, B_ATTACKS_K )
    or IsWAttacker( Sqr, SW, BlackKingLoc, B_ATTACKS_K )
{3}    then Inc( WAttackers );

{4} if BPCanExpel( Sqr )
       then Result := Result + B_UNSAFE;

    case Sqr of
{5}   H7 : if TwoPawns( BLACK, G6, H7 )
              then WTrapped := WTrapped + B_TRAP_A7;

{5}   A7 : if TwoPawns( BLACK, B6, C7 )
              then WTrapped := WTrapped + B_TRAP_A7;

{5}   H6 : if TwoPawns( BLACK, G5, F6 )
              then WTrapped := WTrapped + B_TRAP_A6;

{5}   A6 : if TwoPawns( BLACK, B5, C6 )
              then WTrapped := WTrapped + B_TRAP_A6;

{6}   G5 : if IsPiece( BLACK, KNIGHT, F6 )
           then begin
             if ( IsPiece( BLACK, QUEEN, E7 ) )
             or ( IsPiece( BLACK, KING, E7 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( E7 ) )
                  and ( ( IsPiece( BLACK, QUEEN, D8 ) ) or ( IsPiece( BLACK, KING, D8 ) ) )
                      then Result := Result + B_PIN;
                end;
             end; // G5

{6}   H4 : if  ( IsPiece( BLACK, KNIGHT, F6 ) )
           and ( IsEmpty( G5 ) )
               then begin
                 if ( IsPiece( BLACK, QUEEN, E7 ) )
                 or ( IsPiece( BLACK, KING, E7 ) )
                    then Result := Result + B_PIN
                    else begin
                      if  ( IsEmpty( E7 ) )
                      and ( ( IsPiece( BLACK, QUEEN, D8 ) ) or ( IsPiece( BLACK, KING, D8 ) ) )
                          then Result := Result + B_PIN;
                    end;
           end; // H4

{6}   B5 : if IsPiece( BLACK, KNIGHT, C6 )
           then begin
             if ( IsPiece( BLACK, QUEEN, D7 ) )
             or ( IsPiece( BLACK, KING, D7 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( D7 ) )
                  and ( ( IsPiece( BLACK, KING, E8 ) ) or ( IsPiece(BLACK, QUEEN, E8) ) )
                      then Result := Result + B_PIN;
                 end;
           end; // B5

{6}   A4 : if  ( IsPiece( BLACK, KNIGHT, C6 ) )
           and ( IsEmpty( B5 ) )
           then begin
             if ( IsPiece( BLACK, QUEEN, D7 ) )
             or ( IsPiece( BLACK, KING, D7 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( D7 ) )
                  and ( ( IsPiece( BLACK, KING, E8 ) ) or ( IsPiece(BLACK, QUEEN, E8) ) )
                      then Result := Result + B_PIN;
                end;
           end; // A4

{7}   F1 : if ( WhiteKingLoc = G1 ) or ( WhiteKingLoc = H1 ) or ( WhiteKingLoc = H2 )
              then Result := Result - WB[ F1 ] + 5;

{7}   C1 : if ( WhiteKingLoc = B1 ) or ( WhiteKingLoc = A1 ) or ( WhiteKingLoc = A2 )
              then Result := Result - WB[ C1 ] + 5;
   end; // of case
end; // WBEv

function TEval.BBEv( Sqr : Integer ) : Integer;
         var TempMob : Integer;
begin
{1} Result := B_VALUE + BB[ Sqr ];

    {$IFDEF USE_MOBILITY}
{2} TempMob := BBMob(Sqr, NE) + BBMob(Sqr, NW) + BBMob (Sqr, SE) + BBMob(Sqr, SW);
    BMobility := BMobility + B_MOB[ TempMob ];
    {$ENDIF}

    if IsBAttacker( Sqr, SE, WhiteKingLoc, B_ATTACKS_K )
    or IsBAttacker( Sqr, SW, WhiteKingLoc, B_ATTACKS_K )
    or IsBAttacker( Sqr, NE, WhiteKingLoc, B_ATTACKS_K )
    or IsBAttacker( Sqr, NW, WhiteKingLoc, B_ATTACKS_K )
{3}    then Inc( BAttackers );

{4} if WPCanExpel( Sqr )
       then Result := Result + B_UNSAFE;

    case Sqr of
{5}   H2 : if TwoPawns( WHITE, F2, G3 )
              then BTrapped := BTrapped + B_TRAP_A7;

{5}   A2 : if TwoPawns( WHITE, C2, B3 )
              then BTrapped := BTrapped + B_TRAP_A7;

{5}   H3 : if TwoPawns( WHITE, F3, G4 )
              then BTrapped := BTrapped + B_TRAP_A6;

{5}   A3 : if TwoPawns( WHITE, C3, B4 )
              then BTrapped := BTrapped + B_TRAP_A6;

{6}   G4 : if  ( IsPiece( WHITE, KNIGHT, F3 ) )
           then begin
             if ( IsPiece( WHITE, KING, E2 ) )
             or ( IsPiece( WHITE, QUEEN,E2 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( E2 ) )
                  and ( ( IsPiece( WHITE, QUEEN, D1 ) ) or ( IsPiece( WHITE, KING, D1 ) ) )
                      then Result := Result + B_PIN;
                end;
           end; // G4

{6}   H5 : if  ( IsPiece( WHITE, KNIGHT, F3 ) )
           and ( IsEmpty( G4 ) )
           then begin
             if ( IsPiece( WHITE, KING, E2 ) )
             or ( IsPiece( WHITE, QUEEN,E2 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( E2 ) )
                  and ( ( IsPiece( WHITE, QUEEN, D1 ) ) or ( IsPiece( WHITE, KING, D1 ) ) )
                      then Result := Result + B_PIN;
                end;
           end; // H5

{6}   B4 : if  ( IsPiece( WHITE, KNIGHT, C3 ) )
           then begin
             if ( IsPiece( WHITE, QUEEN, D2 ) )
             or ( IsPiece( WHITE, KING, D2 ) )
                then Result := Result + B_PIN
                else begin
                  if  ( IsEmpty( D2 ) )
                  and ( ( IsPiece( WHITE, KING, E1 ) ) or ( IsPiece( WHITE, QUEEN, E1 ) ) )
                      then Result := Result + B_PIN;
                end;
           end; // B4

{6}   A5 : if  ( IsPiece( WHITE, KNIGHT, C3 ) )
           and ( IsEmpty( B4 ) )
           then begin
             if  ( IsPiece( WHITE, QUEEN, D2 ) )
             or  ( IsPiece( WHITE, KING, D2 ) )
                 then Result := Result + B_PIN
                 else begin
                   if  ( IsEmpty( D2 ) )
                   and ( ( IsPiece( WHITE, KING, E1 ) ) or ( IsPiece( WHITE, QUEEN, E1 ) ) )
                       then Result := Result + B_PIN;
                 end;
           end; // A5

{7}   F8 : if ( BlackKingLoc = G8 ) or ( BlackKingLoc = H8 ) or ( BlackKingLoc = H7 )
              then Result := Result - BB[ F8 ] + 5;

{7}   C8 : if ( BlackKingLoc = B8 ) or ( BlackKingLoc = A8 ) or ( BlackKingLoc = A7 )
              then Result := Result - BB[ F8 ] + 5;
    end; // of case
end; // BBEv

// Rook evaluation:                                        /
//                                                         /
// 1. Material value and piece/square table                /
// 2. Change of mobility value                             /
// 3. Change of attack value                               /
// 4. Rook connection (see comement in params.pas)         /
// 5. Bonus for occupying a half-open or open file, greater/
//    if a file is directed towards enemy king (4a)        /
// 6. Bonus for an outpost on an open file                 /
// 7. Penalty for a blockage by an uncastled King          /
//    (must be here because isn't used on open files,      /
//     which are already tested within the function)       /

function TEval.WREv( Sqr : Integer ) : Integer;
         var TempMob : Integer;
begin
{1} Result := R_VALUE + WR[ Sqr ];

    {$IFDEF USE_MOBILITY}
{2} TempMob := WRMob( Sqr, NORTH) + WRMob( Sqr, SOUTH )
             + WRMob( Sqr, EAST ) + WRMob( Sqr, WEST );
    WMobility := WMobility + R_MOB[ TempMob ];
    {$ENDIF}

{3} if ( NearFile( Sqr, BlackKingLoc ) and ( IsWAttacker( Sqr, NORTH, BlackKingLoc, R_ATTACKS_K ) or IsWAttacker( Sqr, SOUTH, BlackKingLoc, R_ATTACKS_K ) ) )
    or ( NearRank( Sqr, BlackKingLoc ) and ( IsWAttacker( Sqr, EAST, BlackKingLoc, R_ATTACKS_K )  or IsWAttacker( Sqr, WEST, BlackKingLoc, R_ATTACKS_K ) ) )
       then Inc( WAttackers );

{4} if  ( WRookCon )
    and ( Board.Index[ WMat ] > END_MAT )
        then Result := Result + WRC[ Sqr ];

    if FileStat[ Sqr mod 16, WHITE ] = 0
       then begin
{5}     if FileStat[ Sqr mod 16, BLACK ] = 0
           then Result := Result + R_OPEN
           else Result := Result + R_HALF;

{5a}    if NearFile( Sqr, BlackKingLoc )
           then Result := Result + R_PRESS_K;

        if  ( DoOutposts )
        and ( WROutpost[ Sqr ] > 0 )
{6}     then begin
          if IsPiece( WHITE, PAWN, Sqr + SE )
             then Result := Result + WROutpost[Sqr];
          if IsPiece( WHITE, PAWN, Sqr + SW )
             then Result := Result + WROutpost[Sqr];
        end; // outposts
      end                 // open or half-open file
      else begin          // closed file - blockage possible
{7}     case Sqr of
          H1 : if  ( WhiteKingLoc = F1 ) or ( WhiteKingLoc = G1 )
                   then WTrapped := WTrapped + K_BLOCK_R;

          H2 : if  ( ( WhiteKingLoc = F1 ) or (WhiteKingLoc = G1) or ( WhiteKingLoc = G2 ) or ( WhiteKingLoc = H1 ) )
               and ( IsPiece( WHITE, PAWN, G2 ) or IsPiece( WHITE, PAWN, F2 ) )
                   then WTrapped := WTrapped + K_BLOCK_R;

          G1 : if  WhiteKingLoc = F1
                   then WTrapped := WTrapped + K_BLOCK_R;

          G2 : if  ( ( WhiteKingLoc = F1 ) or (WhiteKingLoc = G1) )
               and ( IsPiece( WHITE, PAWN, F2 ) )
                   then WTrapped := WTrapped + K_BLOCK_R;

          A1 : if  ( WhiteKingLoc = C1 ) or ( WhiteKingLoc = B1 )
                   then WTrapped := WTrapped + K_BLOCK_R;

          A2 : if  ( ( WhiteKingLoc = C1 ) or (WhiteKingLoc = B1) or ( WhiteKingLoc = B2 ) or (WhiteKingLoc = A1 ) )
               and ( IsPiece( WHITE, PAWN, B2 ) or IsPiece( WHITE, PAWN, C2 ) )
                   then WTrapped := WTrapped + K_BLOCK_R;

          B1 : if  WhiteKingLoc = C1
                   then WTrapped := WTrapped + K_BLOCK_R;

          B2 : if  ( ( WhiteKingLoc = C1 ) or (WhiteKingLoc = B1) )
               and ( IsPiece( WHITE, PAWN, C2 ) )
                   then WTrapped := WTrapped + K_BLOCK_R;
        end;     // of case - rook blockages               /
      end;       // closed file                            /
end; // WREv

function TEval.BREv( Sqr : Integer ) : Integer;
         var TempMob : Integer;
begin
{1} Result := R_VALUE + BR[ Sqr ];

    {$IFDEF USE_MOBILITY}
{2} TempMob := BRMob( Sqr, NORTH) + BRMob( Sqr, SOUTH )
             + BRMob( Sqr, EAST ) + BRMob( Sqr, WEST );
    BMobility := BMobility + R_MOB[ TempMob ];
    {$ENDIF}

    if ( NearFile( Sqr, WhiteKingLoc ) and ( IsBAttacker( Sqr, NORTH, WhiteKingLoc, R_ATTACKS_K ) or IsBAttacker( Sqr, SOUTH, WhiteKingLoc, R_ATTACKS_K ) ) )
    or ( NearRank( Sqr, WhiteKingLoc ) and ( IsBAttacker( Sqr, EAST,  WhiteKingLoc, R_ATTACKS_K ) or IsBAttacker( Sqr, WEST,  WhiteKingLoc, R_ATTACKS_K ) ) )
{3}    then Inc( BAttackers );

    if FileStat[ Sqr mod 16, BLACK ] = 0
       then begin  // open or half-open file bonus
{4}      if  ( BRookCon )
         and ( Board.Index[ BMat ] > END_MAT )
             then Result := Result + BRC[ Sqr ];

{5}      if FileStat[ Sqr mod 16, WHITE ] = 0
            then Result := Result + R_OPEN
            else Result := Result + R_HALF;

{5a}     if NearFile( Sqr, WhiteKingLoc )
           then Result := Result + R_PRESS_K;

{6}      if  ( DoOutposts )            // rook outposts
         and ( BROutpost[ Sqr ] > 0 )
         then begin
           if IsPiece( BLACK, PAWN, Sqr + NE )
              then Result := Result + BROutpost[ Sqr ];
           if IsPiece( BLACK, PAWN, Sqr + NW )
              then Result := Result + BROutpost[ Sqr ];
         end; // outposts
       end                // open file
       else begin         // closed file - blockage possible
{7}      case Sqr of
           H8 : if ( BlackKingLoc = F8 )
                or ( BlackKingLoc = G8 )
                    then BTrapped := BTrapped + K_BLOCK_R;

           H7 : if  ( (BlackKingLoc = F8) or (BlackKingLoc = G8) or (BlackKingLoc = G7) or (BlackKingLoc = H8) )
                and ( IsPiece( BLACK, PAWN, F7 ) or IsPiece( BLACK, PAWN, G7 ) )
                    then BTrapped := BTrapped + K_BLOCK_R;

           G8 : if  BlackKingLoc = F8
                    then BTrapped := BTrapped + K_BLOCK_R;

           G7 : if  ( (BlackKingLoc = F8) or (BlackKingLoc = G8) )
                and ( IsPiece( BLACK, PAWN, F7 ) )
                    then BTrapped := BTrapped + K_BLOCK_R;

           A8 : if  ( BlackKingLoc = C8 ) or ( BlackKingLoc = B8 )
                    then BTrapped := BTrapped + K_BLOCK_R;

           A7 : if  ( (BlackKingLoc = C8) or (BlackKingLoc = B8) or (BlackKingLoc = B7) or (BlackKingLoc = A8) )
                and ( IsPiece( BLACK, PAWN, B7 ) or IsPiece( BLACK, PAWN, C7 ) )
                    then BTrapped := BTrapped + K_BLOCK_R;

           B8 : if BlackKingLoc = C8
                   then BTrapped := BTrapped + K_BLOCK_R;

           B7 : if  ( (BlackKingLoc = C8) or (BlackKingLoc = B8) )
                and ( IsPiece( BLACK, PAWN, C7 ) )
                    then BTrapped := BTrapped + K_BLOCK_R;
         end; // of case - rook blockages                  /
    end;      // closed file                               /
end; // BREv

// Queen evaluation:                                       /
//                                                         /
// 1. Material and piece/square bonus                      /
// 2. Bonus or penalty for the distance to the enemy king  /
// 3. A change of the king attack value.                   /
// 4. Centralization bonus, depending on the number of     /
//    opponent's minor pieces ( the less the better ).     /
// 5. Early developement penalty, based on a number of     /
//    undeveloped minor pieces.                            /

function TEval.WQEv( Sqr : Integer ) : Integer;
         var bEarlyCheck : Boolean;
begin
    bEarlyCheck := FALSE;

{1} Result := Q_VALUE
{2}         + Q_DIST_K * Dist[ Sqr, BlackKingLoc ];

    if ( NearFile( Sqr, BlackKingLoc ) and IsWAttacker( Sqr, NORTH, BlackKingLoc, Q_ATTACKS_K ) )
    or ( NearFile( Sqr, BlackKingLoc ) and IsWAttacker( Sqr, SOUTH, BlackKingLoc, Q_ATTACKS_K ) )
    or ( NearRank( Sqr, BlackKingLoc ) and IsWAttacker( Sqr, EAST,  BlackKingLoc, Q_ATTACKS_K ) )
    or ( NearRank( Sqr, BlackKingLoc ) and IsWAttacker( Sqr, WEST,  BlackKingLoc, Q_ATTACKS_K ) )
    or IsWAttacker( Sqr, NE, BlackKingLoc, Q_ATTACKS_K )
    or IsWAttacker( Sqr, NW, BlackKingLoc, Q_ATTACKS_K )
    or IsWAttacker( Sqr, SE, BlackKingLoc, Q_ATTACKS_K )
    or IsWAttacker( Sqr, SW, BlackKingLoc, Q_ATTACKS_K )
{3}    then Inc( WAttackers );

{4} case ( Board.Index[ BBCount ] + Board.Index[ BNCount ] ) of
      0  : Result := Result + WQE[ Sqr ] * 4;
      1  : Result := Result + WQO[ Sqr ] + WQE[ Sqr ] * 3;
      2  : Result := Result + WQO[ Sqr] * 2 + WQE[ Sqr ] * 2;
      3  : begin
             bEarlyCheck := TRUE;
             Result := Result + 3* WQO[Sqr] + WQE[ Sqr ];
           end;
      else begin
             bEarlyCheck := TRUE;
             Result := Result + 4*WQO[Sqr]
           end;
end; // of case

{5} if  ( bEarlyCheck )
    and ( Sqr > 39 )
    then begin
      if IsPiece( WHITE, KNIGHT, B1 )
         then Result := Result + Q_EARLY;
      if IsPiece( WHITE, KNIGHT, G1 )
         then Result := Result + Q_EARLY;
      if IsPiece( WHITE, BISH, C1 )
         then Result := Result + Q_EARLY;
      if IsPiece( WHITE, BISH, F1 )
         then Result := Result + Q_EARLY;
    end;
end; // WQEv

function TEval.BQEv( Sqr : Integer ) : Integer;
         var bEarlyCheck : Boolean;
begin
    bEarlyCheck := FALSE;
{1} Result := Q_VALUE
{2}         + Q_DIST_K * Dist[ Sqr, WhiteKingLoc ];

    if ( NearFile( Sqr, WhiteKingLoc ) and IsBAttacker( Sqr, NORTH, WhiteKingLoc, Q_ATTACKS_K ) )
    or ( NearFile( Sqr, WhiteKingLoc ) and IsBAttacker( Sqr, SOUTH, WhiteKingLoc, Q_ATTACKS_K ) )
    or ( NearRank( Sqr, WhiteKingLoc ) and IsBAttacker( Sqr, EAST,  WhiteKingLoc, Q_ATTACKS_K ) )
    or ( NearRank( Sqr, WhiteKingLoc ) and IsBAttacker( Sqr, WEST,  WhiteKingLoc, Q_ATTACKS_K ) )
    or IsBAttacker( Sqr, NE, WhiteKingLoc, Q_ATTACKS_K )
    or IsBAttacker( Sqr, NW, WhiteKingLoc, Q_ATTACKS_K )
    or IsBAttacker( Sqr, SE, WhiteKingLoc, Q_ATTACKS_K )
    or IsBAttacker( Sqr, SW, WhiteKingLoc, Q_ATTACKS_K )
{3}    then Inc( BAttackers );

{4} case ( Board.Index[ WNCount ] + Board.Index[ WBCount ] ) of
      0  : Result := Result + BQE[ Sqr ] * 4;
      1  : Result := Result + BQO[ Sqr ] + BQE[ Sqr ] * 3;
      2  : Result := Result + BQO[ Sqr ] * 2 + BQE[ Sqr ] * 2;
      3  : begin
             bEarlyCheck := TRUE;
             Result := Result + BQO[ Sqr ] * 3 + BQE[ Sqr ];
           end;
      else begin
             bEarlyCheck := TRUE;
             Result := Result + BQO[ Sqr ] * 4;
           end;
end; // of case

{5} if  ( bEarlyCheck )
    and ( Sqr < 80 )
    then begin
      if IsPiece( BLACK, KNIGHT, B8 )
         then Result := Result + Q_EARLY;
      if IsPiece( BLACK, KNIGHT, G8 )
         then Result := Result + Q_EARLY;
      if IsPiece( BLACK, BISH, C8 )
         then Result := Result + Q_EARLY;
      if IsPiece( BLACK, BISH, F8 )
         then Result := Result + Q_EARLY;
    end;
end; // BQEv

// King evaluation :                                        /
//                                                          /
// 1. Set isEndgame to FALSE if we have plenty of material. /
// 2. Penalize uncastled king in the center (middlegame).   /
// 3. Add centralization bonus (endgame).                   /
// 4. In the endgame with all pawns on one wing, a king     /
//    shouldn't wander on the other wing                    /
// 5. Basic mating phase eval: the king of the weaker side  /
//    gets huge centralization bonus, but the stronger side /
//    gets a bonus for small distance between the kings     /
// 6. Very primitive partial checkmate recognition          /

function TEval.WKEv( Sqr : Integer ) : Integer;
begin
   Result := 0;
   if Board.Index[ BMat ] > END_MAT // middlegame
      then begin
{1}    isEndgame := FALSE;

       if  ( Board.Index[ WKMoved ] > 0 )
       and ( IsCentralFile( Sqr ) )
{2}        then Result := Result + K_UNCASTLED;
     end         // middlegame
     else begin  // endgame
{3}    Result := Result + CE[ Sqr ];

{4}    if ( Board.Index[ WPCount ] + Board.Index[ BPCount ] > 0 )
          then begin
            if not AreKingSidePawns
               then Result := Result + NoKSPawns[ Sqr ];
            if not AreQueenSidePawns
               then Result := Result + NoQSPawns[ Sqr ];
          end;

{5}    if Result > 500
       then Result := Result
                    + 10 * Dist[ BlackKingLoc, Sqr ]
                    - 10 * CE[ BlackKingLoc ];
     end; // endgame

{6}  if  ( Sqr div 16 = ROW_1 )
     and ( IsPiece( BLACK, QUEEN, Sqr + NORTH ) )
     and ( IsAttackedBy( BLACK, Sqr + NORTH, TRUE ) )
     and ( not IsAttackedBy( WHITE, Sqr + NORTH, FALSE ) )
         then Result := -1000;

     if  ( Sqr = H1 )
     and ( IsPiece( BLACK, QUEEN, G2 ) )
     and ( IsAttackedBy( BLACK, G2, TRUE ) )
     and ( not IsAttackedBy( WHITE, G2, FALSE ) )
         then Result := -1000;

     if  ( Sqr = A1 )
     and ( IsPiece( BLACK, QUEEN, B2 ) )
     and ( IsAttackedBy( BLACK, B2, TRUE ) )
     and ( not IsAttackedBy( WHITE, B2, FALSE ) )
         then Result := -1000;
end; // WKEval

function TEval.BKEv( Sqr : Integer ) : Integer;
begin
  Result := 0;
  if Board.Index[ WMat ] > END_MAT
     then begin                 //middlegame
{1}    isEndgame := FALSE;

       if  ( Board.Index[ BKMoved ] > 0 )
       and ( IsCentralFile( Sqr ) )
{2}        then Result := Result + K_UNCASTLED;
     end         // middlegame
     else begin  // endgame
{3}    Result := Result + CE[ Sqr ];

{4}    if ( Board.Index[ WPCount ] + Board.Index[ BPCount ] > 0 )
          then begin
            if not AreKingSidePawns
               then Result := Result + NoKSPawns[ Sqr ];
            if not AreQueenSidePawns
               then Result := Result + NoQSPawns[ Sqr ];
          end;

{5}    if Result < -500 then
       Result := Result + 10 * Dist[ WhiteKingLoc, Sqr ]
                        - 10 * CE[ WhiteKingLoc ];
     end; // endgame

{6}  if  ( Sqr div 16 = ROW_8 )
     and ( IsPiece( WHITE, QUEEN, Sqr + SOUTH ) )
     and ( IsAttackedBy( WHITE, Sqr + SOUTH, TRUE ) )
     and ( not IsAttackedBy( Black, Sqr + SOUTH, FALSE ) )
     then Result := -1000;

     if  ( Sqr = H8 )
     and ( IsPiece( WHITE, QUEEN, G7 ) )
     and ( IsAttackedBy( WHITE, G7, TRUE ) )
     and ( not IsAttackedBy( BLACK, G7, FALSE ) )
         then Result := -1000;

     if  ( Sqr = A8 )
     and ( IsPiece( WHITE, QUEEN, B7 ) )
     and ( IsAttackedBy( WHITE, B7, TRUE ) )
     and ( not IsAttackedBy( BLACK, B7, FALSE ) )
         then Result := -1000;
end; // BKEval

// This procedure gathers information about the number
// of pawns on any given file. It may pay off to do it
// incrementally.

procedure TEval.InitPawnsOnFile;
          var A : Integer;
begin
  FileStat := EmptyFiles;

  for A := 8 to 16 do
  if  ( Board.List[ A ].Sqr <> OUTSIDE )
  and ( Board.List[ A ].Kind = PAWN )
  then Inc( FileStat[ Board.List[ A ].Sqr mod 16, WHITE ] );

  for A := -16 to -8 do
  if  ( Board.List[ A ].Sqr <> OUTSIDE )
  and ( Board.List[ A ].Kind = PAWN )
  then Inc( FileStat[ Board.List[ A ].Sqr mod 16, BLACK ] );
end; // InitPawnsOnFile

procedure TEval.InitEval;
begin
  WAttackers := 0;       BAttackers := 0;
  WPressure  := 0;       BPressure  := 0;
  WMobility  := 0;       BMobility  := 0;
  WThreats   := 0;       BThreats   := 0;
  WTrapped   := 0;       BTrapped   := 0;
  WRookCon   := FALSE;   BRookCon   := FALSE;
  isEndgame := TRUE; // if necessary, this flag will be set
                     // to  "FALSE" during king  evaluation
end; // InitEval

// Here a king attack value is scaled according to the number
// of attacking pieces and previously counted partial value.

function TEval.ScaleAttacks( Points, Pieces : Integer ) : Integer;
begin
  case Pieces of
     0 : Result  := 0;
     1 : Result  := 0;
     2 : Result  := 1 * Points;
     3 : Result  := 2 * Points;
     4 : Result  := 4 * Points;
     else Result := 8 * Points;
  end; // of case
end; // ScaleAttacks

function TEval.OppositeBishops : Boolean;
begin
  Result := FALSE;
  if  ( Board.Index[ WBCount ] = 1 )
  and ( Board.Index[ BBCount ] = 1 )
  then begin
    Result := TRUE;
    if  ( Board.List[-5].Sqr = OUTSIDE )
    and ( Board.List[ 5].Sqr = OUTSIDE )
        then Result := FALSE;

    if  ( Board.List[-6].Sqr = OUTSIDE )
    and ( Board.List[ 6].Sqr = OUTSIDE )
        then Result := FALSE;
  end;
end; // OppositeBishops

function TEval.IsBishop( ListLoc : Integer ) : Boolean;
begin
   if Board.List[ ListLoc ]. Sqr <> OUTSIDE
      then Result := TRUE
      else Result := FALSE;
end; // IsBishop

function TEval.Eval( DoHash : Boolean ) : Integer;
         var A : Integer;
         HTemp, HNo : Integer;
begin
  HTemp := InitHashValue;
  HNo   := GetHashAddr( HTemp );

  if  ( EvalHash[ HNo ].Key = HTemp )
  and ( DoHash )
      then begin
        Result := EvalHash[ HNo ].Val;  // retrieve result
        Exit;                           // from EvalHash
      end;

  InitEval;
  InitPawnsOnFile;
  Result := PawnEval + AdjustMaterial;

  if Board.Side = WHITE then Result := Result + TEMPO
                        else Result := Result - TEMPO;

  // Now program loops through the piece list, firing
  // piece-specific  eval routines. In  this  process
  // king  pressure and mobility values are  updated.

  for A := 1 to 16 do
  if Board.List[ A ].Sqr <> OUTSIDE
     then begin // loop - white pieces
        case Board.List[ A ].Kind of
          PAWN   : Result := Result + WPRelations( Board.List[A].Sqr );
          KNIGHT : Result := Result + WNEv( Board.List[A].Sqr );
          BISH   : Result := Result + WBEv( Board.List[A].Sqr );
          ROOK   : Result := Result + WREv( Board.List[A].Sqr );
          QUEEN  : Result := Result + WQEv( Board.List[A].Sqr );
          KING   : Result := Result + WKEv( Board.List[A].Sqr );
        end; // of case
     end; // loop - white pieces

  for A := -16 to -1 do
  if Board.List[ A ].Sqr <> OUTSIDE
     then begin // loop - black pieces
       case Board.List[ A ].Kind of
         PAWN   : Result := Result - BPRelations( Board.List[A].Sqr );
         KNIGHT : Result := Result - BNEv( Board.List[A].Sqr );
         BISH   : Result := Result - BBEv( Board.List[A].Sqr );
         ROOK   : Result := Result - BREv( Board.List[A].Sqr );
         QUEEN  : Result := Result - BQEv( Board.List[A].Sqr );
         KING   : Result := Result - BKEv( Board.List[A].Sqr );
       end; // of case
     end; // loop - black pieces

 // Add in king pressure and mobility bonuses, scaling them
                                        // as approppriate.
  Result := Result
          + ( ( WhiteAttack * ScaleAttacks( WPressure, WAttackers ) ) div 100 )
          - ( ( BlackAttack * ScaleAttacks( BPressure, BAttackers ) ) div 100 )
          + ( ( WhiteMobility * WMobility ) div 100 )
          - ( ( BlackMobility * BMobility ) div 100 )
          + WThreats
          - BThreats  // TODO: side to move is less threatened
          + WTrapped
          - BTrapped;

  if DoPatterns  // Evaluate attack patterns
     then Result := Result + Patterns;

// Endgame corrections. Apart from easy draw routines stored
// in  "endgame.pas" program uses the following  heuristics:
//                                                         /
// 1) Score of a pawn ending is multiplied by 1.2, so that /
//    the engine can make reasonable conversion decisions. /
// 2) In the pure BOC ending result is halved, as it's     /
//    extremely hard to win.                               /
// 3) KRP vs KR is hard to win if the king of the weaker   /
//    side stands on pawn's way.                           /

 if isEndgame
    then begin
      case EndgameType of
        PAWN_END : Result := ( Result * 5 ) div 4;
        BOC_END  : Result := Result div 2;
        KBPKX    : if IsDrawKBPKX( Result )
                      then Result := 0;
        KRPKR    : begin
          if Result > 0  
          then begin  // white advantage
            if IsOnWPWay( BlackKingLoc )
               then Result := ( Result * 2 ) div 3;
            if IsDrawKRPKR_White
               then Result := 0;
          end; // white advantage

          if  Result < 0  
          then begin  // black advantage
            if IsOnBPWay( WhiteKingLoc )
               then Result := ( Result * 2 ) div 3;
            if IsDrawKRPKR_Black
               then Result := 0;
          end; // black advantage
        end; // KRPKR
      end; // case of EndgameType

      if IsInsufficientMaterial( Result )
         then Result := 0;
  end; // endgame evaluation

  if Board.Side = BLACK
     then Result := -Result;

  Result := ( Result div GRAIN ) * GRAIN;

  EvalHash[ HNo ].Key := HTemp;
  EvalHash[ HNo ].Val := Result;
end; // Eval

function TEval.GamePhaseString : string;
begin
   Result := 'Game phase : ';
   if Board.Index[WMat] + Board.Index[BMat] > -24
      then Result := Result + 'middle game '
      else begin
        Result := Result + 'endgame ';
        case EndgameType of
          PAWN_END   : Result := Result + '(pawns)';
          BISH_END   : Result := Result + '(bishops)';
          BOC_END    : Result := Result + '(opposite bishops)';
          KNIGHT_END : Result := Result + ('knights');
          BN_END     : Result := Result + '(bishop vs knight)';
          ROOK_END   : Result := Result + '(rook endgame)';
          KRPKR      : Result := Result + '(KRPKR)';
        end; // of case
      end;
end; // GamePhaseString

function TEval.EvalString : string;
begin
   if ProgSide = WHITE
      then Result := 'Program plays White; result = '
      else Result := 'Program plays Black; result = ';

   Result := Result + IntToStr( Eval(FALSE) ) + LINE_END
   + ' ' + GamePhaseString + LINE_END
   + ' Material adjustments ' + IntToStr( AdjustMaterial ) + LINE_END
   + ' White attackers: ' + IntToStr( WAttackers ) + ' pressure: ' + IntToStr( WPressure ) + ' attack value: ' + IntToStr( ScaleAttacks( WPressure, WAttackers ) ) + ' * ' + IntToStr( WhiteAttack ) + '% = ' + IntToStr( ScaleAttacks( WPressure, WAttackers ) * WhiteAttack div 100 ) + LINE_END
   + ' Black attackers: ' + IntToStr( BAttackers ) + ' pressure: ' + IntToStr( BPressure ) + ' attack value: ' + IntToStr( ScaleAttacks( BPressure, BAttackers ) ) + ' * ' + IntToStr( BlackAttack ) + '% = ' + IntToStr( ScaleAttacks( BPressure, BAttackers ) * BlackAttack div 100 ) + LINE_END
   + ' White mobility: '  + IntToStr( WhiteMobility) + '% of ' + IntToStr( WMobility ) + ' = ' + IntToStr( WMobility * WhiteMobility div 100 ) + LINE_END
   + ' Black mobility: '  + IntToStr( BlackMobility) + '% of ' + IntToStr( BMobility ) + ' = ' + IntToStr( BMobility * BlackMobility div 100 ) + LINE_END
   + ' White threats: ' + IntToStr( WThreats ) + LINE_END
   + ' Black threats: ' + IntToStr( BThreats ) + LINE_END
   + ' White blockages: ' + IntToStr(WTrapped) + LINE_END
   + ' Black blockages: ' + IntToStr(BTrapped);
end; // EvalString

function PieceHeader( Sign : string; Loc : Integer ) : string;
begin
   Result := Sign + SqrName[ Loc ] + ' ';
end; // PieceHeader

function MatStr( Val : Integer ) : string;
begin
  Result := ' (m ' + IntToStr( Val );
  if Val < 100 then Result := Result + ' ';
end; // MatStr

function TabStr( Val : Integer ) : string;
begin
  Result := ' t ' + IntToStr( Val );
end; // TabStr

function TEval.DebugWhite : string;
         var A, Loc : Integer;
begin
   Result := 'WHITE:' + LINE_END + LINE_END;

   for A := 1 to 16 do
   if Board.List[ A ].Sqr <> OUTSIDE
      then begin
         Loc := Board.List[ A ].Sqr;
         case Board.List[ A ].Kind of

           PAWN : begin
             Result := Result + PieceHeader('P', Loc )
                     + IntToStr( WPEv( Loc ) );

             if WPEv( Loc ) < 100
                then Result := Result + ' ';

             Result := Result  + MatStr( PawnMaterial( Loc ) )
                               + TabStr( WP[ Loc ] ) + ' ';

             if ( IsWPIsolated( Loc ) )
             or ( IsWPBackward( Loc ) )
                then Result := Result + 'weak ' + IntToStr( WWeakPEval( Loc ) ) + ' ';
             if IsWPFree( Board.List[A].Sqr )
                then Result := Result + 'free ' + IntToStr( WFreePEval( Loc ) ) + ' ';
             if WPRelations( Loc ) <> 0
                then begin
                  Result := Result + 'rel ' + IntToStr( WPRelations( Loc ) );
                  if ( Loc = G3 )
                  or ( Loc = B3 )
                     then Result := Result + '(f)';
                  Result := Result + ' ';
                end;
             Result := Result + ')' + LINE_END;
           end; // PAWN

           KNIGHT : begin
             Result := Result
             + PieceHeader('N', Loc )
             + IntToStr( WNEv( Loc ) )
             + MatStr( N_VALUE )
             + TabStr( WN[ Loc ] );

             if IsWNOutpost( Loc ) > 0
                then Result := Result + ' outpost';

             Result := Result + ' )' + LINE_END;
           end; // KNIGHT

           BISH : begin
             Result := Result
             + PieceHeader('B', Loc )
             + IntToStr( WBEv( Loc ) )
             + MatStr( B_VALUE )
             + TabStr( WB[ Loc ] ) + ' )'
             + LINE_END;
           end; // BISH

           ROOK : begin
             Result := Result
             + PieceHeader('R', Loc )
             + IntToStr( WREv( Loc ) )
             + MatStr( R_VALUE )
             + TabStr( WR[ Loc ] ) + ' )';

             if FileStat[ Loc mod 16, BLACK ] = 0
                then Result := Result + ' (file) ';

             Result := Result + LINE_END;
           end; // ROOK

           QUEEN : begin
             Result := Result
             + PieceHeader('Q', Loc )
             + IntToStr( WQEv( Loc ) )
             + MatStr( Q_VALUE )
             + TabStr( WQO[ Loc ] )
             + ' d ' + IntToStr( Q_DIST_K * Dist[ Loc, BlackKingLoc ] ) + ' )'
             + LINE_END;
           end; // QUEEN

           KING : begin
             Result := Result + PieceHeader('K', Loc )
                     + IntToStr( WKEv( Loc ) )
                     + LINE_END;
           end; // KING
         end; // of case
      end; // loop - white pieces
end; // DebugWhite

function TEval.DebugBlack : string;
         var A, Loc : Integer;
begin
Result := 'BLACK:' + LINE_END + LINE_END;

   for A := -1 downto -16 do
   if Board.List[ A ].Sqr <> OUTSIDE
      then begin
        Loc := Board.List[ A ].Sqr;

        case Board.List[ A ].Kind of

          PAWN : begin
            Result := Result + PieceHeader('P', Loc )
                             + IntToStr( BPEv( Loc ) );
            if BPEv( Loc ) < 100
               then Result := Result + ' ';

            Result := Result + MatStr( PawnMaterial( Loc ) )
                             + TabStr( BP[ Loc ] ) + ' ';

            if ( IsBPIsolated( Loc ) )
            or ( IsBPBackward( Loc ) )
               then Result := Result + 'weak ' + IntToStr( BWeakPEval( Loc ) ) + ' ';

            if IsBPFree( Loc )
               then Result := Result + 'free ' + IntToStr( BFreePEval( Loc ) ) + ' ';
            if BPRelations( Board.List[A].Sqr ) <> 0
               then begin
                 Result := Result + 'rel ' + IntToStr( BPRelations( Loc ) );
                 if ( Loc = G6 )
                 or ( Loc = B6 )
                    then Result := Result + '(f)';
                 Result := Result + ' ';
               end; // PAWN

            Result := Result + ')' + LINE_END;
          end;

          KNIGHT : begin
            Result := Result
            + PieceHeader('N', Loc )
            + IntToStr( BNEv( Loc ) )
            + MatStr( N_VALUE )
            + TabStr( BN[ Loc ] );

            if IsBNOutpost( Loc ) > 0
               then Result := Result + 'outpost';

            Result := Result + ' )' + LINE_END;
          end; // KNIGHT

          BISH : begin
            Result := Result + PieceHeader('B', Loc )
            + IntToStr( BBEv( Loc ) )
            + MatStr( B_VALUE )
            + TabStr( BB[ Loc ] ) + ' )';
            Result := Result + LINE_END;
          end; // BISH

          ROOK : begin
            Result := Result + PieceHeader('R', Loc )
            + IntToStr( BREv( Loc ) )
            + MatStr( R_VALUE )
            + TabStr( BR[ Loc ] ) + ' )';

            if FileStat[ Loc mod 16, BLACK ] = 0
               then Result := Result + ' (file) ';

            Result := Result + LINE_END;
          end; // ROOK

          QUEEN : begin
            Result := Result + PieceHeader('Q', Loc )
            + IntToStr( BQEv( Loc ) )
            + MatStr( Q_VALUE )
            + TabStr( BQO[ Loc ] )
            + ' d '  + IntToStr( Q_DIST_K * Dist[ Loc, WhiteKingLoc ] ) + ' )';
            Result := Result + LINE_END;
          end;  // QUEEN

          KING : begin
             Result := Result + PieceHeader('K', Loc )
                     + IntToStr( BKEv( Loc ) )
                     + LINE_END;
          end; // KING
        end; // of case
      end; // loop - black pieces
end; // DebugBlack
