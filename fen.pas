
function TForm1.BoardToFen : string;
         var A, B : Integer;
         EmptyInRow : Integer;
begin
   Result := '';
   EmptyInRow := 0;
   for A := 128 downto 0 do
   if A and $88 = 0 then begin // loop
   B := SqrSym[ A ];
        if Board.Index[ B ]= 0 then Inc( EmptyInRow );
        if Board.Index[ B ] > 0
           then begin
             if EmptyInRow > 0
                then Result := Result + IntToStr(EmptyInRow);
             EmptyInRow := 0;

             case Board.List[ Board.Index[ B ] ].Kind of
               KING  : Result := Result + 'K';
               QUEEN : Result := Result + 'Q';
               ROOK  : Result := Result + 'R';
               BISH  : Result := Result + 'B';
               KNIGHT: Result := Result + 'N';
               PAWN  : Result := Result + 'P';
             end; // of case
           end;   // white pieces

        if Board.Index[ B ] < 0
           then begin  // black pieces
             if EmptyInRow > 0
                then Result := Result + IntToStr( EmptyInRow );
             EmptyInRow := 0;

             case Board.List[Board.Index[B]].Kind of
               KING  : Result := Result + 'k';
               QUEEN : Result := Result + 'q';
               ROOK  : Result := Result + 'r';
               BISH  : Result := Result + 'b';
               KNIGHT: Result := Result + 'n';
               PAWN  : Result := Result + 'p';
             end; // of case
          end;    // black pieces

        if (A = 16 ) or (A = 32) or (A = 48) or (A = 64)
        or (A = 80) or (A = 96) or (A = 112)
        then begin
          if EmptyInRow > 0
             then Result := Result + IntToStr(EmptyInRow);
          EmptyInRow := 0;
          Result := Result + '/'
        end; // mark end of line
   end; // loop

   if Board.Side = WHITE
      then Result := Result + ' w '
      else Result := Result + ' b ';

   // write castling rights
   if ( Board.Index[ WKMoved ] = 1 )
   or ( (Board.Index[ WKRMoved ] = 1 ) and ( Board.Index[ WQRMoved ] = 1 ) )
   then Result := Result + '-'
   else begin
     if Board.Index[ WKRMoved ] = 0
        then Result := Result + 'K';
     if Board.Index[ WQRMoved ] = 0
        then Result := Result + 'Q';
   end;

   if ( Board.Index[BKMoved] = 1 )
   or ( ( Board.Index[BKRMoved] = 1 ) and ( Board.Index[WQRMoved] = 1 ) )
   then Result := Result + '-'
   else begin
        if Board.Index[BKRMoved] = 0
           then Result := Result + 'k';
        if Board.Index[BQRMoved] = 0
           then Result := Result + 'q';
   end;

   if Board.Ep <> A1       // write en passant square
   then begin              // if such capture is possible
      GenMoves;
      for A := 1 to MoveNo do
      if MoveSet[ A ].Flag = EPCAPT
      then begin
           Result := Result + ' ' + SqrName[ Board.Ep ];
           Exit;
      end;
      Result := Result +  ' -';
   end
   else Result := Result + ' -';
end; // BoardToFen

procedure TForm1.FenToGraphBoard( ReadString : string );
          var A, B, Col, Row : Integer;

          procedure WritePiece( Color : Integer; PString : string );
          begin
              sgBoard.Cells[ Col, Row ] := PString;
              PieceCol[ Col, Row ] := Color;
              Inc( Col );
          end; // SetPieceColor

begin // FenToGraphBoard
  for A := 0 to 7 do
  for B := 0 to 7 do
  begin
     sgBoard.Cells[ A, B ] := ' ';
  end;

  Col := 0; Row := 0; A := 0;
  repeat
  Inc( A );
  case ReadString[ A ] of
    '1' : Inc( Col );
    '2' : Col := Col + 2;
    '3' : Col := Col + 3;
    '4' : Col := Col + 4;
    '5' : Col := Col + 5;
    '6' : Col := Col + 6;
    '7' : Col := Col + 7;
    '8' : Col := Col + 8;
    'K' : WritePiece( WHITE, 'k');//'l' );
    'k' : WritePiece( BLACK, 'l' );
    'Q' : WritePiece( WHITE, 'q');//'w' );
    'q' : WritePiece( BLACK, 'w' );
    'R' : WritePiece( WHITE, 'r');//'t' );
    'r' : WritePiece( BLACK, 't' );
    'B' : WritePiece( WHITE, 'b');//'v' );
    'b' : WritePiece( BLACK, 'v' );
    'N' : WritePiece( WHITE, 'n');//'m' );
    'n' : WritePiece( BLACK, 'm' );
    'P' : WritePiece( WHITE, 'p');//'o' );
    'p' : WritePiece( BLACK, 'o' );
    '/' : begin  Col := 0;  Inc( Row );  end;
    else  Exit;
  end; // of case

  until( Col > 7 ) and ( Row >= 7 );
end; // FenToGraphBoard

procedure TForm1.GraphBoardToInternal;
begin

end; // GraphBoardToInternal
