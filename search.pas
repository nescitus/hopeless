constructor TSearch.Create;
begin
   inherited Create;
   DO_PVS     := TRUE;          IsFailLow := FALSE;
   DO_IID     := TRUE;          DISABLE_IID_FROM := 4;
   DO_FUT1    := TRUE;          FUT_1 := 100;
   DO_FUT2    := TRUE;          FUT_2 := 200;
   DO_FUT3    := TRUE;          FUT_3 := 300;
   NULL_IN_PV := TRUE;          ASP   := 50;
   DO_LMR     := TRUE;
   DO_STATIC  := FALSE;
   HASH_IN_PV := TRUE;
   DO_ASP     := FALSE;
end; // TSearch.Create

procedure DumpPath(Temp, Depth : Integer);
          var A : Integer;
          s : string;
begin
   for A := StackBound to StackIndex do
       s := s + MoveToStr(MoveStack[A]) + ' ';
end; // DumpPath

function IsMateScore( Val : Integer ) : Boolean;
begin
   if ( Val > MATE_VALUE - 100 )
   or ( Val < -MATE_VALUE + 100 )
      then Result := TRUE
      else Result := FALSE;
end; // IsMateScore

function InCheck : Boolean;
begin
 Result := IsAttackedBy( Board.Side xor 1, Board.List[ ListSide ].Sqr, TRUE )
end; // InCheck

// Function "NullOK" represents conditions in which it's
// safe to use null move pruning.

function TSearch.NullOK : Boolean;
begin
  if  ( Board.Index[WMat] + Board.Index[BMat] + 32 > 10 )
      then Result := TRUE
      else Result := FALSE;
end; // NullOK

function TSearch.PawnToSeventh( SqTo : Integer ) : Boolean;
begin
  if  ( Board.List[ Board.Index[ SqTo ] ].Kind = PAWN )
  and ( ( SqTo div 16 = ROW_2 ) or ( SqTo div 2 = ROW_7 ) )
      then Result := TRUE
      else Result := FALSE;
end; // PawnToSeventh

function IsNormalMove( Flag : Integer ) : Boolean;
begin
   if ( Flag = CAPT )
   or ( Flag = PROM )
   or ( Flag = CAPTPROM )
   or ( Flag = EPCAPT )
      then Result := FALSE
      else Result := TRUE;
end; // IsNormalMove

procedure TSearch.UpdateKillers( Ply, SqFr, SqTo : Integer );
begin
  if ( SqFr <> Killers[ Ply, 1 ] )
  or ( SqTo <> Killers[ Ply, 2 ] )
     then begin
       Killers[ Ply, 3 ] := Killers[ Ply, 1 ];
       Killers[ Ply, 4 ] := Killers[ Ply, 2 ];
       Killers[ Ply, 1 ] := SqFr;
       Killers[ Ply, 2 ] := SqTo;
     end;
end; // UpdateKillers

function TSearch.TimeOut : Boolean;
begin
  ETi := Time;
  if  ( SecondsBetween( ETi, STi ) >= MoveTime )
  and ( DepthFinished > 3 )
     then begin
       if  ( IsFailLow )
       and ( AddSec > 0 )
       then begin                        // After a fail-low it  may
         MoveTime := MoveTime + AddSec;  // pay off to check whether
         AddSec:= 0;          // there is any better move,
         Result := FALSE;     // so  the time is  doubled
         Exit;                // instead  of interrupting
       end;                   // search.
       Result := TRUE;
       NoMem := TRUE;

       if SecUsed > 10 * MoveTime // bugfix
          then begin
            STi := Time;      // At  midnight,  program  may  use
            Result := FALSE;  // double time. It's not a  perfect
            NoMem := FALSE;   // solution, but it will do.
          end;
     end
     else Result := FALSE;
end; // TimeOut

function TSearch.CanMove : Boolean;
         var A, LocalIndex : Integer;
         LocalList : TMoveSet;
begin
  Result := FALSE;
  GenMoves;
  LocalList := MoveSet;
  LocalIndex := MoveNo;
  for A := 1 to LocalIndex do
  begin
    MakeMove( LocalList[ A ] );
    GenCapt;
    if not KingEnPrise
       then Result := TRUE;
    UnMakeMove;
    if Result = TRUE
       then Exit;
  end;
end; // CanMove

function TSearch.IsStaleMate : Boolean;
begin
  GenOppMoves;
  if ( KingEnPrise )
  or ( CanMove )
     then Result := FALSE
     else Result := TRUE;
end; // IsStaleMate

function TSearch.IsCheckMate : Boolean;
begin
  GenOppMoves;
  if  ( KingEnPrise )
  and ( not CanMove )
      then Result := TRUE
      else Result := FALSE;
end; // IsCheckMate

function IsRep( Key : Integer ) : Boolean;
         var A : Integer;
begin
  Result := FALSE;
  for A := StackIndex - 1 downto 1 do
  begin // loop
    if RepStack[ A ] = Key
       then begin
         Result := TRUE;
         Exit;
       end;
  end; // loop
end; // IsRep

function TSearch.MinimalWindowSearch(Ply, Depth, Alfa, Beta, Target : Integer; WasNull: Boolean ) : Integer;
         var A, Temp, Best : Integer;
         TempEval : Integer;
         LocalMoveList : TMoveSet;
         Current, BestMove : TMove;
         LocalIndex : Integer;
         HTemp, HNo : Integer;
         InitialAlfa, NewDepth : Integer;
         QuietMoveCnt : Integer;
         bFutility  : Boolean;
         bRazoring  : Boolean;
         bInCheck   : Boolean;
         label TrueSearch;
         label prune;

procedure ChangeOrder;
          var A : Integer;
          HFr, HTo : Byte;
begin
   if MainHash[ HNo ].Key = HTemp
   then begin
     HFr := MainHash[ HNo ].SqFrom;
     HTo := MainHash[ HNo ].SqTo;
   end
   else begin
     HFr := OUTSIDE;
     HTo := OUTSIDE;
   end;

   for A := 1 to LocalIndex do
   with LocalMoveList[ A ] do begin  // loop

     if  ( SqFr = Killers[ Ply, 3 ] )
     and ( SqTo = Killers[ Ply, 4 ] )
         then Order := CAPT_SORT + 100;

     if  ( SqFr = Killers[ Ply, 1 ] )
     and ( SqTo = Killers[ Ply, 2 ] )
         then Order := CAPT_SORT + 101;

     if  ( SqFr = HFr )
     and ( SqTo = HTo )
         then Order := CAPT_SORT + CAPT_SORT;
   end; // loop
end; // ChangeOrder

procedure PickMove;
          var A, Max, Where : Integer;
begin
   Max   := Low( Integer );
   Where := 1;
   for A := 1 to LocalIndex do
   if  ( LocalMoveList[ A ].Order > Max )
   and ( LocalMoveList[ A ].Flag < NOFLAG )
   then begin
     Where := A;
     Max := LocalMoveList[ A ].Order;
   end;
   Current := LocalMoveList[ Where ];
   LocalMoveList[ Where ].Flag := NOFLAG;
end; // PickMove

begin  // Search

  bInCheck := IsAttackedBy( Board.Side, Board.List[ -ListSide ].Sqr, TRUE );

  if Depth <= Target
     then begin
      if bInCheck
         then begin
           Dec( Target );     // No  QS  if  in  check,  /
           goto TrueSearch;   // normal search instead.  /
         end;
      Result := Quiesc(Ply+1, Alfa, Beta, TRUE );
      ThinkLine[ Depth ] := '';
     end
     else begin
       TrueSearch:
       Inc( MyStat.Nodes );

       // 1. REPETITION DETECTION
       HTemp := InitHashValue;
       if IsRep( HTemp )
          then begin
            Result := 0;
            ThinkLine[Depth] := '[rep] ';
            Exit;
          end;
       RepStack[ StackIndex ] := HTemp;

       // 2. HASH RETRIEVAL
       HNo := GetHashAddr( HTemp );
       if  ( MainHash[ HNo ].Key = HTemp )
       and ( MainHash[ HNo ].Depth >= Depth )
       then begin
         {$IFDEF FULL_STATS} Inc( MyStat.HHits ); {$ENDIF}
         if ( MainHash[ HNo ].Flag = EXACT )
         then begin
           {$IFDEF FULL_STATS} Inc( MyStat.HCuts ); {$ENDIF}
           Result := MainHash[ HNo ].Val;
           if Result > Beta
              then Result := Beta;
           Exit;
         end;

         if MainHash[ HNo ].Flag = ABOVE_BETA
            then Alfa := Max( Alfa, MainHash[ HNo ].Val );

         if MainHash[ HNo ].Flag = BELOW_ALFA
            then Beta := Min( Beta, MainHash[ HNo ].Val );

         if Alfa >= Beta
            then begin
              {$IFDEF FULL_STATS} Inc( MyStat.HCuts ); {$ENDIF}
              Result := MainHash[ HNo ].Val;
              Exit;
            end;
       end; // retrieving hash value

       // Safeguard against reaching max ply limit

       if Ply >= MAX_DEPTH - 1
          then begin
            Result := MyEval.Eval( TRUE );
            Exit;
          end;

       if IsDraw
          then begin
            Result := 0;
            Exit;
          end;

       // 3. NULL MOVE
       if  ( Depth > Target + 2 )
       and ( not WasNull )
       and ( not bInCheck )
       and ( NullOK )
       then begin
          {$IFDEF FULL_STATS} Inc( MyStat.NullTries ); {$ENDIF}
          MakeNullMove;
          if Depth > 6
             then Temp := -MinimalWindowSearch(Ply+1, Depth-3, -Beta-1, -Beta+1, Target, TRUE )
             else Temp := -MinimalWindowSearch(Ply+1, Depth-2, -Beta-1, -Beta+1, Target, TRUE );
          UnMakeMove;
          if Temp >= Beta
             then begin
                {$IFDEF FULL_STATS} Inc( MyStat.NullCuts ); {$ENDIF}
                Result := Beta;
                Exit;
             end;
       end; // null move

       // 4. SETTING CONDITIONS FOR FUTILITY PRUNING
       bFutility := FALSE;
       bRazoring := FALSE;

       if  ( Depth = Target + 1 )
       and ( DO_FUT1 )
       and ( not IsMateScore( Alfa ) )
       then begin
         TempEval := MyEval.Eval( TRUE );
         if TempEval + FUT_1 <= Alfa
            then bFutility := TRUE;
       end; // Futility for Depth-1

       if  ( Depth = Target + 2 )
       and ( DO_FUT2 )
       and ( not IsMateScore( Alfa ) )
       then begin
         TempEval := MyEval.Eval( TRUE );
         if TempEval + FUT_2 <= Alfa
            then bFutility := TRUE;
       end; // Futility for Depth-2

       if  ( Depth = Target + 3 )
       and ( DO_FUT3 )
       and ( not IsMateScore( Alfa ) )
       then begin
         TempEval := MyEval.Eval( TRUE );
         if TempEval + FUT_3 <= Alfa
            then bFutility := TRUE;
       end; // Futility for Depth-3

       // 5. SEARCH PREPARATION
       Best := -INFINITY;
       InitialAlfa := Alfa;

       GenMoves;
       if KingEnPrise
          then begin
            Result := MATE_VALUE + Depth;
            Exit;
          end;

       LocalMoveList := MoveSet;
       LocalIndex := MoveNo;
       ChangeOrder;
       QuietMoveCnt := 0;

       // 6. SEARCH LOOP:
       for A := 1 to LocalIndex do
       begin // loop

         if  ( Depth > Target + 4 )
         and ( DepthFinished > 1 )
         then begin  // the clock
         if TimeOut  // the time is up
            then begin
              Result := -INFINITY;
              Exit;
            end
            else Application.ProcessMessages;
         end; // The clock

         PickMove;
         MakeMove( Current );
         if IsNormalMove(Current.Flag)
         then QuietMoveCnt := QuietMoveCnt + 1;

         // Extension and reduction heuristics

         // A. Pawn to 2nd/7th rank extended by 2 plies
         if PawnToSeventh( Current.SqTo )
         then NewDepth := Target - 2
         else begin
           // B. Check extension by 1 ply
           if  ( Target > -MaxExt )
           and ( InCheck )
           then begin  // check extension
             NewDepth := Target - 1;
             {$IFDEF FULL_STATS} Inc( MyStat.Checks ); {$ENDIF}
           end         // check extension
           else begin  // no extension, trying to reduce
             NewDepth := Target;

             // C. Futility pruning
             if  ( bFutility )
             and (not bInCheck)
             and ( A > 3 )
             and ( IsNormalMove( Current.Flag ) )
             then begin
                Temp := Alfa;
                {$IFDEF FULL_STATS} Inc( MyStat.FutCuts ); {$ENDIF}
                goto prune;
             end; // Futility pruning

             // D. Late move reduction
             if  ( DO_LMR )
             and ( Depth > Target + 1 )
             and ( not bInCheck )
             and ( IsNormalMove(Current.Flag) )
             and ( not IsAttackedBy( BLACK, WhiteKingLoc, TRUE ) )
             and ( not IsAttackedBy( WHITE, BlackKingLoc, TRUE ) )
             and ( QuietMoveCnt > 3 )
             then begin
               if  ( QuietMoveCnt > 6 )
               and ( Depth > Target + 2 )
                  then begin
                    Temp := -MinimalWindowSearch(Ply+1, Depth-1, -Beta, -Alfa, NewDepth+2, FALSE )
                  end
                  else Temp := -MinimalWindowSearch(Ply+1, Depth-1, -Beta, -Alfa, NewDepth+1, FALSE );
               {$IFDEF FULL_STATS} Inc( MyStat.LMReds ); {$ENDIF}
               if Temp < Alfa
               then goto prune;
             end; // Late move reduction

           end; // of reductions
         end;   // of extension/reduction block

         Temp := -MinimalWindowSearch(Ply+1, Depth-1, -Beta, -Alfa, NewDepth, FALSE );
         prune:
         UnmakeMove;

         if  ( Temp < -9000 ) // stalemate detection
         and ( IsStaleMate )
             then Temp := 0;

         if Temp > Best
            then begin
              Best := Temp;
              BestMove := Current;
           //   ThinkLine[ Depth ] := MoveToStr( Current )
           //                       + ' ' + ThinkLine[Depth - 1];
            end;

         if Best >= Beta
            then begin
              History.Update( Depth, Current.SqFr, Current.SqTo );

              if  ( IsNormalMove( Current.Flag ) )
              or  ( ( Current.Flag = CAPT ) and ( Current.Order < CAPT_SORT + 100 ) )
              then UpdateKillers( Ply, Current.SqFr, Current.SqTo );

              Break;
            end;

         if Best > Alfa
            then Alfa := Best;

       end; // loop

       ThinkLine[ Depth ] := '';
       Result := Best;
       WriteHash( HNo, HTemp, BestMove.SqFr, BestMove.SqTo, Depth, Best, InitialAlfa, Beta );
     end;
end; // MinimalWindowSearch

// Function "Search" does principal variation search (PVS)

function TSearch.Search(Ply, Depth, Alfa, Beta, Target : Integer; WasNull : Boolean ) : Integer;
         var A, Temp, Best : Integer;
         LocalMoveList : TMoveSet;
         Current, BestMove : TMove;
         LocalIndex : Integer;
         HTemp, HNo : Integer;
         InitialAlfa, NewDepth : Integer;
         label TrueSearch;

procedure ChangeOrder;
          var A : Integer;
          HFrom, HTo : Byte;
begin
   if MainHash[ HNo ].Key = HTemp
   then begin
     HFrom := MainHash[ HNo ].SqFrom;
     HTo   := MainHash[ HNo ].SqTo;
   end
   else begin
     HFrom := OUTSIDE;
     HTo   := OUTSIDE;
   end;

   for A := 1 to LocalIndex do
   with LocalMoveList[ A ] do begin  // loop

     if  ( SqFr = Killers[ Ply, 3 ] )
     and ( SqTo = Killers[ Ply, 4 ] )
         then Order := CAPT_SORT + 100;

     if  ( SqFr = Killers[ Ply, 1 ] )
     and ( SqTo = Killers[ Ply, 2 ] )
         then Order := CAPT_SORT + 101;

     if  ( SqFr = HFrom )
     and ( SqTo = HTo )
         then Order := CAPT_SORT + CAPT_SORT;
   end; // loop
end; // ChangeOrder

procedure PickMove;
          var A, Max, Where : Integer;
begin
   Max   := Low( Integer );
   Where := 1;
   for A := 1 to LocalIndex do
   if  ( LocalMoveList[ A ].Order > Max )
   and ( LocalMoveList[ A ].Flag < NOFLAG )
   then begin
     Where := A;
     Max := LocalMoveList[ A ].Order;
   end;
   Current := LocalMoveList[ Where ];
   LocalMoveList[ Where ].Flag := NOFLAG;
end; // PickMove

begin  // Search
  Temp := 0;
  if Depth = Target
     then begin
      if IsAttackedBy( Board.Side, Board.List[-ListSide].Sqr, TRUE )
         then begin
            Dec( Target );     // No  QS  if  in  check,  /
            goto TrueSearch;   // normal search instead.  /
         end;
      Result := Quiesc(Ply+1, Alfa, Beta, TRUE );
      ThinkLine[ Depth ] := '';
     end
     else begin
       TrueSearch:
       Inc( MyStat.Nodes );
       Inc( MyStat.PVNodes );

       // 1. REPETITION DETECTION:
       HTemp := InitHashValue;
       if IsRep( HTemp )
          then begin
            Result := 0;
            ThinkLine[ Depth ] := '[rep] ';
            Exit;
          end;
       RepStack[ StackIndex ] := HTemp;

       // 2. HASH RETRIEVAL:
       HNo := GetHashAddr( HTemp );
       if  ( HASH_IN_PV )
       and ( MainHash[ HNo ].Key = HTemp )
       then begin // found entry
         if ( MainHash[ HNo ].Depth >= Depth )
         then begin // correct depth
           {$IFDEF FULL_STATS} Inc( MyStat.HHits ); {$ENDIF}
           if ( MainHash[ HNo ].Flag = EXACT )
           then begin
             {$IFDEF FULL_STATS} Inc( MyStat.HCuts ); {$ENDIF}
             Result := MainHash[ HNo ].Val;
             if Result > Beta
                then Result := Beta;
             ThinkLine[Depth] := '[hash] ';
             Exit;
           end;

           if MainHash[ HNo ].Flag = ABOVE_BETA
              then Alfa := Max( Alfa, MainHash[ HNo ].Val );

           if MainHash[ HNo ].Flag = BELOW_ALFA
              then Beta := Min( Beta, MainHash[ HNo ].Val );

           if Alfa >= Beta
              then begin
                {$IFDEF FULL_STATS} Inc( MyStat.HCuts ); {$ENDIF}
                Result := MainHash[ HNo ].Val;
                ThinkLine[Depth] := '[hash]';
                Exit;
              end;
         end; // correct depth
       end // found entry
       else begin

       // No hash move found - do internal iterative    /
       // deepening. This means starting a shallower    /
       // search  in  order to create a  hash  entry    /
       // that will help in move ordering.              /

       if  ( DO_PVS )
       and ( DO_IID )
       and ( Depth > DISABLE_IID_FROM )
       and ( Ply < MAX_DEPTH - 1 )
           then begin
             {$IFDEF FULL_STATS} Inc( MyStat.IIDeep ); {$ENDIF}
             Search(Ply, Depth-2, Alfa, Beta, 0, FALSE );
           end;
       end;

       // Safeguard against reaching max ply limit

       if Ply >= MAX_DEPTH - 1
          then begin
            Result := MyEval.Eval( TRUE );
            Exit;
          end;

       if IsDraw
          then begin
            Result := 0;
            Exit;
          end;

       // 3. NULL MOVE:
       if  ( NULL_IN_PV )
       and ( Depth > Target + 2 )
       and ( not WasNull )
       and ( NullOK )
       then begin
          {$IFDEF FULL_STATS} Inc( MyStat.NullTries ); {$ENDIF}
          MakeNullMove;
          if Depth > 6
          then Temp := -MinimalWindowSearch(Ply+1, Depth-3, -Beta-1, -Beta+1, Target, TRUE )
          else Temp := -MinimalWindowSearch(Ply+1, Depth-2, -Beta-1, -Beta+1, Target, TRUE );
          UnMakeMove;

          if Temp >= Beta
             then begin
               {$IFDEF FULL_STATS} Inc( MyStat.NullCuts ); {$ENDIF}
               Result := Beta;
               Exit;
             end;
       end; // null move

       // 4. SEARCH PREPARATION
       Best := -INFINITY;
       InitialAlfa := Alfa;

       GenMoves;
       if KingEnPrise
          then begin
            Result := MATE_VALUE + Depth;
            Exit;
          end;

       LocalMoveList := MoveSet;
       LocalIndex    := MoveNo;
       ChangeOrder;

       // 5. SEARCH LOOP:
       for A := 1 to LocalIndex do
       begin // loop

         if  ( Depth > Target + 4 )
         and ( DepthFinished > 1 )
         then begin // the clock
         if TimeOut  // the time is up
            then begin
               Result := -INFINITY;
               Exit;
            end
            else Application.ProcessMessages;
         end; // The clock

         PickMove;
         MakeMove( Current );

         // Extension and reductions ordered by depth:

         // A. Pawn to 2nd/7th rank extended by 2 plies
         if  PawnToSeventh( Current.SqTo )
         then NewDepth := Target - 2
         else begin
           // B. Check extension by 1 ply
           if  ( Target > -MaxExt )
           and ( InCheck )
           then begin  // check extension
             NewDepth := Target - 1;
             {$IFDEF FULL_STATS} Inc( MyStat.Checks ); {$ENDIF}
           end         // check extension
           else begin
             NewDepth := Target;
           end; // end of (possible) reductions
         end;  // end of extension/reduction block

         // Program  is using  Principle  Variation  Search,
         // except  for  nodes near the  leafs.  This  means
         // doing  a  null-window  search  around  the  best
         // score found so far, and entering a normal window
         // search only if that fails high. Of course it is
         // not applicable while searching the first move.

         if ( not DO_PVS )
         or ( A = 1 )
         //or ( Depth > DISABLE_PVS_FROM )
         or ( -MinimalWindowSearch(Ply+1, Depth-1, -Best-1, -Best, NewDepth, FALSE ) > Best )
            then Temp := -Search(Ply+1, Depth-1, -Beta, -Alfa, NewDepth, FALSE );

         if  ( Temp < -9000 ) // stalemate detection
         and ( IsStaleMate )
             then Temp := 0;

         if Temp > Best
            then begin
              if  ( Form1.IsDumping )
              and ( DepthFinished = 7 )
              and ( Depth = 1 )
                  then DumpPath( Temp, Depth );
              Best := Temp;
              BestMove := Current;
              ThinkLine[ Depth ] := MoveToStr( Current )
                                  + ' ' + ThinkLine[Depth - 1];
            end;

         UnMakeMove;

         if Best >= Beta
            then begin
              History.Update( Depth, Current.SqFr, Current.SqTo );

              if  ( IsNormalMove(Current.Flag ) )
              or  ( ( Current.Flag = CAPT ) and ( Current.Order < CAPT_SORT + 100 ) )
              then UpdateKillers( Ply, Current.SqFr, Current.SqTo );

              Break;
           end;

        if Best > Alfa
           then Alfa := Best;
       end; // loop

       Result := Best;
       WriteHash( HNo, HTemp, BestMove.SqFr, BestMove.SqTo, Depth, Best, InitialAlfa, Beta );
     end;
end; // Search
