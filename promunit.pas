unit promunit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons;

type
  TPromForm = class(TForm)
    QButt: TSpeedButton;
    RButt: TSpeedButton;
    BButt: TSpeedButton;
    NButt: TSpeedButton;
    procedure QButtClick(Sender: TObject);
    procedure RButtClick(Sender: TObject);
    procedure BButtClick(Sender: TObject);
    procedure NButtClick(Sender: TObject);
  public
    Picked: Boolean;
    PromPiece: Integer;
  end; // TPromForm

var
  PromForm: TPromForm;

implementation

{$R *.dfm}

procedure TPromForm.QButtClick(Sender: TObject);
begin
  Picked := TRUE;
  PromPiece := 5; // QUEEN
  Hide;
end;

procedure TPromForm.RButtClick(Sender: TObject);
begin
  Picked := TRUE;
  PromPiece := 4; // ROOK
  Hide;
end;

procedure TPromForm.BButtClick(Sender: TObject);
begin
  Picked := TRUE;
  PromPiece := 3; // BISH
  Hide;
end;

procedure TPromForm.NButtClick(Sender: TObject);
begin
  Picked := TRUE;
  PromPiece := 2; // KNIGHT
  Hide;
end;

end.
