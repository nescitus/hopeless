
unit language;

interface

{$I version.inc}

type
  TLanguage = (POLISH, ENGLISH, FRENCH);
  
const
  LINE_END = #$D#$A;
  TEX: array[TLanguage, 0..62] of string = (
    ({$I polish.inc}),
    ({$I english.inc}),
    ({$I french.inc})
  );
  
implementation

end.
