# Hopeless

*Hopeless* is a Winboard chess engine written in Pascal by Pawel Koziol.

The engine has a built-in graphical interface, named *Clericus*.

*Clericus* is a Delphi project.

**Credits**

Clericus Chess GUI uses [Silk Icons](http://www.famfamfam.com/lab/icons/silk/).
