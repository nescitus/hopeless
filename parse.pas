
function IsNumber( Ch : Char ) : Boolean;
begin
  if (Ch='0') or (Ch='1') or (Ch='2') or (Ch='3')
  or (Ch='4') or (Ch='5') or (Ch='6') or (Ch='7')
  or (Ch='8') or (Ch='9') or (Ch ='-')
     then Result := TRUE
     else Result := FALSE;
end; // IsNumber

function ExtractNumStr( StrFrom : string ) : string;
         var A : Integer;
         NumberStarted : Boolean;
begin
  Result := '';
  NumberStarted := FALSE;
  for A := 1 to Length( StrFrom ) do
  begin  // loop
    if  IsNumber( StrFrom[A] )
       then begin
         if ( A = 1 )
         or ( StrFrom[ A-1 ] = ' ' )
         or ( StrFrom[ A-1 ] = '=' )
            then NumberStarted := TRUE;
         if NumberStarted
            then Result := Result + StrFrom[ A ];
       end
       else begin
         if NumberStarted
            then Exit;
       end;
  end; // loop
end; // ExtractNumStr

function ExtractNumVal( StrFrom : string ) : Integer;
         var TempString : string;
begin
     TempString := ExtractNumStr( StrFrom );
     Result := StrToInt( TempString );
end; // ExtractNumVal

function ExtractBracketStr( StrFrom : string ) : string;
         var A : Integer;
         StrStarted : Boolean;
begin
   Result := '';
   StrStarted := FALSE;
   for A := 1 to Length( StrFrom ) do
   begin
        if StrStarted
        then begin
           if StrFrom[ A ] = '>'
              then Exit
              else Result := Result + StrFrom[ A ];
        end;
        if StrFrom[ A ] = '<'
           then StrStarted := TRUE;
   end;
end; // ExtractBracketStr

procedure TForm1.ProcessInitString( CurrentString : string );

function FoundString( LookingFor : string ) : Boolean;
begin
    if AnsiContainsStr( CurrentString, LookingFor )
       then Result := TRUE
       else Result := FALSE;
end; // FoundString

begin
  if ( CurrentString[ 1 ] = ';' )
  or ( Length( CurrentString ) < 2 )
  or ( AnsiContainsStr( CurrentString, 'FILE_END' ) )
     then Exit;  // don't process the comment line

  // set graphical parameters
  {$IFNDEF XBOARD}
  (*
  if FoundString( 'LIGHT_SQUARES' )
     then LtSqrCol := ExtractNumVal( CurrentString );
  if FoundString( 'DARK_SQUARES' )
     then DarkSqrCol := ExtractNumVal( CurrentString );
  *)
  if FoundString( 'NOTATION' )
     then begin
       if FoundString( 'SAN' )
          then Notation := N_SAN;
       if FoundString( 'PGN' )
          then Notation := N_PGN;
     end;
  if FoundString( 'LANGUAGE' )
     then begin
       if FoundString( 'POLISH' )
          then vLang := POLISH;
       if FoundString( 'ENGLISH' )
          then vLang := ENGLISH;
       if FoundString( 'FRENCH' )
          then vLang := FRENCH;
     end;
  {$ENDIF}

  // set resigning conditions
  if FoundString( 'RESIGN_VALUE' )
     then RESIGN_VALUE := ExtractNumVal( CurrentString );
  if FoundString( 'RESIGN_BOARD_VAL' )
     then RESIGN_BOARD_VAL := ExtractNumVal( CurrentString );
  if FoundString( 'RESIGN_DURATION' )
     then RESIGN_DURATION := ExtractNumVal( CurrentString );

  // set engine preferences
  if FoundString( 'MAIN_BOOK_NAME' )
     then MainBookName := ExtractBracketStr( CurrentString );
  if FoundString( 'GUIDE_BOOK_NAME' )
     then begin
       GuideBookName := ExtractBracketStr( CurrentString );
       {$IFNDEF XBOARD}
       if ExtractBracketStr( CurrentString ) = 'books/MainBook.hop' then miBookNormal.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Broad.hop' then miBookBroad.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Hypermodern.hop' then miBookModern.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Gambit.hop'  then miBookGambit.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Fighter.hop' then miBookFighter.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Unusual.hop' then miBookUnusual.Checked := TRUE;
       if ExtractBracketStr( CurrentString ) = 'books/Solid.hop'   then miBookSolid.Checked := TRUE;
       {$ENDIF}
     end;

  if FoundString( 'STYLE_NAME' )
     then begin
       if FoundString('smart') then miSmart.Checked := TRUE;
       if FoundString('objective') then miObjective.Checked := TRUE;
     end;

  if FoundString( 'EVAL_GRAIN' )
     then MyEval.GRAIN := ExtractNumVal( CurrentString );

  if FoundString( 'IMBALANCE_CORRECTION' )
     then MyEval.IMBALANCE := ExtractNumVal( CurrentString );

  if FoundString( 'TEMPO' )
     then MyEval.TEMPO := ExtractNumVal( CurrentString );

  // piece values
  if FoundString( 'P_VALUE_N' )
     then MyEval.P_VALUE_N := ExtractNumVal( CurrentString );

  if FoundString( 'P_VALUE_DE' )
     then MyEval.P_VALUE_DE := ExtractNumVal( CurrentString );

  if FoundString( 'P_VALUE_AH' )
     then MyEval.P_VALUE_AH := ExtractNumVal( CurrentString );

  if FoundString( 'B_VALUE' )
     then MyEval.B_VALUE := ExtractNumVal( CurrentString );

  if  ( FoundString( 'N_VALUE' ) )
  and ( not FoundString( 'RESIGN_VALUE' ) ) // bugfix
     then MyEval.N_VALUE := ExtractNumVal( CurrentString );

  if FoundString( 'R_VALUE' )
     then MyEval.R_VALUE := ExtractNumVal( CurrentString );

  if FoundString( 'Q_VALUE' )
     then MyEval.Q_VALUE := ExtractNumVal( CurrentString );

  // booleans
  if FoundString( 'SCALE MATERIAL ON' )
     then MyEval.DoScaleMat := TRUE;

  if FoundString( 'SCALE MATERIAL OFF' )
     then MyEval.DoScaleMat := FALSE;

  if FoundString( 'OUTPOSTS ON')
     then MyEval.DoOutposts := TRUE;

  if FoundString( 'OUTPOSTS OFF')
     then MyEval.DoOutposts := FALSE;

  if FoundString( 'ADVANCED PAWN STRUCT ON' )
     then MyEval.DoPawnStructs := TRUE;

  if FoundString( 'ADVANCED PAWN STRUCT OFF' )
     then MyEval.DoPawnStructs := FALSE;

  if FoundString( 'COMBINATIONS ON' )
     then MyEval.DoPatterns := TRUE;

  if FoundString( 'COMBINATIONS OFF')
     then MyEval.DoPatterns := FALSE;

  // percents
  if FoundString( 'AGGRESSION' )
     then ProgAttack := ExtractNumVal( CurrentString );

  if FoundString( 'CAUTION' )
     then OppoAttack := ExtractNumVal( CurrentString );

  if FoundString( 'FREEDOM' )
     then ProgMobility := ExtractNumVal( CurrentString );

  if FoundString( 'RESTRAINT' )
     then OppoMobility := ExtractNumVal( CurrentString );

  if FoundString( 'MY_PAWN_SHIELD' )
     then ProgShield := ExtractNumVal( CurrentString );

  if FoundString( 'OPPO_PAWN_SHIELD' )
     then OppoShield := ExtractNumVal( CurrentString );

  if FoundString( 'P_DOUBLE' )
     then MyEval.P_DOUBLE := ExtractNumVal( CurrentString );

  if FoundString( 'P_WEAK' )
     then MyEval.P_WEAK := ExtractNumVal( CurrentString );

  if FoundString( 'P_BLOCKED_DE' )
     then MyEval.P_BLOCKED_DE := ExtractNumVal( CurrentString );

  if FoundString( 'P_PROTECTS_K' )
     then MyEval.P_PROTECTS_K := ExtractNumVal( CurrentString );

  if FoundString( 'P_DESERTED_K' )
     then MyEval.P_DESERTED_K := ExtractNumVal( CurrentString );

  if FoundString( 'P_RUNNING' )
     then MyEval.P_RUNNING := ExtractNumVal( CurrentString );

  if FoundString( 'KNIGHT_MOB_UNIT' )
     then MyEval.N_MOB_UNIT := ExtractNumVal( CurrentString );

  if FoundString( 'KNIGHT_TRAPPED' )
     then MyEval.N_TRAPPED := ExtractNumVal( CurrentString );

  if FoundString( 'KNIGHT HINDERS' )
     then MyEval.N_BLOCK_P := ExtractNumVal( CurrentString );

  if FoundString( 'B_PAIR' )
     then MyEval.B_PAIR := ExtractNumVal( CurrentString );

  if FoundString( 'B_PINS' )
     then MyEval.B_PIN := ExtractNumVal( CurrentString );

  if FoundString( 'B_TRAP_A7' )
     then MyEval.B_TRAP_A7 := ExtractNumVal( CurrentString );

  if FoundString( 'B_TRAP_A6' )
     then MyEval.B_TRAP_A6 := ExtractNumVal( CurrentString );

  if FoundString( 'B_NO_FIANCH' )
     then MyEval.B_NO_FIANCH := ExtractNumVal( CurrentString );

  if FoundString( 'B_CENT_MISS' )
     then MyEval.B_CENT_MISS := ExtractNumVal( CurrentString );

  if FoundString( 'B_UNSAFE' )
     then MyEval.B_UNSAFE := ExtractNumVal( CurrentString );

  if FoundString( 'R_OPEN' )
     then MyEval.R_OPEN := ExtractNumVal( CurrentString );

  if FoundString( 'R_HALF' )
     then MyEval.R_HALF := ExtractNumVal( CurrentString );

  if FoundString( 'R_PRESS_K' )
     then MyEval.R_PRESS_K := ExtractNumVal( CurrentString );

  if FoundString( 'R_PAIR' )
     then MyEval.R_PAIR := ExtractNumVal( CurrentString );

  if FoundString( 'Q_DIST_K' )
     then MyEval.Q_DIST_K := ExtractNumVal( CurrentString );

  if FoundString( 'Q_EARLY' )
     then MyEval.Q_EARLY := ExtractNumVal( CurrentString );

  if FoundString( 'K_BLOCK_R' )
     then MyEval.K_BLOCK_R := ExtractNumVal( CurrentString );

  if FoundString( 'K_UNCASTLED' )
     then MyEval.K_UNCASTLED := ExtractNumVal( CurrentString );

  if FoundString( 'K_COLOR_WEAK' )
     then MyEval.K_COLOR_WEAK := ExtractNumVal( CurrentString );

  if FoundString( 'N_ATTACKS_K' )
     then MyEval.N_ATTACKS_K := ExtractNumVal( CurrentString );

  if FoundString( 'B_ATTACKS_K' )
     then MyEval.B_ATTACKS_K := ExtractNumVal( CurrentString );

  if FoundString( 'R_ATTACKS_K' )
     then MyEval.R_ATTACKS_K := ExtractNumVal( CurrentString );

  if FoundString( 'Q_ATTACKS_K' )
     then MyEval.Q_ATTACKS_K := ExtractNumVal( CurrentString );

  // search options
  if FoundString( 'NULL_IN_PV ON' )
     then MySearch.NULL_IN_PV := TRUE;
  if FoundString( 'NULL_IN_PV OFF' )
     then MySearch.NULL_IN_PV := FALSE;

  if FoundString( 'IID ON' )
     then MySearch.DO_IID := TRUE;
  if FoundString( 'IID OFF' )
     then MySearch.DO_IID := FALSE;

  if FoundString( 'PVS ON' )
     then MySearch.DO_PVS := TRUE;
  if FoundString( 'PVS OFF' )
     then MySearch.DO_PVS := FALSE;

  if FoundString( 'LMR ON' )
     then MySearch.DO_LMR := TRUE;
  if FoundString( 'LMR OFF' )
     then MySearch.DO_LMR := FALSE;

  if FoundString( 'FUT_1' )
     then begin
       if FoundString( 'OFF' )
          then MySearch.DO_FUT1 := FALSE
          else begin
            MySearch.DO_FUT1 := TRUE;
            MySearch.FUT_1 := ExtractNumVal( CurrentString );
          end;
     end; // FUT_1

  if FoundString( 'FUT_2' )
     then begin
       if FoundString( 'OFF' )
          then MySearch.DO_FUT2 := FALSE
          else begin
            MySearch.DO_FUT2 := TRUE;
            MySearch.FUT_2 := ExtractNumVal( CurrentString );
          end;
     end; // FUT_2

  if FoundString( 'DISABLE_IID_FROM' )
     then MySearch.DISABLE_IID_FROM := ExtractNumVal( CurrentString );
  if FoundString( 'ASPIRATION_WINDOW' )
     then MySearch.ASP := ExtractNumVal( CurrentString );
end; // ProcessCurrentString

procedure TForm1.ReadIniFile( FileName : string );
          var CurrentString : string;
begin
   AssignFile( IniFile, FileName );
   if FileExists( FileName )
      then begin
        Reset( IniFile );
        while not Eof( IniFile ) do
        begin
          Readln( IniFile, CurrentString );
          CurrentString := TrimLeft( CurrentString );
          ProcessInitString( CurrentString );
        end;
        CloseFile( IniFile );
      end;
      {$IFNDEF XBOARD} RefreshBoardDisplay; {$ENDIF}
end; // ReadIniFile

procedure TForm1.WriteIniFile( FileName : string );
begin
   AssignFile( IniFile, FileName );
   Rewrite( IniFile );
   Writeln( IniFile, '; Clericus / Hopeless Chess initialization file');
   Writeln( IniFile, ';');

   {$IFNDEF XBOARD}
   Writeln( IniFile, ' LIGHT_SQUARES ' + IntToStr(LtSqrCol) );
   Writeln( IniFile, ' DARK_SQUARES '  + IntToStr(DarkSqrCol) );

   if Notation = N_PGN
      then Writeln( IniFile, ' NOTATION PGN' );
   if Notation = N_SAN
      then Writeln( IniFile, ' NOTATION SAN' );
   if vLang = POLISH
      then Writeln( IniFile, ' LANGUAGE POLISH' );
   if vLang = ENGLISH
      then Writeln( IniFile, ' LANGUAGE ENGLISH' );
   {$ENDIF}

   Writeln( IniFile, ';');
   Writeln( IniFile, '; EVALUATION SETTINGS' );
   Writeln( IniFile, ';');

   Writeln( IniFile, ' MAIN_BOOK_NAME <' + MainBookName + '>' );
   Writeln( IniFile, ' GUIDE_BOOK_NAME <' + GuideBookName + '>' );
   Writeln( IniFile, ';' );
   Writeln( IniFile, ' Q_VALUE ' + IntToStr( MyEval.Q_VALUE ) );
   Writeln( IniFile, ' R_VALUE ' + IntToStr( MyEval.R_VALUE ) );
   Writeln( IniFile, ' B_VALUE ' + IntToStr( MyEval.B_VALUE ) );
   Writeln( IniFile, ' N_VALUE ' + IntToStr( MyEval.N_VALUE ) );
   Writeln( IniFile, ' P_VALUE_N ' + IntToStr( MyEval.P_VALUE_N ) );
   Writeln( IniFile, ' P_VALUE_DE ' + IntToStr( MyEval.P_VALUE_DE ) );
   Writeln( IniFile, ' P_VALUE_AH ' + IntToStr( MyEval.P_VALUE_AH ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' EVAL_GRAIN ' + IntToStr( MyEval.GRAIN ) );
   Writeln( IniFile, ' IMBALANCE_CORRECTION ' + IntToStr( MyEval.IMBALANCE ) );
   Writeln( IniFile, ' TEMPO ' + IntToStr( MyEval.TEMPO ) );
   Writeln( IniFile, ';' );
   Writeln( IniFile, ' AGGRESSION ' + IntToStr( ProgAttack ) );
   Writeln( IniFile, ' CAUTION '    + IntToStr( OppoAttack ) );
   Writeln( IniFile, ' FREEDOM '    + IntToStr( ProgMobility ) );
   Writeln( IniFile, ' RESTRAINT '  + IntToStr( OppoMobility ) );
   Writeln( IniFile, ' MY_PAWN_SHIELD ' + IntToStr( ProgShield ) );
   Writeln( IniFile, ' OPPO_PAWN_SHIELD ' + IntToStr( OppoShield ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' P_DOUBLE ' + IntToStr( MyEval.P_DOUBLE ) );
   Writeln( IniFile, ' P_WEAK '   + IntToStr( MyEval.P_WEAK ) );
   Writeln( IniFile, ' P_BLOCKED_DE ' + IntToStr( MyEval.P_BLOCKED_DE ) );
   Writeln( IniFile, ' P_PROTECTS_K ' + IntToStr( MyEval.P_PROTECTS_K ) );
   Writeln( IniFile, ' P_DESERTED_K ' + IntToStr( MyEval.P_DESERTED_K ) );
   Writeln( IniFile, ' P_RUNNING ' + IntToStr( MyEval.P_RUNNING ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' KNIGHT_MOB_UNIT ' + IntToStr( MyEval.N_MOB_UNIT ) );
   Writeln( IniFile, ' KNIGHT_TRAPPED ' + IntToStr( MyEval.N_TRAPPED ) );
   Writeln( IniFile, ' KNIGHT_HINDERS_C ' + IntToStr( MyEval.N_BLOCK_P ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' B_PAIR ' + IntToStr( MyEval.B_PAIR ) );
   Writeln( IniFile, ' B_PINS ' + IntToStr( MyEval.B_PIN ) );
   Writeln( IniFile, ' B_TRAP_A7 '   + IntToStr( MyEval.B_TRAP_A7 ) );
   Writeln( IniFile, ' B_TRAP_A6 '   + IntToStr( MyEval.B_TRAP_A6 ) );
   Writeln( IniFile, ' B_NO_FIANCH ' + IntToStr( MyEval.B_NO_FIANCH ) );
   Writeln( IniFile, ' B_CENT_MISS ' + IntToStr( MyEval.B_CENT_MISS ) );
   Writeln( IniFile, ' B_UNSAFE ' + IntToStr( MyEval.B_UNSAFE ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' R_PAIR ' + IntToStr( MyEval.R_PAIR ) );
   Writeln( IniFile, ' R_OPEN ' + IntToStr( MyEval.R_OPEN ) );
   Writeln( IniFile, ' R_HALF ' + IntToStr( MyEval.R_HALF ) );
   Writeln( IniFile, ' R_PRESS_K ' + IntToStr( MyEval.R_PRESS_K ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' Q_DIST_K ' + IntToStr( MyEval.Q_DIST_K ) );
   Writeln( IniFile, ' Q_EARLY ' + IntToStr( MyEval.Q_EARLY ) );
   Writeln( IniFile, ';' );

   Writeln( IniFile, ' K_BLOCK_R ' + IntToStr( MyEval.K_BLOCK_R ) );
   Writeln( IniFile, ' K_UNCASTLED ' + IntToStr( MyEval.K_UNCASTLED ) );
   Writeln( IniFile, ' K_COLOR_WEAK ' + IntToStr( MyEval.K_COLOR_WEAK ) );
   Writeln( IniFile, ';' );
   Writeln( IniFile, ' N_ATTACKS_K ' + IntToStr( MyEval.N_ATTACKS_K ) );
   Writeln( IniFile, ' B_ATTACKS_K ' + IntToStr( MyEval.B_ATTACKS_K ) );
   Writeln( IniFile, ' R_ATTACKS_K ' + IntToStr( MyEval.R_ATTACKS_K ) );
   Writeln( IniFile, ' Q_ATTACKS_K ' + IntToStr( MyEval.Q_ATTACKS_K ) );
   Writeln( IniFile, ';');

   Writeln( IniFile, ' RESIGN_VALUE ' + IntToStr(RESIGN_VALUE) );
   Writeln( IniFile, ' RESIGN_BOARD_VAL ' + IntToStr(RESIGN_BOARD_VAL) );
   Writeln( IniFile, ' RESIGN_DURATION ' + IntToStr(RESIGN_DURATION) );
   Writeln( IniFile, ';');

   if MyEval.DoScaleMat = TRUE
      then Writeln( IniFile, ' SCALE MATERIAL ON' )
      else Writeln( IniFile, ' SCALE MATERIAL OFF ' );

   if MyEval.DoPawnStructs = TRUE
      then Writeln( IniFile, ' ADVANCED PAWN STRUCT ON ' )
      else Writeln( IniFile, ' ADVANCED PAWN STRUCT OFF ' );

   if MyEval.DoOutposts = TRUE
      then Writeln( IniFile, ' OUTPOSTS ON ' )
      else Writeln( IniFile, ' OUTPOSTS OFF ' );

   if MyEval.DoPatterns = TRUE
      then Writeln( IniFile, ' COMBINATIONS ON ' )
      else Writeln( IniFile, ' COMBINATIONS OFF ');

   Writeln( IniFile, ';' );
   Writeln( IniFile, '; Search parameters: ');
   Writeln( IniFile, ';' );

   if MySearch.NULL_IN_PV = TRUE
      then Writeln( IniFile, ' NULL_IN_PV ON' )
      else Writeln( IniFile, ' NULL_IN_PV OFF' );

   if MySearch.DO_IID
      then Writeln( IniFile, ' IID ON ' )
      else Writeln( IniFile, ' IID OFF ' );

   if MySearch.DO_PVS
      then Writeln( IniFile, ' PVS ON ' )
      else Writeln( IniFile, ' PVS OFF ' );

   if MySearch.DO_LMR
      then Writeln( IniFile, ' LMR ON ' )
      else Writeln( IniFile, ' LMR OFF ' );

   Writeln( IniFile, ' DISABLE_IID_FROM ' + IntToStr( MySearch.DISABLE_IID_FROM ) );

   if MySearch.DO_FUT1
      then Writeln( IniFile, ' FUT_1 ' + IntToStr( MySearch.FUT_1 ) )
      else Writeln( IniFile, ' FUT_1 OFF' );

   if MySearch.DO_FUT2
      then Writeln( IniFile, ' FUT_2 ' + IntToStr( MySearch.FUT_2 ) )
      else Writeln( IniFile, ' FUT_2 OFF' );

   Writeln( IniFile, ' ASPIRATION_WINDOW ' + IntToStr( MySearch.ASP ) );

   CloseFile( IniFile );
end; // WriteIniFile

procedure TForm1.RestoreSettings;
begin
   MyEval.Destroy;
   MyEval := TEval.Create;
   {$IFNDEF XBOARD}
   LtSqrCol      := clSilver;
   DarkSqrCol    := clMedGray;
   {$ENDIF}
   BookName      := 'MainBook.hop';
   GuideBookName := 'MainBook.hop';
   RESIGN_VALUE  := -900;
   RESIGN_BOARD_VAL := -500;
   RESIGN_DURATION  := 3;
   ProgMobility  := 100;
   OppoMobility  := 150;
   ProgAttack    := 200;
   OppoAttack    := 200;
   ProgShield    := 100;
   OppoShield    := 100;
   WriteIniFile('clericus.ini');
end; // RestoreSettings