object EvalForm: TEvalForm
  Left = 169
  Top = 355
  Caption = 'Engine parameters'
  ClientHeight = 494
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object KingPressLabel: TLabel
    Left = 5
    Top = 30
    Width = 97
    Height = 20
    Caption = 'King pressure'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object MobilityLabel: TLabel
    Left = 5
    Top = 55
    Width = 52
    Height = 20
    Caption = 'Mobility'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BishPairValLabel: TLabel
    Left = 400
    Top = 325
    Width = 52
    Height = 13
    Caption = 'Bishop pair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object DoublePawnValLabel: TLabel
    Left = 200
    Top = 200
    Width = 40
    Height = 13
    Caption = 'Doubled'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object PawnShieldLabel: TLabel
    Left = 5
    Top = 5
    Width = 84
    Height = 20
    Caption = 'Pawn shield'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object PawnValLabel: TLabel
    Left = 200
    Top = 125
    Width = 82
    Height = 13
    Caption = 'Pawn base value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object KnightValLabel: TLabel
    Left = 400
    Top = 125
    Width = 85
    Height = 13
    Caption = 'Knight base value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object QueenValLabel: TLabel
    Left = 580
    Top = 125
    Width = 87
    Height = 13
    Caption = 'Queen base value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookValLabel: TLabel
    Left = 200
    Top = 300
    Width = 81
    Height = 13
    Caption = 'Rook base value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BishValLabel: TLabel
    Left = 400
    Top = 300
    Width = 87
    Height = 13
    Caption = 'Bishop base value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookOpenLabel: TLabel
    Left = 200
    Top = 325
    Width = 42
    Height = 13
    Caption = 'Open file'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookHalfLabel: TLabel
    Left = 200
    Top = 350
    Width = 62
    Height = 13
    Caption = 'Half-open file'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object WeakPawnValLabel: TLabel
    Left = 200
    Top = 225
    Width = 29
    Height = 13
    Caption = 'Weak'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookPairLabel: TLabel
    Left = 200
    Top = 400
    Width = 46
    Height = 13
    Caption = 'Rook pair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BishPinLabel: TLabel
    Left = 400
    Top = 350
    Width = 47
    Height = 13
    Caption = 'Pin bonus'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label24: TLabel
    Left = 530
    Top = 325
    Width = 24
    Height = 24
    Caption = 'm'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label25: TLabel
    Left = 530
    Top = 415
    Width = 24
    Height = 24
    Caption = 'w'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label26: TLabel
    Left = 530
    Top = 385
    Width = 24
    Height = 24
    Caption = 't'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label27: TLabel
    Left = 530
    Top = 355
    Width = 24
    Height = 24
    Caption = 'v'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object AttackLabel: TLabel
    Left = 560
    Top = 300
    Width = 101
    Height = 20
    Caption = 'King attackers'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object CentPawnValLabel: TLabel
    Left = 200
    Top = 150
    Width = 62
    Height = 13
    Caption = 'Central pawn'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookPawnValLabel: TLabel
    Left = 200
    Top = 175
    Width = 55
    Height = 13
    Caption = 'Rook pawn'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object KnightMobLabel: TLabel
    Left = 400
    Top = 150
    Width = 55
    Height = 13
    Caption = 'Mobility unit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object GrainLabel: TLabel
    Left = 72
    Top = 378
    Width = 25
    Height = 13
    Caption = 'Grain'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RookPressKingLabel: TLabel
    Left = 200
    Top = 375
    Width = 72
    Height = 13
    Caption = 'Oppo'#39's king file'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object ImbalanceLabel: TLabel
    Left = 72
    Top = 402
    Width = 49
    Height = 13
    Caption = 'Imbalance'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object QKDistLabel: TLabel
    Left = 580
    Top = 150
    Width = 64
    Height = 13
    Caption = 'King distance'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object QEarlyLabel: TLabel
    Left = 580
    Top = 175
    Width = 93
    Height = 13
    Caption = 'Early developement'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object NUnstableLabel: TLabel
    Left = 400
    Top = 175
    Width = 81
    Height = 13
    Caption = 'Unstable position'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BUnstableLabel: TLabel
    Left = 400
    Top = 375
    Width = 81
    Height = 13
    Caption = 'Unstable position'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BMissLabel: TLabel
    Left = 400
    Top = 425
    Width = 86
    Height = 13
    Caption = 'Center color weak'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object RBlockedLabel: TLabel
    Left = 200
    Top = 425
    Width = 76
    Height = 13
    Caption = 'Blocked by king'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object NBlockPLabel: TLabel
    Left = 400
    Top = 200
    Width = 79
    Height = 13
    Caption = 'Blocking c pawn'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object BlockedDELab: TLabel
    Left = 200
    Top = 250
    Width = 39
    Height = 13
    Caption = 'Blocked'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object KColWeakLab: TLabel
    Left = 400
    Top = 450
    Width = 76
    Height = 13
    Caption = 'King color weak'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object SpoilFianchLabel: TLabel
    Left = 400
    Top = 400
    Width = 85
    Height = 13
    Caption = 'Spoiled fianchetto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object OppoShieldBar: TTrackBar
    Left = 400
    Top = 5
    Width = 193
    Height = 45
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 11
    OnChange = OppoShieldBarChange
  end
  object ProgShieldBar: TTrackBar
    Left = 200
    Top = 5
    Width = 193
    Height = 45
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 10
    OnChange = ProgShieldBarChange
  end
  object ProgAttBar: TTrackBar
    Left = 200
    Top = 30
    Width = 193
    Height = 45
    LineSize = 20
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 0
    OnChange = ProgAttBarChange
  end
  object ProgAttEdit: TEdit
    Left = 150
    Top = 30
    Width = 41
    Height = 21
    TabOrder = 1
    Text = 'ProgAttEdit'
  end
  object OppoAttBar: TTrackBar
    Left = 400
    Top = 30
    Width = 193
    Height = 45
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 2
    OnChange = OppoAttBarChange
  end
  object OppoAttEdit: TEdit
    Left = 600
    Top = 30
    Width = 41
    Height = 21
    TabOrder = 3
    Text = 'Edit1'
  end
  object ProgMobBar: TTrackBar
    Left = 200
    Top = 55
    Width = 193
    Height = 37
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 4
    OnChange = ProgMobBarChange
  end
  object ProgMobEdit: TEdit
    Left = 150
    Top = 55
    Width = 41
    Height = 21
    TabOrder = 5
    Text = 'Edit1'
  end
  object OppoMobBar: TTrackBar
    Left = 400
    Top = 55
    Width = 193
    Height = 33
    Max = 250
    Min = 50
    Frequency = 25
    Position = 100
    TabOrder = 6
    OnChange = OppoMobBarChange
  end
  object OppoMobEdit: TEdit
    Left = 600
    Top = 55
    Width = 41
    Height = 21
    TabOrder = 7
    Text = 'Edit1'
  end
  object BishPairEdit: TSpinEdit
    Left = 350
    Top = 325
    Width = 41
    Height = 22
    MaxValue = 75
    MinValue = 0
    TabOrder = 8
    Value = 0
    OnChange = BishPairEditChange
  end
  object DoubledEdit: TSpinEdit
    Left = 150
    Top = 200
    Width = 41
    Height = 22
    MaxValue = -1
    MinValue = -50
    TabOrder = 9
    Value = -1
    OnChange = DoubledEditChange
  end
  object OppoShieldEdit: TEdit
    Left = 600
    Top = 5
    Width = 41
    Height = 21
    TabOrder = 12
    Text = 'Edit1'
  end
  object PawnValEdit: TSpinEdit
    Left = 150
    Top = 125
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 13
    Value = 0
    OnChange = PawnValEditChange
  end
  object KnightValEdit: TSpinEdit
    Left = 350
    Top = 125
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 14
    Value = 0
    OnChange = KnightValEditChange
  end
  object QueenValEdit: TSpinEdit
    Left = 530
    Top = 125
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 15
    Value = 0
    OnChange = QueenValEditChange
  end
  object RookValEdit: TSpinEdit
    Left = 150
    Top = 300
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 16
    Value = 0
    OnChange = RookValEditChange
  end
  object BishValEdit: TSpinEdit
    Left = 350
    Top = 300
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 17
    Value = 0
    OnChange = BishValEditChange
  end
  object ScaleMatCheckBox: TCheckBox
    Left = 5
    Top = 125
    Width = 97
    Height = 17
    Hint = 
      'Program scales material values against the number of remaining p' +
      'awns'
    Caption = 'Scale material'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 18
    OnClick = ScaleMatCheckBoxClick
  end
  object CombinationCheckBox: TCheckBox
    Left = 5
    Top = 175
    Width = 97
    Height = 17
    Hint = 
      'Program recognizes certain attack patterns at the expense of spe' +
      'ed'
    Caption = 'Combinations'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 19
    OnClick = CombinationCheckBoxClick
  end
  object OutpostCheckBox: TCheckBox
    Left = 5
    Top = 150
    Width = 97
    Height = 17
    Caption = 'Outposts'
    TabOrder = 20
    OnClick = OutpostCheckBoxClick
  end
  object RookOpenEdit: TSpinEdit
    Left = 150
    Top = 325
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 21
    Value = 0
    OnChange = RookOpenEditChange
  end
  object RookHalfEdit: TSpinEdit
    Left = 150
    Top = 350
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 22
    Value = 0
    OnChange = RookHalfEditChange
  end
  object WeakPawnEdit: TSpinEdit
    Left = 150
    Top = 225
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 23
    Value = 0
    OnChange = WeakPawnEditChange
  end
  object RookPairEdit: TSpinEdit
    Left = 150
    Top = 400
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 24
    Value = 0
    OnChange = RookPairEditChange
  end
  object BishPinEdit: TSpinEdit
    Left = 350
    Top = 350
    Width = 41
    Height = 22
    MaxValue = 75
    MinValue = 0
    TabOrder = 25
    Value = 0
    OnChange = BishPinEditChange
  end
  object KnightAttBar: TTrackBar
    Left = 560
    Top = 325
    Width = 94
    Height = 30
    TabOrder = 26
    OnChange = KnightAttBarChange
  end
  object BishAttBar: TTrackBar
    Left = 560
    Top = 350
    Width = 94
    Height = 30
    TabOrder = 27
    OnChange = BishAttBarChange
  end
  object RookAttBar: TTrackBar
    Left = 560
    Top = 375
    Width = 94
    Height = 30
    TabOrder = 28
    OnChange = RookAttBarChange
  end
  object QueenAttBar: TTrackBar
    Left = 560
    Top = 400
    Width = 94
    Height = 30
    TabOrder = 29
    OnChange = QueenAttBarChange
  end
  object CentPawnValEdit: TSpinEdit
    Left = 150
    Top = 150
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 30
    Value = 0
    OnChange = CentPawnValEditChange
  end
  object RookPawnValEdit: TSpinEdit
    Left = 150
    Top = 175
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 31
    Value = 0
    OnChange = RookPawnValEditChange
  end
  object KnightMobEdit: TSpinEdit
    Left = 350
    Top = 150
    Width = 41
    Height = 22
    MaxValue = 3
    MinValue = 0
    TabOrder = 32
    Value = 0
    OnChange = KnightMobEditChange
  end
  object GrainEdit: TSpinEdit
    Left = 24
    Top = 350
    Width = 41
    Height = 22
    MaxValue = 50
    MinValue = 1
    TabOrder = 33
    Value = 1
    OnChange = GrainEditChange
  end
  object RookKingEdit: TSpinEdit
    Left = 150
    Top = 375
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 34
    Value = 0
    OnChange = RookKingEditChange
  end
  object Natt: TEdit
    Left = 660
    Top = 325
    Width = 33
    Height = 21
    TabOrder = 35
    Text = 'NAtt'
  end
  object BAtt: TEdit
    Left = 660
    Top = 350
    Width = 33
    Height = 21
    TabOrder = 36
    Text = 'Edit1'
  end
  object RAtt: TEdit
    Left = 660
    Top = 375
    Width = 33
    Height = 21
    TabOrder = 37
    Text = 'Edit1'
  end
  object Qatt: TEdit
    Left = 660
    Top = 400
    Width = 33
    Height = 21
    TabOrder = 38
    Text = 'Edit1'
  end
  object WoodEdit: TSpinEdit
    Left = 24
    Top = 400
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 39
    Value = 0
    OnChange = WoodEditChange
  end
  object KQDistEdit: TSpinEdit
    Left = 530
    Top = 150
    Width = 41
    Height = 22
    MaxValue = 5
    MinValue = 0
    TabOrder = 40
    Value = 0
    OnChange = KQDistEditChange
  end
  object QEarlyEdit: TSpinEdit
    Left = 530
    Top = 175
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 41
    Value = 0
    OnChange = QEarlyEditChange
  end
  object NUnstableEd: TSpinEdit
    Left = 350
    Top = 175
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 42
    Value = 0
    OnChange = NUnstableEdChange
  end
  object BUnstableEd: TSpinEdit
    Left = 350
    Top = 375
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 43
    Value = 0
    OnChange = BUnstableEdChange
  end
  object CentColWeakEd: TSpinEdit
    Left = 350
    Top = 425
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 44
    Value = 0
    OnChange = CentColWeakEdChange
  end
  object BlockedRookEd: TSpinEdit
    Left = 150
    Top = 425
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 45
    Value = 0
    OnChange = BlockedRookEdChange
  end
  object NBlockPEd: TSpinEdit
    Left = 350
    Top = 200
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 46
    Value = 0
    OnChange = NBlockPEdChange
  end
  object BlockedDEEd: TSpinEdit
    Left = 150
    Top = 250
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 47
    Value = 0
    OnChange = BlockedDEEdChange
  end
  object KColorWeakEd: TSpinEdit
    Left = 350
    Top = 450
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 48
    Value = 0
    OnChange = KColorWeakEdChange
  end
  object NoFianchEd: TSpinEdit
    Left = 350
    Top = 400
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 49
    Value = 0
    OnChange = NoFianchEdChange
  end
  object ProgShieldEdit: TEdit
    Left = 150
    Top = 5
    Width = 41
    Height = 21
    TabOrder = 50
    Text = 'ProgShieldEdit'
  end
end
