
function MoveToStr( AMove : TMove ) : string;
begin
   Result := SqrName[ AMove.SqFr ] + SqrName[ AMove.SqTo ];
end; // MoveToStr

function SqrToChar( Sqr : Integer ) : char;
begin
   if Board.Index[ Sqr ] > 0
      then Result := WPieceChar[ Board.List[ Board.Index[ Sqr ] ].Kind ]
      else Result := BPieceChar[ Board.List[ Board.Index[ Sqr ] ].Kind ]
end; // SqrToChar

function SecToClock( Sec : Integer ) : string;
         var Min : Integer;
         MinString, SecString : string;
begin
   Min := 0;
   Result := '00:00';
   while Sec > 59 do
   begin
     Sec := Sec - 60;
     Inc( Min );
   end;

   if Sec > 9 then SecString := IntToStr( Sec )
              else SecString := '0' + IntToStr( Sec );

   if Min > 9 then MinString := IntToStr( Min )
              else MinString := '0' + IntToStr( Min );

   if Min = 0 then Result := '00:' + SecString
              else Result := MinString + ':' + SecString;
end; // SecToClock

procedure DisplayPV( Depth, Best : Integer );
begin
   {$IFDEF XBOARD}
   Form1.WBThread.SendCommand( IntToStr(Depth) +' '+IntToStr(Best)+' '+IntToStr(SecUsed*100)+' '+IntToStr(MyStat.Nodes)+' '+ThinkLine[Depth], TRUE );
   {$ENDIF}
   LastLine := ThinkLine[ Depth ];
   LastVal := Best;
   {$IFNDEF XBOARD}
   Form1.AddToAnMemo( IntToStr( Depth ) + '. ' + LastLine + IntToStr( LastVal ) + LINE_END
                       + '   ' + IntToStr( MyStat.Nodes ) + ' nodes in ' + SecToClock( SecUsed ) + ' '
                       + MyStat.Nps( STi, ETi ) );
   {$ENDIF}
end; // DisplayPV
