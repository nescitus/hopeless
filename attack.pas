
function ScanFrom( Sqr, Color, Vect, Piece1, Piece2 : Integer ) : Boolean;
         var Here : Integer;
begin
  Result := FALSE;
  Here := Sqr + Vect;
  while Here and $88 = 0 do
  begin        
    if Board.Index[ Here ] <> 0
       then begin
         if Board.List[ Board.Index[ Here ] ].Color = Color
            then begin  // the above condition helps, as otherwise in case of a wrong colour a test for it would have been done twice
              if ( IsPiece(Color, Piece1, Here ) )
              or ( IsPiece(Color, Piece2, Here ) )
                 then Result := TRUE;
            end;
         Exit;
       end;
    Here := Here + Vect;
  end;
end; // ScanFrom

function IsAttackedBy( Color, Sqr : Integer; CountKing : Boolean ) : Boolean;
         var A : Integer;
begin
  Result := FALSE;

  if Color = WHITE
     then begin
       if ( IsPiece( WHITE, PAWN, Sqr + SE ) )
       or ( IsPiece( WHITE, PAWN, Sqr + SW ) )
          then begin Result := TRUE; Exit; end;
     end
     else begin
       if ( IsPiece( BLACK, PAWN, Sqr + NE ) )
       or ( IsPiece( BLACK, PAWN, Sqr + NW ) )
          then begin Result := TRUE; Exit; end;
     end;

  for A := 1 to 8 do
  begin
    if ( IsPiece( Color, KNIGHT, Sqr + KnightArray[ A ] ) )
    or ( ( CountKing) and ( IsPiece( Color, KING, Sqr + StarArray[ A ] ) ) )
       then begin Result := TRUE; Exit; end;
  end;

  if ( ScanFrom(Sqr, Color, NORTH, ROOK, QUEEN) )
  or ( ScanFrom(Sqr, Color, SOUTH, ROOK, QUEEN) )
  or ( ScanFrom(Sqr, Color, EAST, ROOK, QUEEN) )
  or ( ScanFrom(Sqr, Color, WEST, ROOK, QUEEN) )
  or ( ScanFrom(Sqr, Color, NE, BISH, QUEEN) )
  or ( ScanFrom(Sqr, Color, NW, BISH, QUEEN) )
  or ( ScanFrom(Sqr, Color, SE, BISH, QUEEN) )
  or ( ScanFrom(Sqr, Color, SW, BISH, QUEEN) )
     then Result := TRUE;
end; // IsAttackedBy
