
procedure GenTest;
          var A : Integer;
begin
   Writeln(' List of generated moves: ');
   GenMoves;
   for A := 1 to MoveNo do
   Write( MoveToStr( MoveSet[A] )+' ' );
   Writeln;
end; // GenTest

procedure SpeedTest;
          var A : Integer;
begin
  Writeln(' Testing move generation speed, please wait... ');
  STi := Time;
  for A := 1 to 10000000 do GenMoves;
  ETi := Time;
  Writeln ( SecToClock( SecondsBetween( STi, ETi ) ) );
end; // SpeedTest
