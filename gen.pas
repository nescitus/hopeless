
procedure AddMove( SqFr, SqTo, Flag : Integer );
begin
   Inc( MoveNo );
   MoveSet[ MoveNo ].SqFr := SqFr;
   MoveSet[ MoveNo ].SqTo := SqTo;
   MoveSet[ MoveNo ].Flag := Flag;
   MoveSet[ MoveNo ].Order := History.Value( SqFr, SqTo );
end; // AddMove

procedure AddCapt( SqFr, SqTo : Integer );
begin
   Inc( MoveNo );
   MoveSet[ MoveNo ].SqFr := SqFr;
   MoveSet[ MoveNo ].SqTo := SqTo;
   MoveSet[ MoveNo ].Flag := CAPT;
   MoveSet[ MoveNo ].Order := CAPT_SORT
            + Cap_Bonus[ Board.List[ Board.Index[SqTo] ].Kind ]
            - Att_Malus[ Board.List[ Board.Index[SqFr] ].Kind ];

   if Board.List[ Board.Index[SqTo] ].Kind = KING
      then KingEnPrise := TRUE;
end; // AddCapt

procedure AddProm( SqFr, SqTo, Piece, Flag : Integer );
begin
   Inc( MoveNo );
   MoveSet[ MoveNo ].SqFr := SqFr;
   MoveSet[ MoveNo ].SqTo := SqTo;
   MoveSet[ MoveNo ].Flag := Flag;
   MoveSet[ MoveNo ].Prom := Piece;
   MoveSet[ MoveNo ].Order := 1200000;
   if Board.List[ Board.Index[SqTo] ].Kind = KING
      then KingEnPrise := TRUE;
end; // AddProm

procedure KNMoves( A, SqFr : Integer; Vectors : TVect );
          var B : Integer;
          SqTo  : Integer;
begin
  for B := 1 to 8 do
  begin // B loop
     SqTo := SqFr + Vectors[ B ];
     if ( SqTo and $88 = 0 )
        then with Board do begin // is square
          if Index[ SqTo ] = 0
             then AddMove( SqFr, SqTo, NORM )
             else begin
               if List[ A ].Color <> List[ Index[ SqTo ] ].Color
                  then AddCapt( SqFr, SqTo );
             end;
        end;  // is square
  end; // B loop
end; // KNMoves

procedure KNCapt( A, SqFrom : Integer; Vectors : TVect );
          var B : Integer;
          SqTo  : Integer;
begin
  for B := 1 to 8 do
  begin // B loop
     SqTo := SqFrom + Vectors[ B ];
     if  ( SqTo and $88 = 0 )
     and ( Board.Index[ SqTo ] <> 0 )
     and ( Board.List[ A ].Color <> Board.List[ Board.Index[ SqTo ] ].Color )
         then AddCapt( SqFrom, SqTo );
  end; // B loop
end; // KNCapt

procedure SliderMoves( A, SqFrom, Vect : Integer );
          var SqTo : Integer;
begin
   SqTo := SqFrom + Vect;
   while ( SqTo and $88 = 0 ) do
   begin
      if Board.Index[ SqTo ] = 0
         then AddMove( SqFrom, SqTo, NORM )
         else begin
           if Board.List[ A ].Color <> Board.List[ Board.Index[ SqTo ] ].Color
               then AddCapt( SqFrom, SqTo );
           Break;
         end;
         SqTo := SqTo + Vect;
   end; // while loop
end; // SliderMoves

procedure SliderCapt( A, SqFr, Vect : Integer );
          var SqTo : Integer;
begin
   SqTo := SqFr + Vect;
   while ( SqTo and $88 = 0 ) do
   begin
      if Board.Index[ SqTo ] <> 0
         then begin
           if Board.List[ A ].Color <> Board.List[ Board.Index[ SqTo ] ].Color
               then AddCapt( SqFr, SqTo );
           Break;
         end;
         SqTo := SqTo + Vect;
   end; // while loop
end; // SliderCapt

procedure AddPromSeries( SqFr, SqTo, Flag : Integer );
begin
   AddProm( SqFr, SqTo, QUEEN, Flag );
   AddProm( SqFr, SqTo, KNIGHT, Flag );
   AddProm( SqFr, SqTo, ROOK, Flag );
   AddProm( SqFr, SqTo, BISH, Flag );
end; // AddPromSeries

procedure GenWhiteCastle;
begin
   if  ( IsEmpty( F1 ) )
   and ( IsEmpty( G1 ) )
   and ( Board.Index[ WKRMoved ] = 0 )
   and ( IsPiece( WHITE, ROOK, H1 ) )
   and ( not IsAttackedBy( BLACK, E1, TRUE ) )
   and ( not IsAttackedBy( BLACK, F1, TRUE ) )
       then AddMove( E1, G1, CASTLE);

   if  ( IsEmpty( D1 ) )
   and ( IsEmpty( C1 ) )
   and ( IsEmpty( B1 ) )
   and ( Board.Index[ WQRMoved ] = 0 )
   and ( IsPiece( WHITE, ROOK, A1 ) )
   and ( not IsAttackedBy( BLACK, E1, TRUE ) )
   and ( not IsAttackedBy( BLACK, D1, TRUE ) )
       then AddMove( E1, C1, CASTLE);
end; // GenWhiteCastle

procedure GenBlackCastle;
begin
   if  ( IsEmpty( F8 ) )
   and ( IsEmpty( G8 ) )
   and ( Board.Index[ BKRMoved ] = 0 )
   and ( IsPiece( BLACK, ROOK, H8 ) )
   and ( not IsAttackedBy( WHITE, E8, TRUE ) )
   and ( not IsAttackedBy( WHITE, F8, TRUE ) )
       then AddMove( E8, G8, CASTLE);

   if  ( IsEmpty( D8 ) )
   and ( IsEmpty( C8 ) )
   and ( IsEmpty( B8 ) )
   and ( Board.Index[ BQRMoved ] = 0 )
   and ( IsPiece( BLACK, ROOK, A8 ) )
   and ( not IsAttackedBy( WHITE, E8, TRUE ) )
   and ( not IsAttackedBy( WHITE, D8, TRUE ) )
       then AddMove( E8, C8, CASTLE );
end; // GenBlackCastle

procedure GenMoves;
          var A, LStart, LEnd : Integer;
          SqFrom : Integer;
begin
   KingEnPrise := FALSE;
   MoveNo := 0;

   if Board.Side = WHITE
      then begin
        LStart := 1;    // look at the white part /
        LEnd   := 16;   // of the move list       /

        if  ( WhiteKingLoc = E1 )
        and ( Board.Index[ WKMoved ] = 0 )
            then GenWhiteCastle;

        if Board.Ep <> OUTSIDE  // generate en passant captures
           then begin
              if IsPiece( WHITE, PAWN, Board.Ep + SW )
                 then AddMove( Board.Ep + SW, Board.Ep, EPCAPT );
              if IsPiece( WHITE, PAWN, Board.Ep + SE )
                 then AddMove( Board.Ep + SE, Board.Ep, EPCAPT );
           end;
      end // white only
      else begin
        LStart := -16; // look at the black part  /
        LEnd   := -1;  // of the move list        /

        if  ( BlackKingLoc = E8 )
        and ( Board.Index[ BKMoved ] = 0 )
        then GenBlackCastle;

        if Board.Ep <> OUTSIDE // e.p. captures
           then begin
              if IsPiece( BLACK, PAWN, Board.Ep + NW )
                 then AddMove( Board.Ep + NW, Board.Ep, EPCAPT );
              if IsPiece( BLACK, PAWN, Board.Ep + NE )
                 then AddMove( Board.Ep + NE, Board.Ep, EPCAPT );
            end;
      end; // black only

   for A := LStart to LEnd do
   if Board.List[ A ].Sqr <> OUTSIDE // piece not captured
      then begin // loop

      SqFrom := Board.List[ A ].Sqr;

      if Board.List[ A ].Kind = PAWN
         then begin // PAWN
           if Board.List[ A ].Color = WHITE
              then begin // white pawn
                if SqFrom > 95
                then begin // promotion zone
                   if  ( (SqFrom + NORTH) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NORTH ] = 0 )
                   then AddPromSeries( SqFrom, SqFrom + NORTH, PROM );

                   if  ( (SqFrom + NE) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NE ] < 0 )
                   then AddPromSeries( SqFrom, SqFrom + NE, CAPTPROM );

                   if  ( (SqFrom + NW) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NW ] < 0 )
                   then AddPromSeries( SqFrom, SqFrom + NW, CAPTPROM );
                end // promotion zone
                else begin // normal zone
                   if IsEmpty( SqFrom + NORTH )
                      then begin
                         AddMove( SqFrom, SqFrom + NORTH, NORM );
                         if  ( SqFrom < 24 )
                         and ( Board.Index[ SqFrom + NN ] = 0 )
                         then AddMove( SqFrom, SqFrom + NN, PAWNJUMP );
                      end;

                   if  ( ( SqFrom + NE ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NE ] < 0 )
                   then AddCapt( SqFrom, SQFrom + NE );

                   if  ( ( SqFrom + NW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NW ] < 0 )
                   then AddCapt( SqFrom, SQFrom + NW );
                end; // normal zone
              end // white pawn
              else begin // black pawn
                if SqFrom < 24
                then begin // promotion zone
                   if  ( ( SqFrom + SOUTH ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SOUTH ] = 0 )
                   then AddPromSeries( SqFrom, SqFrom + SOUTH, PROM );

                   if  ( ( SqFrom + SE ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SE ] > 0 )
                   then AddPromSeries( SqFrom, SqFrom + SE, CAPTPROM );

                   if  ( ( SqFrom + SW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SW ] > 0 )
                   then AddPromSeries( SqFrom, SqFrom + SW, CAPTPROM );
                end // promotion zone
                else begin // normal zone
                  if IsEmpty( SqFrom + SOUTH )
                     then begin
                       AddMove( SqFrom, SqFrom + SOUTH, NORM );
                       if  ( SqFrom > 95 )
                       and ( Board.Index[ SqFrom + SS ] = 0 )
                       then AddMove( SqFrom, SqFrom + SS, PAWNJUMP );
                     end;

                  if  ( ( SqFrom + SE ) and $88 = 0 )
                  and ( Board.Index[ SqFrom + SE ] > 0 )
                  then AddCapt( SqFrom, SQFrom + SE );

                   if  ( ( SqFrom + SW ) and $88 = 0 )
                  and ( Board.Index[ SqFrom + SW ] > 0 )
                  then AddCapt( SqFrom, SQFrom + SW );
                end; // normal zone
              end; // black pawn
         end  // PAWN
         else begin // non-pawn

         if Board.List[ A ].Kind = KNIGHT
            then KNMoves( A, SqFrom, KnightArray )
            else begin
            if Board.List[ A ].Kind = KING
            then KNMoves( A, SqFrom, StarArray )
            else begin // sliders
                 if ( Board.List[ A ].Kind = ROOK )
                 or ( Board.List[ A ].Kind = QUEEN )
                    then begin
                       SliderMoves( A, SqFrom, NORTH );
                       SliderMoves( A, SqFrom, SOUTH );
                       SliderMoves( A, SqFrom, EAST );
                       SliderMoves( A, SqFrom, WEST );
                    end;
                 if ( Board.List[ A ].Kind = BISH )
                 or ( Board.List[ A ].Kind = QUEEN )
                    then begin
                       SliderMoves( A, SqFrom, NE );
                       SliderMoves( A, SqFrom, NW );
                       SliderMoves( A, SqFrom, SE );
                       SliderMoves( A, SqFrom, SW );
                    end;
            end; // sliders
            end;
         end; // non-pawns
      end; // loop
end; // GenMoves

procedure GenCapt;
          var A, LStart, LEnd : Integer;
          SqFrom : Integer;
begin
   KingEnPrise := FALSE;
   MoveNo := 0;

   if Board.Side = WHITE
      then begin
        LStart := 1;     // look at the white part  /
        LEnd   := 16;    // of the move list        /
      end
      else begin
        LStart := -16;   // look at the black part  /
        LEnd   := -1;    // of the move list        /
      end;

   for A := LStart to LEnd do
   if Board.List[ A ].Sqr <> OUTSIDE // piece not captured
      then begin // loop
      SqFrom := Board.List[ A ].Sqr;
      if Board.List[ A ].Kind = PAWN
         then begin // PAWN
           if Board.List[ A ].Color = WHITE
              then begin // white pawn
                if SqFrom > 95
                then begin // promotion zone
                   if  ( ( SqFrom + NORTH ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NORTH ] = 0 )
                   then AddProm( SqFrom, SqFrom + NORTH, QUEEN, PROM );

                   if  ( ( SqFrom + NE ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NE ] < 0 )
                   then AddProm(SqFrom, SqFrom + NE, QUEEN, CAPTPROM );

                   if  ( ( SqFrom + NW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NW ] < 0 )
                   then AddProm( SqFrom, SqFrom + NW, QUEEN, CAPTPROM);
                end // promotion zone
                else begin // normal zone
                   if  ( ( SqFrom + NE ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NE ] < 0 )
                   then AddCapt( SqFrom, SQFrom + NE );

                   if  ( ( SqFrom + NW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + NW ] < 0 )
                   then AddCapt( SqFrom, SQFrom + NW );
                end; // normal zone
              end // white pawn
              else begin // black pawn
                if SqFrom < 15
                then begin // promotion zone
                   if  ( ( SqFrom + SOUTH ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SOUTH ] = 0 )
                   then AddProm( SqFrom, SqFrom + SOUTH, QUEEN, PROM );

                   if  ( ( SqFrom + SE ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SE ] > 0 )
                   then AddProm( SqFrom, SqFrom + SE, QUEEN, CAPTPROM );

                   if  ( ( SqFrom + SW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SW ] > 0 )
                   then AddProm( SqFrom, SqFrom + SW, QUEEN, CAPTPROM );
                end // promotion zone
                else begin // normal zone
                   if  ( (SqFrom + SE) and $88 = 0 )
                   and ( Board.Index[SqFrom + SE] > 0 )
                   then AddCapt( SqFrom, SQFrom + SE );

                   if  ( ( SqFrom + SW ) and $88 = 0 )
                   and ( Board.Index[ SqFrom + SW ] > 0 )
                   then AddCapt( SqFrom, SQFrom + SW );
                end; // normal zone
              end; // black pawn
         end  // PAWN
         else begin // non-pawn

         if Board.List[ A ].Kind = KNIGHT
            then KNCapt( A, SqFrom, KnightArray )
            else begin
            if Board.List[ A ].Kind = KING
            then KNCapt( A, SqFrom, StarArray )
            else begin // sliders
              if ( Board.List[ A ].Kind = ROOK )
              or ( Board.List[ A ].Kind = QUEEN )
                 then begin
                   SliderCapt( A, SqFrom, NORTH );
                   SliderCapt( A, SqFrom, SOUTH );
                   SliderCapt( A, SqFrom, EAST );
                   SliderCapt( A, SqFrom, WEST );
                 end;
              if ( Board.List[ A ].Kind = BISH )
              or ( Board.List[ A ].Kind = QUEEN )
                 then begin
                   SliderCapt( A, SqFrom, NE );
                   SliderCapt( A, SqFrom, NW );
                   SliderCapt( A, SqFrom, SE );
                   SliderCapt( A, SqFrom, SW );
                 end;
            end; // sliders
            end;
         end;
      end; // loop
end; // GenCapt

procedure GenOppMoves;
begin
  SwitchSide;
  GenMoves;
  SwitchSide;
end; // GenOppMoves
