unit main;

interface

//{$DEFINE XBOARD}
{$DEFINE FULL_STATS}

uses
  Classes, Forms, Dialogs, Messages, SysUtils, StrUtils, DateUtils, Math,
  Controls, StdCtrls, Grids, Types, Graphics, Menus, ExtCtrls, ComCtrls,
  ImgList, ToolWin, ImageList, Windows,
{$IFDEF XBOARD}
  WBUnit,
{$ENDIF}
  promunit,
  bookunit,
  language;

const
  INFINITY = 30000;
  MATE_VALUE = 10000;
  CAPT_SORT = 10000000;
  MAX_DEPTH = 36; // safety limit for search depth
  MOVE_LIMIT = 800; // maximal stack size
  BOOK_SIZE = 100000;
  COMM_SIZE = 5000; // max no. of opening book comments
  HASH_SIZE = 262144 * 8;

  // piece types
  PAWN = 1;
  KNIGHT = 2;
  BISH = 3;
  ROOK = 4;
  QUEEN = 5;
  KING = 6;

  OUTSIDE = 120; // square outside of the board
  WHITE = 1;
  BLACK = 0;

  NO_PROM = 9;
  END_MAT = -12;

  // square numbers according to 0x88 scheme
  A8 = 112; B8 = 113; C8 = 114; D8 = 115; E8 = 116; F8 = 117; G8 = 118; H8 = 119;
  A7 = 096; B7 = 097; C7 = 098; D7 = 099; E7 = 100; F7 = 101; G7 = 102; H7 = 103;
  A6 = 080; B6 = 081; C6 = 082; D6 = 083; E6 = 084; F6 = 085; G6 = 086; H6 = 087;
  A5 = 064; B5 = 065; C5 = 066; D5 = 067; E5 = 068; F5 = 069; G5 = 070; H5 = 071;
  A4 = 048; B4 = 049; C4 = 050; D4 = 051; E4 = 052; F4 = 053; G4 = 054; H4 = 055;
  A3 = 032; B3 = 033; C3 = 034; D3 = 035; E3 = 036; F3 = 037; G3 = 038; H3 = 039;
  A2 = 016; B2 = 017; C2 = 018; D2 = 019; E2 = 020; F2 = 021; G2 = 022; H2 = 023;
  A1 = 000; B1 = 001; C1 = 002; D1 = 003; E1 = 004; F1 = 005; G1 = 006; H1 = 007;

  // Row and column identifiers
  ROW_1 = A1 div 16;
  ROW_2 = A2 div 16;
  ROW_3 = A3 div 16;
  ROW_4 = A4 div 16;
  ROW_5 = A5 div 16;
  ROW_6 = A6 div 16;
  ROW_7 = A7 div 16;
  ROW_8 = A8 div 16;
  COL_A = A1 mod 16;
  COL_B = B1 mod 16;
  COL_C = C1 mod 16;
  COL_D = D1 mod 16;
  COL_E = E1 mod 16;
  COL_F = F1 mod 16;
  COL_G = G1 mod 16;
  COL_H = H1 mod 16;

  // slots in the empty part of the 0x88 board
  WKMoved = 8;
  WKRMoved = 10;
  WQRMoved = 11;
  BKMoved = 9;
  BKRMoved = 12;
  BQRMoved = 13;
  WPCount = 14;
  BPCount = 15; // no. of pawns for each side
  WNCount = 24;
  BNCount = 25; // no. of knights           .
  WBCount = 26;
  BBCount = 27; // no. of bishops           .
  WRCount = 28;
  BRCount = 29; // no. of rooks             .
  WQCount = 30;
  BQCount = 31; // no. of queens            .
  WMat = 40;
  BMat = 41; // artificial value for PIECE material

  // vectors
  NORTH = 16;
  NN = NORTH + NORTH;
  SOUTH = -16;
  SS = SOUTH + SOUTH;
  EAST = 1;
  WEST = -1;
  NE = 17;
  SW = -17;
  NW = 15;
  SE = -15;
  NNE = NN + EAST;
  NNW = NN + WEST;
  SSE = SS + EAST;
  SSW = SS + WEST;

  // move types
  NORM = 1;
  PAWNJUMP = 2;
  CAPT = 3;
  PROM = 4;
  CAPTPROM = 5;
  EPCAPT = 6;
  CASTLE = 7;
  NOFLAG = 8;

  EXACT = 1;
  BELOW_ALFA = 2;
  ABOVE_BETA = 3; // hash flags

  B_NORM = 0;
  B_BAD = 1;
  B_FUN = 2; // book flags
  B_EXCL = 3;
  B_DISL = 4;
  B_END = 5;

type
  TInd = -16 .. 16;
  TDat = array [0 .. 127] of Integer;
  TPieceVal = array [0 .. 6] of Integer;
  TDispArray = array [0 .. 7, 0 .. 7] of Integer;
  TCoef = array [0 .. 127, 0 .. 127] of Integer;
  TEndgame = (UNDEFINED, PAWN_END, BISH_END, BOC_END, KNIGHT_END, BN_END, KRPKR,
    KBPKX, ROOK_END);
  TS78 = string[78];

  THashRec = record
    Key: Integer;
    Depth: ShortInt;
    SqFrom: ShortInt;
    SqTo: ShortInt;
    Val: SmallInt;
    Flag: Byte;
  end; // THashRec

  TSmallHashRec = record
    Key: Integer;
    Val: SmallInt;
  end; // TSmallHashRec

  TFullBook = record
    Fen: TS78;
    Move: string[4];
    Freq: Integer; // frequency of move input
    Learn: Integer; // user-defined move value
  end; // TFullBook

  TPosComment = record
    Fen: TS78;
    Comment: string[255];
  end;

  TPiece = record // In TPiece "Kind" is piece
    Kind: TInd; // kind,  "Sqr" its location
    Sqr: ShortInt; // on the board ( or OUTSIDE
    Color: TInd; // if a piece has been  cap-
  end; // TPiece              // tured ). "Color"  is  set
  // at program initialization
  // and  never  changed  afterwards.  Though  it  can  be
  // inferred from the location on the piece list ( if < 0
  // then  a  piece  is black ), the  additional  variable
  // simplifies the move generation.

  TList = array [-16 .. 16] of TPiece;
  THash = array [0 .. HASH_SIZE] of THashRec;
  TSmallHash = array [0 .. HASH_SIZE] of TSmallHashRec;
  THashData = array [0 .. 127, 0 .. 1, 1 .. 6] of Integer;
  THashFile = file of THashData;

  TBoard = record
    Index: array [0 .. 127] of TInd; // piece location on the list or 0
    List: TList; // piece list
    Side: ShortInt; // side to move
    Ep: ShortInt; // en passant square
    MovesTo50: ShortInt; // counter for 50 move rule
  end; // TBoard

  TMove = record
    SqFr, SqTo: Byte; // move coordinates
    PROM: TInd; // promoted piece
    Flag: TInd; // move kind, see constants
    Order: Integer; // value used for sorting
  end; // TMove

  TMoveSet = array [1 .. 250] of TMove;
  TVect = array [1 .. 8] of Integer;
  TFileStat = array [-1 .. 9, 0 .. 1] of TInd;
  TTiming = (SEC_PER_MOVE, FIXED_DEPTH, MIN_PER_GAME, PERIODS);
  TNotation = (N_SAN, N_PGN);
  
{$I header.pas} // class definitions are stored there

  TForm1 = class(TForm)
    sgBoard: TStringGrid;
    tmApp: TTimer;
    mnApp: TMainMenu;
    miGame: TMenuItem;
    miTiming: TMenuItem;
    miDepth: TMenuItem;
    miTimePerMove: TMenuItem;
    miTimePerGame: TMenuItem;
    mi1Sec: TMenuItem;
    mi5Sec: TMenuItem;
    mi10Sec: TMenuItem;
    mi30Sec: TMenuItem;
    mi4Plies: TMenuItem;
    mi5Plies: TMenuItem;
    mi6Plies: TMenuItem;
    mi7Plies: TMenuItem;
    mi8Plies: TMenuItem;
    mi5Min: TMenuItem;
    mi10Min: TMenuItem;
    mi15Min: TMenuItem;
    mi30Min: TMenuItem;
    mmAnalyze: TMemo;
    mmGame: TMemo;
    miNew: TMenuItem;
    miQuit: TMenuItem;
    miView: TMenuItem;
    miStats: TMenuItem;
    miStart: TMenuItem;
    miStop: TMenuItem;
    miTurn: TMenuItem;
    miTools: TMenuItem;
    miBookEditor: TMenuItem;
    tbApp: TToolBar;
    ilButtons: TImageList;
    tbBack: TToolButton;
    tbBackAll: TToolButton;
    tbForward: TToolButton;
    tbForwardAll: TToolButton;
    tbBook: TToolButton;
    tbQuit: TToolButton;
    tbShow: TToolButton;
    tbInfo: TToolButton;
    tbEval: TToolButton;
    tbStart: TToolButton;
    tbNew: TToolButton;
    tbStop: TToolButton;
    tbStats: TToolButton;
    tbFlip: TToolButton;
    miSettings: TMenuItem;
    miRestore: TMenuItem;
    miName: TMenuItem;

    miEvalParamEditor: TMenuItem;    mmInfo: TMemo;
    miOpeningStyle: TMenuItem;
    miBookBroad: TMenuItem;
    miBookNormal: TMenuItem;
    miBookSolid: TMenuItem;
    miBookModern: TMenuItem;
    miBookGambit: TMenuItem;
    miBookFighter: TMenuItem;
    miBookUnusual: TMenuItem;
    miNoBook: TMenuItem;
    miLanguage: TMenuItem;
    miPolish: TMenuItem;
    miEnglish: TMenuItem;
    miPlayingStyle: TMenuItem;
    miObjective: TMenuItem;
    miSmart: TMenuItem;
    miPessimist: TMenuItem;
    miSearchParamEditor: TMenuItem;
    sgClock: TStringGrid;
    ilChessMen: TImageList;
    miSaveSettings: TMenuItem;
    miFrench: TMenuItem;
    procedure sgBoardDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure InvertPieceStruct;
    procedure sgBoardMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sgBoardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mi30SecClick(Sender: TObject);
    procedure mi10SecClick(Sender: TObject);
    procedure mi5SecClick(Sender: TObject);
    procedure mi1SecClick(Sender: TObject);
    procedure mi4PliesClick(Sender: TObject);
    procedure mi5PliesClick(Sender: TObject);
    procedure mi6PliesClick(Sender: TObject);
    procedure mi7PliesClick(Sender: TObject);
    procedure mi8PliesClick(Sender: TObject);
    procedure mi5MinClick(Sender: TObject);
    procedure mi10MinClick(Sender: TObject);
    procedure mi15MinClick(Sender: TObject);
    procedure mi30MinClick(Sender: TObject);
    procedure miNewClick(Sender: TObject);
    procedure miQuitClick(Sender: TObject);
    procedure miStatsClick(Sender: TObject);
    procedure miStartClick(Sender: TObject);
    procedure miTurnClick(Sender: TObject);
    procedure miStopClick(Sender: TObject);
    procedure tmAppTimer(Sender: TObject);
    procedure tbBackClick(Sender: TObject);
    procedure tbBackAllClick(Sender: TObject);
    procedure tbForwardClick(Sender: TObject);
    procedure tbForwardAllClick(Sender: TObject);
    procedure miBookEditorClick(Sender: TObject);
    procedure tbShowClick(Sender: TObject);
    procedure tbInfoClick(Sender: TObject);
    procedure tbEvalClick(Sender: TObject);
    procedure Savecurrent1Click(Sender: TObject);
    procedure miRestoreClick(Sender: TObject);
    procedure miEvalParamEditorClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure miBookNormalClick(Sender: TObject);
    procedure miBookBroadClick(Sender: TObject);
    procedure miBookSolidClick(Sender: TObject);
    procedure miBookModernClick(Sender: TObject);
    procedure miBookFighterClick(Sender: TObject);
    procedure miBookGambitClick(Sender: TObject);
    procedure miBookUnusualClick(Sender: TObject);
    procedure miPolishClick(Sender: TObject);
    procedure miEnglishClick(Sender: TObject);
    procedure miObjectiveClick(Sender: TObject);
    procedure miSmartClick(Sender: TObject);
    procedure miPessimistClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miSearchParamEditorClick(Sender: TObject);
    procedure miNoBookClick(Sender: TObject);
  private
    MoveMade: Boolean;
{$IFDEF XBOARD}
    WBThread: TWBThread;
{$ENDIF}
    procedure BookSelection(CurrentBook: string);
    procedure LanguageSettings;
    procedure UpdateNotation;
    function GameToSan: string;
    function MoveToPGN(Move: TMove): string;
    function GameToPGN: string;
    procedure RefreshBoardDisplay;
    procedure BeforeActualMove(Move: TMove);
    procedure ProcessInitString(CurrentString: string);
    procedure ReadIniFile(FileName: string);
    procedure WriteIniFile(FileName: string);
    procedure RestoreSettings;
    procedure SetProgSide;
  public
    InputMode: (imDragging, imClicking);
    IsDumping: Boolean;
    ProgMobility: Integer;
    OppoMobility: Integer;
    ProgAttack: Integer;
    OppoAttack: Integer;
    ProgShield: Integer;
    OppoShield: Integer;
    procedure InfoMessage(What: string);
    procedure AddToAnMemo(What: string);
    procedure FenToGraphBoard(ReadString: string);
    procedure GraphBoardToInternal;
    function BoardToFen: string;
    procedure Execute(Command: string);
    procedure AfterActualMove(Move: TMove);
{$IFDEF XBOARD}
    procedure WinboardMessage(var Msg: TMessage); message CW_WBMSG;
{$ENDIF}
  end; // TMainForm

var
  sProgramInfo: string = 'Clericus ' + CLERICUS_VERSION + LINE_END + 'a chess program by Pawel Koziol' + LINE_END + 'nescitus@o2.pl';
  sDrawByRep: string = 'Draw' + LINE_END + 'by repetition';
  sDrawBy50m: string = 'Draw' + LINE_END + '50 move rule';
  sDrawByMat: string = 'Draw' + LINE_END + 'insufficient material';
  sCheckmate: string = 'Checkmate!';
  sStalemate: string = 'Stalemate!';
  sBookSaving: string = 'Saving book...';
  sBookSaved: string = 'Book saved';
  sMoveBack: string = 'Taking back';
  sMoveFwd: string = 'Replaying';
  sFwdAll: string = 'Go to end';
  sBackAll: string = 'Go to start';
  IniFile: TextFile;
  MyStat: TStat;
  History: THistory;
  MyEval: TEval;
  MyBook: TBook;
  MySearch: TSearch;
  vLang: TLanguage = POLISH;
  ClockSide: Integer = WHITE;
  ProgSide: Integer = BLACK;
  SearchDepth: Integer = 7;
  Want_resign: Integer = 0;
  ProgPassive: Boolean = FALSE;
  ProgThinking: Boolean = FALSE;
  LastLine: string;
  LastVal: Integer;
  WMTime, BMTime, WTotTime, BTotTime: Integer;
  //LtSqrCol: Integer = clSilver;
  //DarkSqrCol: Integer = clGray;
  LtSqrCol: Integer = clCream;
  DarkSqrCol: Integer = clSkyBlue;
  Notation: TNotation = N_PGN;
  PlayerName: string = 'Player';
  ProgName: string = 'Clericus';
  MainBookName: string = 'books/MainBook.hop';
  GuideBookName: string = 'books/GuideBook.hop';
  BookName: string;
  CommentName: string = 'books/Comments.eng';
  RESIGN_VALUE: Integer = -900;
  RESIGN_BOARD_VAL: Integer = -500;
  RESIGN_DURATION: Integer = 3;
  PieceCol: TDispArray;
  StartCol, StartRow, EndCol, EndRow: Integer;
  ColName: array [0..7] of char = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
  RowName: array [0..7] of char = ('8', '7', '6', '5', '4', '3', '2', '1');
  KnightArray: TVect = (18, -18, 33, -33, 31, -31, 14, -14);
  StarArray: TVect = (NORTH, SOUTH, EAST, WEST, NE, SE, NW, SW);
  KingEnPrise: Boolean = FALSE;
  ThinkLine: array [-36 .. 36] of string;
  FileStat, EmptyFiles: TFileStat;
  HashData: THashData; // array of values used for hashing
  RandFile: THashFile; // file with those values
  MainHash: THash; // main hash table
  // book variables
  BookNo: Integer = 0;
  NofEntries: Integer;
  NofComments: Integer;
  BookArray: array [0 .. 1, 0 .. BOOK_SIZE] of TFullBook;
  CommentArray: array [0 .. BOOK_SIZE] of TPosComment;
  BookFile: file of TFullBook;
  CommentFile: file of TPosComment;
  OpeningMove: TMove;
  MissedOpening: Integer = 0;
  Board: TBoard;
  Command: string;
  GameStack: array[0..MOVE_LIMIT] of TBoard;
  MoveStack: array[0..MOVE_LIMIT] of TMove;
  RepStack: array[0..MOVE_LIMIT] of Integer;
  StackIndex: Integer = 0;
  StackBound: Integer = 0;
  MyMove: TMove;
  MoveNo: Integer;
  MoveSet: TMoveSet;
  Timing: TTiming = MIN_PER_GAME;
  STi, ETi: TDateTime;
  SecUsed, TimeUsed: Integer;
  AddSec: Integer;
  MoveTime: Integer = 10;
  BaseTime: Integer = 300;
  BaseInc: Integer;
  BaseMoves, MemMoves: Integer;
  MemTime: Integer;
  TimeDiv: Integer = 30;
  NoMem: Boolean = FALSE;
  WPieceChar: array [0 .. 6] of char = ('.', 'P', 'N', 'B', 'R', 'Q', 'K');
  BPieceChar: array [0 .. 6] of char = ('.', 'p', 'n', 'b', 'r', 'q', 'k');
  SqrName: array [0 .. 127] of string = (
    'a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1', '*', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
    'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8', '*', 'x', 'x', 'x', 'x', 'x', 'x', 'x'
  );
  SqrSym: TDat = (
    H1, G1, F1, E1, D1, C1, B1, A1, 0, 0, 0, 0, 0, 0, 0, 0,
    H2, G2, F2, E2, D2, C2, B2, A2, 0, 0, 0, 0, 0, 0, 0, 0,
    H3, G3, F3, E3, D3, C3, B3, A3, 0, 0, 0, 0, 0, 0, 0, 0,
    H4, G4, F4, E4, D4, C4, B4, A4, 0, 0, 0, 0, 0, 0, 0, 0,
    H5, G5, F5, E5, D5, C5, B5, A5, 0, 0, 0, 0, 0, 0, 0, 0,
    H6, G6, F6, E6, D6, C6, B6, A6, 0, 0, 0, 0, 0, 0, 0, 0,
    H7, G7, F7, E7, D7, C7, B7, A7, 0, 0, 0, 0, 0, 0, 0, 0,
    H8, G8, F8, E8, D8, C8, B8, A8, 0, 0, 0, 0, 0, 0, 0, 0
  );
  ArrToSq: array[0..7, 0..7] of Integer;
{$I params.pas}   // evaluation function parameters
  Form1: TForm1;
  gUsePictures: Boolean;

implementation

uses
  evaleditor, debugwindow, searched, log;

{$R *.dfm}

procedure TForm1.InfoMessage(What: string);
begin
  mmInfo.Font.Size := 16;
  mmInfo.Text := What;
  mmInfo.Repaint;
end; // InfoMessage

procedure TForm1.AddToAnMemo(What: string);
begin
  mmAnalyze.Text := What + LINE_END + mmAnalyze.Text;
end; // AddToAnMemo

{$I prep.pas}      // preparatory procedures
{$I basic.pas}     // simple and frequently used procedures
{$I hist.pas}      // a class encapsulating history table
{$I parse.pas}     // parsing the text strings/files
{$I disp.pas}      // writing the board in the text mode
{$I stat.pas}      // class responsible for statistics
{$I hash.pas}      // hash table routines
{$I attack.pas}    // testing if a square is attacked
{$I gen.pas}       // move generation
{$I pawnev.pas}    // pawn evaluation
{$I patterns.pas}  // attack pattern evaluation
{$I endgame.pas}   // special endgame evaluation
{$I eval.pas}      // general evaluation
{$I recognize.pas} // draw claim and interior node recognizers
{$I moves.pas}     // make/unmake move
{$I fen.pas}       // creating FEN strings
{$I book.pas}      // opening book handling
{$I quiesc.pas}    // quiescence search
{$I search.pas}    // main search
{$I root.pas}      // root search
{$I iterate.pas}   // iterative deepening
{$I test.pas}      // test routines
{$I timing.pas}    // deciding about thinking time

procedure InitBoard; // to be replaced after a FEN reader is created
begin
  TLog.Append('InitBoard');
  ClearBoard;
  Want_resign := 0;

  FillSqr(KING, E1, 1);
  FillSqr(KING, E8, -1);
  FillSqr(QUEEN, D1, 2);
  FillSqr(QUEEN, D8, -2);
  FillSqr(ROOK, A1, 3);
  FillSqr(ROOK, A8, -3);
  FillSqr(ROOK, H1, 4);
  FillSqr(ROOK, H8, -4);
  FillSqr(BISH, C1, 5);
  FillSqr(BISH, C8, -6);
  FillSqr(BISH, F1, 6);
  FillSqr(BISH, F8, -5);
  FillSqr(KNIGHT, B1, 7);
  FillSqr(KNIGHT, B8, -7);
  FillSqr(KNIGHT, G1, 8);
  FillSqr(KNIGHT, G8, -8);
  FillSqr(PAWN, A2, 9);
  FillSqr(PAWN, A7, -9);
  FillSqr(PAWN, B2, 10);
  FillSqr(PAWN, B7, -10);
  FillSqr(PAWN, C2, 11);
  FillSqr(PAWN, C7, -11);
  FillSqr(PAWN, D2, 12);
  FillSqr(PAWN, D7, -12);
  FillSqr(PAWN, E2, 13);
  FillSqr(PAWN, E7, -13);
  FillSqr(PAWN, F2, 14);
  FillSqr(PAWN, F7, -14);
  FillSqr(PAWN, G2, 15);
  FillSqr(PAWN, G7, -15);
  FillSqr(PAWN, H2, 16);
  FillSqr(PAWN, H7, -16);

  // set material counters - to be moved into FillSqr
  Board.Index[WPCount] := 8;
  Board.Index[BPCount] := 8; // - to be mo
  Board.Index[WNCount] := 2;
  Board.Index[BNCount] := 2;
  Board.Index[WBCount] := 2;
  Board.Index[BBCount] := 2;
  Board.Index[WRCount] := 2;
  Board.Index[BRCount] := 2;
  Board.Index[WQCount] := 1;
  Board.Index[BQCount] := 1;
  Board.Index[WMat] := -5;
  Board.Index[BMat] := -5;

  GameStack[0] := Board; // remember the starting position
end; // InitBoard

procedure SafeUndoMove;
begin
  TLog.Append('SafeUndoMove');
  if StackIndex > 0 then
    UnMakeMove;
end; // SafeUndoMove

procedure SafeRedoMove;
begin
  TLog.Append('SafeRedoMove');
  if StackIndex < StackBound then
  begin
    Inc(StackIndex);
    Board := GameStack[StackIndex];
  end;
end; // SafeRedoMove

function TForm1.GameToSan: string;
var
  A: Integer;
begin
  Result := '';
  for A := 1 to StackIndex do
  begin
    Result := Result + ' ' + MoveToStr(MoveStack[A]);
    if A mod 2 = 0 then
      Result := Result + LINE_END;
  end;
end; // GameToSan

function GetPieceLetter(Sqr: Integer): string;
begin
  if Board.List[Board.Index[Sqr]].Sqr <> OUTSIDE then
    Result := WPieceChar[Board.List[Board.Index[Sqr]].Kind]
  else
    Result := '';

  if Result = 'P' then
    Result := '';
end; // GetPieceLetter

function TForm1.MoveToPGN(Move: TMove): string;
var
  MoveType, Disambiguate: string;
  A: Integer;
begin
  if (IsEmpty(Move.SqTo)) and (Move.Flag <> EPCAPT) then
    MoveType := ''
  else
  begin
    MoveType := 'x';
    if IsPiece(Board.Side, PAWN, Move.SqFr) then
      MoveType := SqrName[Move.SqFr][1] + 'x';
  end;
  // Disambiguate PGN move in case
  Disambiguate := ''; // when two identical pieces can
  GenMoves; // move to single square.
  for A := 1 to MoveNo do
    if (MoveSet[A].SqTo = Move.SqTo) and (MoveSet[A].SqFr <> Move.SqFr) and
      (Board.List[Board.Index[Move.SqFr]].Kind <> PAWN) and
      ((Board.List[Board.Index[Move.SqFr]].Kind = Board.List[Board.
      Index[MoveSet[A].SqFr]].Kind)) then
    begin
      if SqrName[Move.SqFr][1] <> SqrName[MoveSet[A].SqFr][1] then
        Disambiguate := SqrName[Move.SqFr][1]
      else
        Disambiguate := SqrName[Move.SqFr][2];
    end;

  Result := GetPieceLetter(Move.SqFr) + Disambiguate + MoveType +
    SqrName[Move.SqTo];

  if Move.Flag = CASTLE // write castling
  then
  begin // move correctly
    if Move.SqTo mod 16 = COL_G then
      Result := '0-0'
    else
      Result := '0-0-0';
  end;
end; // MoveToPGN

function TForm1.GameToPGN: string;
var
  A, LastStackIndex: Integer;
begin
  TLog.Append('GameToPGN');
  Result := '';
  LastStackIndex := StackIndex;
  StackIndex := 1;
  SafeUndoMove;
  for A := 1 to LastStackIndex do
  begin // loop
    if (A mod 2 = 1) then
      Result := Result + IntToStr(A div 2 + 1) + '.';
    Result := Result + MoveToPGN(MoveStack[A]) + ' ';
    MakeMove(MoveStack[A]);
  end; // loop
end; // GameToPGN

procedure SwitchMoveColor;
begin
  ClockSide := ClockSide xor 1;
  if ClockSide = WHITE then
    WMTime := 0
  else
    BMTime := 0;
end; // SwitchMoveColor

procedure TForm1.BeforeActualMove(Move: TMove);
begin
  TLog.Append('BeforeActualMove');
  Form1.mmInfo.Font.Size := 8;
  if Notation = N_PGN then
    mmGame.Text := GameToPGN;
  if Board.Side = WHITE then
    mmGame.Text := mmGame.Text + IntToStr(StackIndex div 2 + 1) + '.';
  mmGame.Text := mmGame.Text + MoveToPGN(Move);
end; // BeforeActualMove

procedure TForm1.AfterActualMove(Move: TMove);
var
  AddStr: string; // Used whenever a move
begin // is made on the board,
  ClaimResult; // not during the search.
  RepStack[StackIndex] := InitHashValue;
  AddStr := MoveToStr(Move);
  if Board.Side = WHITE then
    AddStr := AddStr + LINE_END;
{$IFNDEF XBOARD}
  if Notation = N_SAN then
    mmGame.Text := mmGame.Text + ' ' + AddStr;
{$ENDIF}
  SwitchMoveColor;
  StackBound := StackIndex;
end; // AfterActualMove

procedure TForm1.SetProgSide;
begin
  ProgSide := Board.Side;

  if ProgSide = WHITE then
  begin
    WhiteAttack := ProgAttack;
    WhiteMobility := ProgMobility;
    WhiteShield := ProgShield;
    BlackAttack := OppoAttack;
    BlackMobility := OppoMobility;
    BlackShield := OppoShield;
  end else
  begin
    BlackAttack := ProgAttack;
    BlackMobility := ProgMobility;
    BlackShield := ProgShield;
    WhiteAttack := OppoAttack;
    WhiteMobility := OppoMobility;
    WhiteShield := OppoShield;
  end;
end; // SetProgSide

function GetOpeningMove: Boolean;
begin
  Result := FALSE;
  if MissedOpening > 5 then
    Exit;

  BookNo := 0;
  if not MyBook.IsFlag(MyBook.BookMoveString) then
  begin
    MyMove := OpeningMove;
    Result := TRUE;
  end
  else
  begin
    BookNo := 1;
    if not MyBook.IsFlag(MyBook.BookMoveString) then
    begin
      MyMove := OpeningMove;
      Result := TRUE;
    end;
  end;
end; // GetOpeningMove

procedure DoThinking;
begin
  TLog.Append('DoThinking');
  ProgThinking := TRUE; // set flag blocking certain user actions
  Form1.SetProgSide; // initialize asymmetric eval parameters
  UpdateTime;

  if not GetOpeningMove then
  begin
    Inc(MissedOpening);
    MySearch.Iterate(SearchDepth);
  end;

{$IFNDEF XBOARD}
  Form1.BeforeActualMove(MyMove);
  Form1.mmGame.Refresh;
{$ENDIF}
  MakeMove(MyMove);

{$IFDEF XBOARD}
  Form1.WBThread.SendMove(MoveToStr(MyMove));
{$ENDIF}
  Form1.AfterActualMove(MyMove);
  ProgThinking := FALSE;
end; // DoThinking

function ParseMove(Command: string; FromBoard: Boolean): Boolean;
var
  MoveRead: TMove;
begin
  Result := FALSE;
  MoveRead := StrToMove(Command);
  if MoveRead.SqFr <> OUTSIDE then
  begin
    Result := TRUE;
    if (MoveRead.Flag = PROM) or (MoveRead.Flag = CAPTPROM) then
    begin
      MoveRead.PROM := QUEEN;
      if FromBoard = FALSE then
      begin
        if Length(Command) > 4 then
        begin // perhaps opponent underpromotes
          case Command[5] of
            'n':
              MoveRead.PROM := KNIGHT;
            'r':
              MoveRead.PROM := ROOK;
            'b':
              MoveRead.PROM := BISH;
          end; // of case
        end;
      end
      else
      begin // read promotion in graphic mode
        PromForm.Show;
        PromForm.Picked := FALSE;
        repeat
          Application.ProcessMessages
        until PromForm.Picked = TRUE;
        MoveRead.PROM := PromForm.PromPiece;
      end;
    end;
{$IFNDEF XBOARD} Form1.BeforeActualMove(MoveRead); {$ENDIF}
    MakeMove(MoveRead);
    Form1.AfterActualMove(MoveRead);
  end;
end; // ParseMove

procedure ActivateProgram;
begin
  ProgPassive := FALSE;
  ProgSide := Board.Side;
  DoThinking;
end; // ActivateProgram

procedure TForm1.Execute(Command: string);
begin
  if AnsiContainsStr(Command, 'go') and (StackIndex <> 2) then
    ActivateProgram;
  if AnsiContainsStr(Command, 'new') then
  begin
    PrepareAll;
    History.ClearTable;
    InitBoard;
  end;
  if AnsiContainsStr(Command, 'level ') then
    SetLevel(Command);
  if AnsiContainsStr(Command, 'list') then
    GenTest;
  if AnsiContainsStr(Command, 'sd ') and (ExtractNumStr(Command) <> '') then
  begin
    SearchDepth := StrToInt(ExtractNumStr(Command));
    MoveTime := 12 * 3600;
    Timing := FIXED_DEPTH;
  end; // "sd" command
  if AnsiContainsStr(Command, 'st ') and (ExtractNumStr(Command) <> '') then
  begin
    MoveTime := StrToInt(ExtractNumStr(Command));
    SearchDepth := 28;
    Timing := SEC_PER_MOVE;
  end; // "st" command
  if AnsiContainsStr(Command, 'stest') then
    SpeedTest;
  if AnsiContainsStr(Command, 'stop') then
    ProgPassive := TRUE;
  if AnsiContainsStr(Command, 'undo') then
    SafeUndoMove;
  if AnsiContainsStr(Command, 'quit') then
    Application.Terminate;
  if (Length(Command) >= 4) // check if user has entered a move
    and (ParseMove(Command, FALSE)) and (not ProgPassive) then
    DoThinking;
end; // Execute

{$IFDEF XBOARD}
procedure TForm1.WinboardMessage(var Msg: TMessage);
begin
  Execute(WBThread.WinboardCommand);
end; // WinBoardMessage
{$ENDIF}

procedure LoadBook;
begin
  BookNo := 0;
  BookName := GuideBookName;
  MyBook.ReadBookFile;
  BookNo := 1;
  BookName := MainBookName;
  MyBook.ReadBookFile;
end; // LoadBook

procedure TForm1.LanguageSettings;
begin
  miGame.Caption := TEX[vLang, 0];
  miTiming.Caption := TEX[vLang, 1];
  miView.Caption := TEX[vLang, 2];
  miSettings.Caption := TEX[vLang, 3];
  miTools.Caption := TEX[vLang, 4];
  miNew.Caption := TEX[vLang, 5];
  miStop.Caption := TEX[vLang, 6];
  miStart.Caption := TEX[vLang, 7];
  miQuit.Caption := TEX[vLang, 8];
  miDepth.Caption := TEX[vLang, 9];
  miTimePerMove.Caption := TEX[vLang, 10];
  miTimePerGame.Caption := TEX[vLang, 11];
  miTurn.Caption := TEX[vLang, 12];
  miStats.Caption := TEX[vLang, 13];
  miBookEditor.Caption := TEX[vLang, 14];
  miEvalParamEditor.Caption := TEX[vLang, 15];
  miSearchParamEditor.Caption := TEX[vLang, 16];
  (* *)
  miName.Caption := TEX[vLang, 18];
  miSaveSettings.Caption := TEX[vLang, 19];
  miRestore.Caption := TEX[vLang, 20];
  miOpeningStyle.Caption := TEX[vLang, 21];
  miBookModern.Caption := TEX[vLang, 22];
  miBookBroad.Caption := TEX[vLang, 23];
  miBookSolid.Caption := TEX[vLang, 24];
  miBookGambit.Caption := TEX[vLang, 25];
  miBookNormal.Caption := TEX[vLang, 26];
  miBookFighter.Caption := TEX[vLang, 27];
  miBookUnusual.Caption := TEX[vLang, 28];
  miNoBook.Caption := TEX[vLang, 29];
  miPlayingStyle.Caption := TEX[vLang, 30];
  miSmart.Caption := TEX[vLang, 31];
  miObjective.Caption := TEX[vLang, 32];
  miPessimist.Caption := TEX[vLang, 33];
  miLanguage.Caption := TEX[vLang, 34];
  sgClock.Cells[0, 0] := TEX[vLang, 35];
  sgClock.Cells[1, 0] := TEX[vLang, 36];
  tbNew.Hint := TEX[vLang, 37];
  tbFlip.Hint := TEX[vLang, 38];
  tbStop.Hint := TEX[vLang, 39];
  tbBackAll.Hint := TEX[vLang, 40];
  tbBack.Hint := TEX[vLang, 41];
  tbForward.Hint := TEX[vLang, 42];
  tbForwardAll.Hint := TEX[vLang, 43];
  tbStart.Hint := TEX[vLang, 44];
  tbEval.Hint := TEX[vLang, 45];
  tbBook.Hint := TEX[vLang, 46];
  tbShow.Hint := TEX[vLang, 47];
  tbStats.Hint := TEX[vLang, 48];
  tbInfo.Hint := TEX[vLang, 49];
  tbQuit.Hint := TEX[vLang, 50];
  sProgramInfo := TEX[vLang, 51];
  sDrawByRep := TEX[vLang, 52];
  sDrawBy50m := TEX[vLang, 53];
  sDrawByMat := TEX[vLang, 54];
  sCheckmate := TEX[vLang, 55];
  sStalemate := TEX[vLang, 56];
  sBookSaving := TEX[vLang, 57];
  sBookSaved := TEX[vLang, 58];
  sMoveBack := TEX[vLang, 59];
  sMoveFwd := TEX[vLang, 60];
  sFwdAll := TEX[vLang, 61];
  sBackAll := TEX[vLang, 62];
end; // LanguageSettings

procedure TForm1.sgBoardDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
const
  T = 'ponmbvrtqwkl';
var
  S: string;
  i: integer;
begin
  with sgBoard do
  begin
    S := Cells[ACol, ARow];
    if gUsePictures then
    begin
      if (ACol + ARow) and 1 = 0 then
        Canvas.Brush.Color := LtSqrCol
      else
        Canvas.Brush.Color := DarkSqrCol;
      Canvas.FillRect(Rect);
      i := Pred(Pos(S[1], T));
      if i >= 0 then
        ilChessMen.Draw(Canvas, Rect.Left, Rect.Top, i);
    end else
    begin
      Canvas.Brush.Color := LtSqrCol;
      Canvas.Font.Name := 'Chess Usual';
      Canvas.Font.Height := 40;
      if (ACol + ARow) and 1 = 1 then
        if S = ' ' then
          S := '+'
        else
          S := UpperCase(S);
      //Canvas.TextOut(Rect.Left, Rect.Top, S);
      DrawText(Canvas.Handle, PChar(S), Length(S), Rect,
        DT_CENTER or DT_NOPREFIX or DT_VCENTER or DT_SINGLELINE);
    end;
  end; // with sgBoard
end; // BoardGridDrawCell

procedure TForm1.InvertPieceStruct;
var
  A, B: Integer;
  TempIntArray: TDispArray;
  TempStrArray: array [0 .. 7, 0 .. 7] of string;
begin
  for A := 0 to 7 do
    for B := 0 to 7 do
    begin
      TempIntArray[A, B] := PieceCol[7 - A, 7 - B];
      TempStrArray[A, B] := sgBoard.Cells[7 - A, 7 - B];
    end;

  PieceCol := TempIntArray; // invert sgBoard display
  for A := 0 to 7 do
    for B := 0 to 7 do
      sgBoard.Cells[A, B] := TempStrArray[A, B];
end; // InvertPieceStruct

procedure TForm1.RefreshBoardDisplay;
begin
  FenToGraphBoard(BoardToFen);
  if miTurn.Checked then
    InvertPieceStruct;
  if miBookEditor.Checked then
    BookEditor.RefreshForm;
  sgBoard.Repaint;
end; // RefreshBoardDisplay

procedure TForm1.sgBoardMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  FenString, MoveString: string;
begin
  if InputMode = imDragging then
  begin
    sgBoard.MouseToCell(X, Y, StartCol, StartRow);
    if miTurn.Checked then
    begin
      StartCol := 7 - StartCol;
      StartRow := 7 - StartRow;
    end;
  end else
  begin // InputMode = imClicking
    sgBoard.MouseToCell(X, Y, EndCol, EndRow);
    if miTurn.Checked then
    begin
      EndCol := 7 - EndCol;
      EndRow := 7 - EndRow;
    end;
    FenString := BoardToFen;

    MoveString := ColName[StartCol] + RowName[StartRow] + ColName[EndCol] +
      RowName[EndRow];
    if ParseMove(MoveString, TRUE) then
    begin
      MoveMade := TRUE;
      if BookEditor.miPopulate.Checked then
        MyBook.WriteBookmove(FenString, MoveString);
      RefreshBoardDisplay;
      if not ProgPassive then
      begin
        ActivateProgram;
        RefreshBoardDisplay;
      end; // not ProgPassive
    end;
  end;
end; // BoardGridMouseDown

procedure TForm1.sgBoardMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  FenString, MoveString: String;
begin
  sgBoard.MouseToCell(X, Y, EndCol, EndRow);

  if miTurn.Checked then
  begin
    EndCol := 7 - EndCol;
    EndRow := 7 - EndRow;
  end;

  if (StartCol = EndCol) and (StartRow = EndRow) then
  begin
    InputMode := imClicking;
    Exit;
  end
  else
    InputMode := imDragging;

  FenString := BoardToFen;
  MoveString := ColName[StartCol] + RowName[StartRow] + ColName[EndCol] +
    RowName[EndRow];

  if ParseMove(MoveString, TRUE) then
  begin
    MoveMade := TRUE;
    if BookEditor.miPopulate.Checked then
      MyBook.WriteBookmove(FenString, MoveString);
    RefreshBoardDisplay;
    if not ProgPassive then
    begin
      ActivateProgram;
      RefreshBoardDisplay;
    end; // not ProgPassive
  end;
end; // BoardGridMouseUp

procedure TForm1.mi30SecClick(Sender: TObject);
begin
  Execute('st 30');
end;

procedure TForm1.mi10SecClick(Sender: TObject);
begin
  Execute('st 10');
end;

procedure TForm1.mi5SecClick(Sender: TObject);
begin
  Execute('st 5');
end;

procedure TForm1.mi1SecClick(Sender: TObject);
begin
  Execute('st 1');
end;

procedure TForm1.mi4PliesClick(Sender: TObject);
begin
  Execute('sd 4');
end;

procedure TForm1.mi5PliesClick(Sender: TObject);
begin
  Execute('sd 5');
end;

procedure TForm1.mi6PliesClick(Sender: TObject);
begin
  Execute('sd 6');
end;

procedure TForm1.mi7PliesClick(Sender: TObject);
begin
  Execute('sd 7');
end;

procedure TForm1.mi8PliesClick(Sender: TObject);
begin
  Execute('sd 8');
end;

procedure TForm1.mi5MinClick(Sender: TObject);
begin
  Execute('level 0 5 0');
end;

procedure TForm1.mi10MinClick(Sender: TObject);
begin
  Execute('level 0 10 0');
end;

procedure TForm1.mi15MinClick(Sender: TObject);
begin
  Execute('level 0 15 0');
end;

procedure TForm1.mi30MinClick(Sender: TObject);
begin
  Execute('level 0 30 0');
end;

procedure TForm1.miNewClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  Execute('new');
  FenToGraphBoard(BoardToFen);
  if miBookEditor.Checked then
  begin
    ProgPassive := TRUE;
    BookEditor.RefreshForm;
  end;
end; // New1Click

procedure TForm1.miQuitClick(Sender: TObject);
begin
  Execute('st 1');
  Application.Terminate;
end; // Quit1Click

procedure TForm1.miStatsClick(Sender: TObject);
begin
  ShowMessage(MyStat.MakeString);
end; // Statistics1Click

procedure TForm1.miStartClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  ActivateProgram;
  RefreshBoardDisplay;
end; // Think1Click

procedure TForm1.miTurnClick(Sender: TObject);
begin
  InvertPieceStruct;
end; // Invert1Click

procedure TForm1.miStopClick(Sender: TObject);
begin
  Execute('stop');
end; // Deactivate1Click

procedure TForm1.tmAppTimer(Sender: TObject);
begin
  if ClockSide = WHITE then
  begin
    Inc(WMTime);
    sgClock.Cells[0, 1] := SecToClock(WMTime);
    Inc(WTotTime);
    sgClock.Cells[0, 2] := SecToClock(WTotTime);
  end else
  begin
    Inc(BMTime);
    sgClock.Cells[1, 1] := SecToClock(BMTime);
    Inc(BTotTime);
    sgClock.Cells[1, 2] := SecToClock(BTotTime);
  end;
end; // Form1.Timer1Timer

procedure TForm1.UpdateNotation;
begin
  if Notation = N_SAN then
    mmGame.Text := GameToSan
  else
    mmGame.Text := GameToPGN;
end; // UpdateNotation

procedure TForm1.tbBackClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  mmInfo.Font.Size := 8;
  mmInfo.Text := sMoveBack;
  SafeUndoMove;
  UpdateNotation;
  RefreshBoardDisplay;
  SwitchMoveColor;
end; // undoing a move

procedure TForm1.tbBackAllClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  mmInfo.Font.Size := 8;
  mmInfo.Text := 'Back to start';
  StackIndex := 1;
  SafeUndoMove;
  mmInfo.Text := sBackAll;
  UpdateNotation;
  RefreshBoardDisplay;
  ClockSide := Board.Side;
end; // undoing all moves

procedure TForm1.tbForwardClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  mmInfo.Text := sMoveFwd;
  SafeRedoMove;
  UpdateNotation;
  RefreshBoardDisplay;
  SwitchMoveColor;
end; // redoing a move

procedure TForm1.tbForwardAllClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  mmInfo.Text := '';
  StackIndex := StackBound - 1;
  SafeRedoMove;
  UpdateNotation;
  RefreshBoardDisplay;
  ClockSide := Board.Side;
  mmInfo.Text := sFwdAll;
end; // going to the end of the game

procedure TForm1.miBookEditorClick(Sender: TObject);
begin
  if not miBookEditor.Checked then
  begin
    BookEditor.miPopulate.Checked := FALSE;
    BookEditor.Hide;
  end else
  begin
    ProgPassive := TRUE;
    BookEditor.Show;
  end;
end; // opens or closes book editor

procedure TForm1.tbShowClick(Sender: TObject);
begin
  if mmAnalyze.Visible then
  begin
    mmAnalyze.Hide;
    mmInfo.Hide;
    ClientHeight := sgBoard.Top + sgBoard.Height + 5;
  end else
  begin
    mmAnalyze.Show;
    mmInfo.Show;
    ClientHeight := mmAnalyze.Top + mmAnalyze.Height + 5;
  end;
end; // shows or hides analysis window

procedure TForm1.tbInfoClick(Sender: TObject);
begin
  ShowMessage(sProgramInfo);
end; // displays program info

procedure TForm1.tbEvalClick(Sender: TObject);
begin
  if ProgThinking then
    Exit;
  if not DebugForm.Visible then
  begin
    SetProgSide;
    DebugForm.GeneralMemo.Text := MyEval.EvalString;
    DebugForm.WhiteMemo.Text := MyEval.DebugWhite;
    DebugForm.BlackMemo.Text := MyEval.DebugBlack;
    DebugForm.Show;
  end else
    DebugForm.Hide;
end; // displays evaluation details

procedure TForm1.Savecurrent1Click(Sender: TObject);
begin
  WriteIniFile('clericus.ini');
end; // saves current settings

procedure TForm1.miRestoreClick(Sender: TObject);
begin
  RestoreSettings;
  sgBoard.Repaint;
end; // restores original settings

procedure TForm1.miEvalParamEditorClick(Sender: TObject);
begin
  EvalForm.Show;
end; // EvalEditor1Click

procedure TForm1.miObjectiveClick(Sender: TObject);
begin
  miObjective.Checked := TRUE;
  ReadIniFile('style/objective.ini');
end; // Objective1Click

procedure TForm1.miSmartClick(Sender: TObject);
begin
  miSmart.Checked := TRUE;
  ReadIniFile('style/smart.ini');
end; // Smart1Click

procedure TForm1.miPessimistClick(Sender: TObject);
begin
  miPessimist.Checked := TRUE;
  ReadIniFile('style/pessimist.ini');
end; // Pessimist1Click

procedure TForm1.BookSelection(CurrentBook: string);
begin
  MyBook.ClearBookArray(0);
  GuideBookName := CurrentBook;
  LoadBook;
  if miBookEditor.Checked then
    BookEditor.RefreshForm;
end; // BookSelection

procedure TForm1.miNoBookClick(Sender: TObject);
begin
  MissedOpening := 100;
  miNoBook.Checked := TRUE;
end; // BookOff1Click

procedure TForm1.miBookNormalClick(Sender: TObject);
begin
  miBookNormal.Checked := TRUE;
  BookSelection('books/MainBook.hop');
  MissedOpening := 0;
end; // Normal1Click

procedure TForm1.miBookBroadClick(Sender: TObject);
begin
  miBookBroad.Checked := TRUE;
  BookSelection('books/Broad.hop');
  MissedOpening := 0;
end; // Broad1Click

procedure TForm1.miBookSolidClick(Sender: TObject);
begin
  miBookSolid.Checked := TRUE;
  BookSelection('books/Solid.hop');
  MissedOpening := 0;
end; // Solid1Click

procedure TForm1.miBookModernClick(Sender: TObject);
begin
  miBookModern.Checked := TRUE;
  BookSelection('books/Hypermodern.hop');
  MissedOpening := 0;
end; // Hypermodern1Click

procedure TForm1.miBookFighterClick(Sender: TObject);
begin
  miBookFighter.Checked := TRUE;
  BookSelection('books/Fighter.hop');
  MissedOpening := 0;
end; // Fighter1Click

procedure TForm1.miBookGambitClick(Sender: TObject);
begin
  miBookGambit.Checked := TRUE;
  BookSelection('books/Gambit.hop');
  MissedOpening := 0;
end; // Gambit1Click

procedure TForm1.miBookUnusualClick(Sender: TObject);
begin
  miBookUnusual.Checked := TRUE;
  BookSelection('books/Unusual.hop');
  MissedOpening := 0;
end; // Unusual1Click

procedure TForm1.miPolishClick(Sender: TObject);
begin
  //PolishLanguage;
  vLang := POLISH;
  LanguageSettings;
  EvalForm.PolishLanguage;
  BookEditor.PolishLanguage;
  SearchEditor.PolishLanguage;
end; // Polish1Click

procedure TForm1.miEnglishClick(Sender: TObject);
begin
  //EnglishLanguage;
  vLang := ENGLISH;
  LanguageSettings;
  EvalForm.EnglishLanguage;
  BookEditor.EnglishLanguage;
  SearchEditor.EnglishLanguage;
end; // English1.Click

procedure TForm1.miSearchParamEditorClick(Sender: TObject);
begin
  SearchEditor.Show;
end; // SearchEditor1Click

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Execute('st 1'); // forces immediate exit
end; // FormClose

procedure TForm1.FormCreate;
begin
  Randomize;

  ProgAttack := 200;
  OppoAttack := 200;
  ProgMobility := 100;
  OppoMobility := 150;
  ProgShield := 100;
  OppoShield := 100;

  gUsePictures := (Screen.Fonts.IndexOf('Chess Usual') = -1) or TRUE;
  Caption := 'Clericus ' + CLERICUS_VERSION;
{$IFDEF XBOARD}
  Application.ShowMainForm := FALSE;
  Visible := FALSE;
  WBThread := TWBThread.Create(FALSE, Handle);
  WBThread.SendCommand('feature done="0"', TRUE);
{$ENDIF}
  SetHashData;
  MyStat := TStat.Create;
  History := THistory.Create;
  MyEval := TEval.Create;
  MyBook := TBook.Create;
  MySearch := TSearch.Create;
  ReadIniFile('clericus.ini');
  LoadBook;
  MyBook.ReadCommentFile;
  PrepareAll;
  InitBoard;
  ProgPassive := FALSE;
{$IFDEF XBOARD}
  WBThread.SendCommand('feature done="1"', TRUE);
{$ELSE}
  FenToGraphBoard(BoardToFen);
  LanguageSettings;
{$ENDIF}
{$IFNDEF DEBUG}
  miEvalParamEditor.Enabled := FALSE;
  miSearchParamEditor.Enabled := FALSE;
  miEvalParamEditor.Visible := FALSE;
  miSearchParamEditor.Visible := FALSE;
{$ENDIF}
end; // TForm1.FormCreate

procedure TForm1.FormDestroy(Sender: TObject);
begin
  MyStat.Destroy;
  History.Destroy;
  MyBook.Destroy;
  MyEval.Destroy;
  MySearch.Destroy;
end; // FormDestroy

end.
