
// This file is a part of Clericus Chess program, written by
// Pawel Koziol, nescitus@o2.pl. It contains class headers.

TStat = class // implemented in stat.pas
public
  Nodes, QNodes, PVNodes, HHits, HCuts,
  NullTries, NullCuts, DeltaCuts, Checks,
  IIDeep, FutCuts, RazorReds, LMReds : Integer;
  procedure Clear;
  function MakeString : string;
  function KNodes : string;
  function Nps( STi, ETi : TDateTime ) : string;
  function PercentStr( SmallNo, BigNo : Integer; Explanation : string ) : string;
end; // TStat

THistory = class   // implemented in hist.pas
private
   Table : TCoef;
   procedure Trim;
public
   constructor Create;
   procedure ClearTable;
   procedure Age;
   procedure Update( Depth, SqFr, SqTo : Integer );
   function Value( SqFr, SqTo : Integer ) : Integer;
end; // THistory

TBook = class   // implemented in book.pas
  private
     procedure SortBookMove( From : Integer );
     function GetMoveValue( A : Integer ) : Integer;
  public
     BookMoveCount : Integer;
     procedure ReadBookFile;
     procedure ReadCommentFile;
     procedure WriteBookMove( Fen, Move : string );
     procedure WriteComment( Fen, Comment : string );
     procedure SaveBook;
     procedure SaveComments;
     procedure MarkBookMove( Fen, Move : string; Mark : Integer );
     procedure DeleteBookMove( Fen, Move : string );
     procedure ClearBookArray( BookNo : Integer );
     procedure ClearCommentArray;
     function  BookMoveString : string;
     function  CommentString : string;
     function  IsFlag( MoveString : string ) : Boolean;
     function  GetMissingMove : string;
     procedure GetRandomLeaf;
  end; // TBook

TEval = class   // TEval encapsulates evaluation function.
  private
    Dist        : TCoef;     // auxiliary data structures
    Neighbours  : array[ BLACK..WHITE, 0..127, 0..127 ] of Boolean;

    EvalHash : TSmallHash;              // Hash tables
    PawnHash : TSmallHash;

    isEndgame  : Boolean; // flag for turning on endgame tests
    WAttackers, BAttackers : Integer; // # of pieces attacking king
    WPressure,  BPressure  : Integer; // pressure created by them
    WMobility,  BMobility  : Integer;
    WThreats,   BThreats   : Integer; // direct attacks on pieces
    WTrapped,   BTrapped   : Integer; // blocked pieces penalty
    WPromDist,  BPromDist  : Integer;
    WRookCon,   BRookCon   : Boolean; // rook connection flags

    procedure SetDist;
    procedure SetNeighbours;
    procedure InitEval;
    procedure WPThreatens( Sqr : Integer );
    procedure BPThreatens( Sqr : Integer );
    function  WKSFianchetto : Integer;
    function  BKSFianchetto : Integer;

    // functions concerned with board geometry
    function IsKingSide( Sqr : Integer ) : Boolean;
    function IsQueenSide( Sqr : Integer ) : Boolean;
    function IsCentralFile( Sqr : Integer ) : Boolean;
    function HorDist( Sqr1, Sqr2 : Integer ) : Integer;

    // functions detecting piece relationships
    function IsWNOutpost( Sqr : Integer ) : Integer;
    function IsBNOutpost( Sqr : Integer ) : Integer;
    function NearFile( Sqr1, Sqr2 : Integer ) : Boolean;
    function NearRank( Sqr1, Sqr2 : Integer ) : Boolean;
    function IsOnWPWay( Sqr : Integer ) : Boolean;
    function IsOnBPWay( Sqr : Integer ) : Boolean;
    function AreKingSidePawns : Boolean;
    function AreQueenSidePawns : Boolean;
    function IsNeighbour( Color, Sqr1, Sqr2 : Integer ) : Boolean;
    function IsWAttacker( Sqr, Vect, Target, Bonus : Integer ) : Boolean;
    function IsBAttacker( Sqr, Vect, Target, Bonus : Integer ) : Boolean;
    function IsWNAttacker( Sqr1, Sqr2 : Integer ) : Boolean;
    function IsBNAttacker( Sqr1, Sqr2 : Integer ) : Boolean;
    function ScaleAttacks( Points, Pieces : Integer ) : Integer;
    function IsBishop( ListLoc : Integer ) : Boolean;
    function OppositeBishops : Boolean;
    function EndgameType : TEndgame;
    function AdjustMaterial : Integer;

    // draw detectors
    function IsDrawKBPKX( CurrScore : Integer ) : Boolean;
    function IsDrawKRPKR_White : Boolean;
    function IsDrawKRPKR_Black : Boolean;
    function IsInsufficientMaterial( CurrScore : Integer ) : Boolean;

    // mobility functions
    function WBMob( Sqr, Vect : Integer ) : Integer;
    function BBMob( Sqr, Vect : Integer ) : Integer;
    function WRMob( Sqr, Vect : Integer ) : Integer;
    function BRMob( Sqr, Vect : Integer ) : Integer;

    // pawn evaluation functions
    function PawnMaterial( Sqr : Integer ) : Integer;
    function WKPawnShield( Sqr : Integer ) : Integer;
    function BKPawnShield( Sqr : Integer ) : Integer;
    function IsWPFree( Sqr : Integer ) : Boolean;
    function IsBPFree( Sqr : Integer ) : Boolean;
    function IsWPIsolated( Sqr : Integer ) : Boolean;
    function IsBPIsolated( Sqr : Integer ) : Boolean;
    function IsWPBackward( Sqr : Integer ) : Boolean;
    function IsBPBackward( Sqr : Integer ) : Boolean;
    function WFreePEval( Sqr : Integer ) : Integer;
    function BFreePEval( Sqr : Integer ) : Integer;
    function WWeakPEval( Sqr : Integer ) : Integer;
    function BWeakPEval( Sqr : Integer ) : Integer;
    function WPEv( Sqr : Integer ) : Integer;
    function BPEv( Sqr : Integer ) : Integer;
    function PawnEval : Integer;

// Evaluating single pieces
    function WPRelations( Sqr : Integer ) : Integer;
    function BPRelations( Sqr : Integer ) : Integer;
    function WNEv( Sqr : Integer ) : Integer;
    function BNEv( Sqr : Integer ) : Integer;
    function WBEv( Sqr : Integer ) : Integer;
    function BBEv( Sqr : Integer ) : Integer;
    function WREv( Sqr : Integer ) : Integer;
    function BREv( Sqr : Integer ) : Integer;
    function WQEv( Sqr : Integer ) : Integer;
    function BQEv( Sqr : Integer ) : Integer;
    function WKEv( Sqr : Integer ) : Integer;
    function BKEv( Sqr : Integer ) : Integer;

    function Patterns : Integer;

  public

//  Customizable parameters of the evaluation function;     /
//  please note the following naming convention:            /
//                                                          /
//  1) all  parameters' names  consist of  capital letters  /
//  2) piece-specific terms begin with the name of the piece/
//  3) if necessary, name ends with a letter indicating     /
//     the influenced piece or the board location           /

    Q_VALUE       : Integer;       P_VALUE_N     : Integer;
    R_VALUE       : Integer;       P_VALUE_DE    : Integer;
    B_VALUE       : Integer;       P_VALUE_AH    : Integer;
    N_VALUE       : Integer;

    GRAIN         : Integer;      TEMPO         : Integer;
    IMBALANCE     : Integer;

    P_DOUBLE      : Integer;      P_PROTECTS_K  : Integer;
    P_WEAK        : Integer;      P_DESERTED_K  : Integer;
    P_BLOCKED_DE  : Integer;      P_RUNNING     : Integer;

    N_MOB_UNIT    : Integer;      N_TRAPPED     : Integer;
    N_BLOCK_P     : Integer;      N_UNSAFE      : Integer;

    B_PAIR        : Integer;      B_TRAP_A7     : Integer;
    B_PIN         : Integer;      B_TRAP_A6     : Integer;
    B_NO_FIANCH   : Integer;      B_CENT_MISS   : Integer;
    B_UNSAFE      : Integer;

    R_OPEN        : Integer;      R_PRESS_K     : Integer;
    R_HALF        : Integer;      R_PAIR        : Integer;

    Q_DIST_K      : Integer;      Q_EARLY       : Integer;

    K_UNCASTLED   : Integer;      K_COLOR_WEAK  : Integer;
    K_BLOCK_R     : Integer;

    N_ATTACKS_K   : Integer;
    B_ATTACKS_K   : Integer;
    R_ATTACKS_K   : Integer;
    Q_ATTACKS_K   : Integer;

    DoPatterns    : Boolean;      DoOutposts    : Boolean;
    DoPawnStructs : Boolean;      DoScaleMat    : Boolean;

    procedure InitPawnsOnFile;  // This is public, because /
    // it will be used in interior node recognizers        /

    function  Eval( DoHash : Boolean ) : Integer;

    // Debug output
    function GamePhaseString : string;
    function EvalString : string;
    function DebugWhite : string;
    function DebugBlack : string;
    constructor Create;
  end; // TEval

TSearch = class // quiesc.pas, search.pas, root.pas, iterate.pas
  private
    IsFailLow : Boolean;
    DepthFinished : Integer;
    MaxExt    : Integer;
    Killers   : array[ -MAX_DEPTH..MAX_DEPTH, 0..4 ] of Integer;

    procedure UpdateKillers( Ply, SqFr, SqTo : Integer );
    function TimeOut : Boolean;
    function CanMove : Boolean;
    function MaxCaptValue( SqFr, SqTo : Integer; Immediate : Boolean ) : Integer;
    function NullOK : Boolean;
    function PawnToSeventh( SqTo : Integer ) : Boolean;
    function Quiesc(Ply, Alfa, Beta : Integer; Immediate : Boolean ) : Integer;
    function MinimalWindowSearch(Ply, Depth, Alfa, Beta, Target : Integer; WasNull : Boolean ) : Integer;
    function Search(Ply, Depth, Alfa, Beta, Target : Integer; WasNull : Boolean ) : Integer;
    function RootSearch(Ply, Depth, Alfa, Beta : Integer ) : Integer;
  public
    ASP              : Integer;  DO_ASP   : Boolean;
    FUT_1            : Integer;  DO_FUT1  : Boolean;
    FUT_2            : Integer;  DO_FUT2  : Boolean;
    FUT_3            : Integer;  DO_FUT3  : Boolean;
    DO_PVS           : Boolean;
    DISABLE_IID_FROM : Integer;  DO_IID   : Boolean;
    DO_STATIC        : Boolean;  DO_LMR   : Boolean;
    NULL_IN_PV       : Boolean;
    HASH_IN_PV       : Boolean; 

    function IsStaleMate : Boolean;
    function IsCheckMate : Boolean;
    function Iterate( Depth : Integer ) : Integer;
    constructor Create;
  end; // TSearch