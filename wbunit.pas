
//***************************************************************
//
// Wbunit (C) dr Maciej Szmit 2005
// Inspirations:
//      Unit_Winboard (C) Norbert Dudek
//      MyChess       (C) Grzegorz Olczak
//      Geko          (C) Giuseppe Canella
//
//***************************************************************

unit wbunit;

interface
uses
	Classes, Windows, Messages, SysUtils;

type
	TWbThread	= class( TThread )
	private
		HFormHandle	: THandle;
	protected
		procedure Execute; override; //sends standard Windwos message to main form window
	public
    WinboardCommand	: String;
		constructor Create(CreateSuspended: Boolean; Form : THandle);
    procedure SendCommand( Msg : string; DoRecord : Boolean ); //sends command to winboard
    procedure SendMove(mv : string); //sends move (with 'move ') prefix to winboard
	end;

const
	CW_WBMSG = WM_USER + 1000;

var LogFile : TextFile;

implementation  // TWbThread

constructor TWbThread.Create(CreateSuspended: Boolean; Form: THandle);
begin
	inherited Create(CreateSuspended);
	HFormHandle := Form;
  WinboardCommand:='';
  AssignFile( LogFile, 'wblog.log' );
  Rewrite( LogFile );
end;

procedure TWbThread.Execute;
var
	Command	: string;
	ch		: char;
begin
	while TRUE do
	begin
		Command :='';
		repeat
			try
				Read( input, ch );
			except
				Suspend;
				Exit;
			end;
			if ch <> #10 then
				Command := Command + ch;
		until ( ch = #10 );
    WinboardCommand:= Command;
    Writeln( LogFile, 'RECEIVING ' + Command );
		SendCommand( Command, FALSE ); //sends "acknowledgement" to Winboard
		SendMessage( HFormHandle, CW_WBMSG, 0, 0 );
    //SendMessage not PostMessage only synchronous execution allowed !!!
	end;
end;

procedure TWbThread.SendCommand( Msg : string; DoRecord : Boolean );
begin
   if  ( Length( Msg ) <> 0 )
   and ( Msg[1] <> 'O' )
       then Msg := Lowercase( Msg );
   if DoRecord
      then Writeln( LogFile, 'SENDING ' + Msg );
   Write( msg + #10 );
   Flush( Output );

end; // TWbThread.SendCommand

procedure TWbThread.SendMove( mv : string );
begin
 	SendCommand( 'move ' + mv, TRUE );
  Flush( Output );
end; // TWbThread.SendMove

end.
