unit bookunit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Buttons, Menus, ExtCtrls, language;

type
  TBookEditor = class(TForm)
    sgBook: TStringGrid;
    edMove: TEdit;
    btBad: TSpeedButton;
    btDubious: TSpeedButton;
    btNeutral: TSpeedButton;
    btFine: TSpeedButton;
    btGreat: TSpeedButton;
    btDelete: TSpeedButton;
    mnBook: TMainMenu;
    miOptions: TMenuItem;
    miPopulate: TMenuItem;
    miSave: TMenuItem;
    miSelectedBook: TMenuItem;
    miMain: TMenuItem;
    miGuide: TMenuItem;
    miAlternative: TMenuItem;
    miFind: TMenuItem;
    btBlank: TButton;
    btFinal: TButton;
    miRandomLeaf: TMenuItem;
    mmComment: TMemo;
    btValidate: TButton;
    btSave: TButton;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure miSaveClick(Sender: TObject);
    procedure miPopulateClick(Sender: TObject);
    procedure btBadClick(Sender: TObject);
    procedure btDubiousClick(Sender: TObject);
    procedure btNeutralClick(Sender: TObject);
    procedure btFineClick(Sender: TObject);
    procedure btGreatClick(Sender: TObject);
    procedure sgBookMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btDeleteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miMainClick(Sender: TObject);
    procedure miGuideClick(Sender: TObject);
    procedure sgBookDblClick(Sender: TObject);
    procedure miRandomClick(Sender: TObject);
    procedure miAlternativeClick(Sender: TObject);
    procedure btBlankClick(Sender: TObject);
    procedure btFinalClick(Sender: TObject);
    procedure miRandomLeafClick(Sender: TObject);
    procedure btValidateClick(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
  public
    DispEntries: Integer;
    procedure RefreshForm;
    procedure PolishLanguage;
    procedure EnglishLanguage;
    procedure LanguageSettings;
    function GetLeafValue: Integer;
  end; // TBookEditor

var
  BookEditor: TBookEditor;

implementation

uses main;
{$R *.dfm}

procedure TBookEditor.PolishLanguage;
begin
  Caption := 'Edytor debiut�w';
  miOptions.Caption := 'Opcje';
  miPopulate.Caption := 'Czytaj z planszy';
  miSave.Caption := 'Zapisz teraz';
  miSelectedBook.Caption := 'Edytuj';
  miMain.Caption := 'g��wn� ksi��k�';
  miGuide.Caption := 'ksi��k� repertuarow�';
  miFind.Caption := 'Znajd�';
  miAlternative.Caption := 'Ruchy z odpowiedzi�';
  miRandomLeaf.Caption := 'Losowy li��';
end; // PolishLanguage

Procedure TBookEditor.EnglishLanguage;
begin
  Caption := 'Book editor';
  miOptions.Caption := 'Options';
  miPopulate.Caption := 'Populate from board';
  miSave.Caption := 'Save now';
  miSelectedBook.Caption := 'Book';
  miMain.Caption := 'Main book';
  miGuide.Caption := 'Guide book';
  miFind.Caption := 'Find';
  miAlternative.Caption := 'Moves with continuation';
  miRandomLeaf.Caption := 'Random leaf';
end; // EnglishLanguage

procedure TBookEditor.LanguageSettings;
begin
  case vLang of
    POLISH:
      PolishLanguage;
    ENGLISH:
      EnglishLanguage;
  end; // of case
end; // LanguageSettings

function TBookEditor.GetLeafValue: Integer;
begin
  MyBook.GetRandomLeaf;
  Form1.Execute('sd 8');
  Result := MySearch.Iterate(8);
  Form1.FenToGraphBoard(Form1.BoardToFen);
  RefreshForm;
  Form1.Execute('stop');
end; // GetLeafValue

procedure ResetMoveValue(Fen, Move: string);
var
  A: Integer;
begin
  for A := 0 to NofEntries do
    if (BookArray[BookNo, A].Fen = TS78(Fen)) and (BookArray[BookNo, A].Move = shortstring(Move))
    then
    begin
      BookArray[BookNo, A].Freq := 10;
      Exit;
    end;
end; // ResetMoveValue

procedure TBookEditor.RefreshForm;
var
  A, B, Slot: Integer;
  FenString: string;
begin
  mmComment.Text := MyBook.CommentString;
  Label1.Caption := IntToStr(DispEntries) + '/' + IntToStr(BOOK_SIZE);
  edMove.Text := '';

  for A := 0 to 2 do
    for B := 0 to 12 do
      sgBook.Cells[A, B] := '';

  FenString := Form1.BoardToFen;
  Slot := 0;
  for A := 1 to NofEntries do
  begin
    if BookArray[BookNo, A].Fen = TS78(FenString) then
    begin
      sgBook.Cells[0, Slot] := string(BookArray[BookNo, A].Move);
      if BookArray[BookNo, A].Freq = -100 then
        sgBook.Cells[1, Slot] := 'deleted'
      else
        sgBook.Cells[1, Slot] := IntToStr(BookArray[BookNo, A].Freq);

      case BookArray[BookNo, A].Learn of
        B_EXCL:
          sgBook.Cells[2, Slot] := '!';
        B_FUN:
          sgBook.Cells[2, Slot] := '!?';
        B_DISL:
          sgBook.Cells[2, Slot] := '?!';
        B_BAD:
          sgBook.Cells[2, Slot] := '?';
        B_END:
          sgBook.Cells[2, Slot] := 'END';
      end; // of case

      Inc(Slot);
    end;
  end;
  edMove.Text := sgBook.Cells[0, 0];

end; // RefreshForm

procedure TBookEditor.FormShow(Sender: TObject);
begin
  RefreshForm;
end; // FormShow

procedure TBookEditor.miSaveClick(Sender: TObject);
begin
  Form1.InfoMessage(sBookSaving);
  MyBook.SaveBook;
  Form1.InfoMessage(sBookSaved);
end; // SaveNow1Click

procedure TBookEditor.miPopulateClick(Sender: TObject);
begin
  if not miPopulate.Checked then
    Form1.Execute('stop');
end; // FeedFromBoard1Click

procedure TBookEditor.btBadClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_BAD);
  RefreshForm;
end; // BadMoveButtClick

procedure TBookEditor.btDubiousClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_DISL);
  RefreshForm;
end; // DubiousMoveButtClick

procedure TBookEditor.btNeutralClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_NORM);
  RefreshForm;
end; // NormalMoveButtClick

procedure TBookEditor.btFineClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_FUN);
  RefreshForm;
end; // FineMoveButtClick

procedure TBookEditor.btGreatClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_EXCL);
  RefreshForm;
end; // GreatMoveButtClick

procedure TBookEditor.sgBookMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Col, Row: Integer;
begin
  sgBook.MouseToCell(X, Y, Col, Row);
  edMove.Text := sgBook.Cells[0, Row];
end; // BookGridMouseDown

procedure TBookEditor.btDeleteClick(Sender: TObject);
begin
  MyBook.DeleteBookMove(Form1.BoardToFen, edMove.Text);
  Dec(DispEntries);
  Label1.Caption := IntToStr(DispEntries);
  RefreshForm;
end; // DeleteMoveButtClick

// Temporary button, to be used until the move frequencies
// are mended.
(*
procedure TBookEditor.btResetClick(Sender: TObject);
begin
  ResetMoveValue(Form1.BoardToFen, edMove.Text);
  RefreshForm;
end; // Button1Click
*)

procedure TBookEditor.FormCreate(Sender: TObject);
begin
  DispEntries := NofEntries;
  LanguageSettings;
end; // FormCreate

procedure TBookEditor.miMainClick(Sender: TObject);
begin
  BookNo := 1;
  BookName := MainBookName;
  miMain.Checked := TRUE;
  RefreshForm;
end; // MainBook1Click

procedure TBookEditor.miGuideClick(Sender: TObject);
begin
  BookNo := 0;
  BookName := GuideBookName;
  miGuide.Checked := TRUE;
  RefreshForm;
end; // GuideBook1Click

procedure TBookEditor.sgBookDblClick(Sender: TObject);
begin
  Form1.Execute('stop');
  Form1.Execute(edMove.Text);
  Form1.FenToGraphBoard(Form1.BoardToFen);
  RefreshForm;
end; // BookGridDblClick;

procedure TBookEditor.miRandomClick(Sender: TObject);
begin
  GetLeafValue;
end;

procedure TBookEditor.miAlternativeClick(Sender: TObject);
begin
  ShowMessage(MyBook.GetMissingMove);
end; // CheckAlternatives1Click

procedure TBookEditor.btBlankClick(Sender: TObject);
begin
  MyBook.WriteBookMove(Form1.BoardToFen, 'FILL');
  RefreshForm;
end;

procedure TBookEditor.btFinalClick(Sender: TObject);
begin
  MyBook.MarkBookMove(Form1.BoardToFen, edMove.Text, B_END);
  RefreshForm;
end;

procedure TBookEditor.miRandomLeafClick(Sender: TObject);
begin
  MyBook.GetRandomLeaf;
end;

procedure TBookEditor.btValidateClick(Sender: TObject);
begin
  MyBook.WriteComment(Form1.BoardToFen, mmComment.Text);
end;

procedure TBookEditor.btSaveClick(Sender: TObject);
begin
  MyBook.SaveComments;
end;

end.
